// The most first thing you need to load the config
import dotenv from 'dotenv';
import Server from './rewrite/sosise-core/Server';
dotenv.config();

const server = new Server();
server.run();
