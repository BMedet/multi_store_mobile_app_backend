/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import Helper from "sosise-core/build/Helper/Helper";

const storeConfig = {
    defaultStoreType: {
        id: 0,
        entityId: 0,
        label: 'DEFAULT',
        code: 'DEFAULT',
        baseUrl: 'DEFAULT',
        token: 'DEFAULT',
        isActive: true,
        createdAt: Helper.getCurrentDateTime(),
        updatedAt: Helper.getCurrentDateTime()
    }
};

export default storeConfig;
