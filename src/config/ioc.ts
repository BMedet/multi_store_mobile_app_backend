import IOC from "sosise-core/build/ServiceProviders/IOC";
import LoggerService from "sosise-core/build/Services/Logger/LoggerService";
import CustomerRepository from "../app/Repositories/Db/LocalStorage/Customer/CustomerRepository";
import CatalogAttributeDbRepository from "../app/Repositories/Db/LocalStorage/Index/Catalog/Attribute/CatalogAttributeDbRepository";
import CatalogCategoryDbRepository from "../app/Repositories/Db/LocalStorage/Index/Catalog/Category/CatalogCategoryDbRepository";
import CatalogProductDbRepository from "../app/Repositories/Db/LocalStorage/Index/Catalog/Product/CatalogProductDbRepository";
import RegionDbRepository from "../app/Repositories/Db/LocalStorage/Index/Region/RegionDbRepository";
import LogRepository from "../app/Repositories/Db/LocalStorage/Log/LogRepository";
import StoreRepository from "../app/Repositories/Db/LocalStorage/Store/StoreRepository";
import CatalogAttributesMagentoDbRepository from "../app/Repositories/Db/Magento/Catalog/Attribute/CatalogAttributesMagentoDbRepository";
import CatalogCategoryMagentoDbRepository from "../app/Repositories/Db/Magento/Catalog/Category/CatalogCategoryMagentoDbRepository";
import CatalogProductMagentoDbRepository from "../app/Repositories/Db/Magento/Catalog/Product/CatalogProductMagentoDbRepository";
import RegionMagentoDbRepository from "../app/Repositories/Db/Magento/Region/RegionMagentoDbRepository";
import CatalogAttributeElasticsearchRepository from "../app/Repositories/Elasticsearch/Index/Catalog/CatalogAttributeElasticsearchRepository";
import CatalogCategoryElasticsearchRepository from "../app/Repositories/Elasticsearch/Index/Catalog/CatalogCategoryElasticsearchRepository";
import CatalogProductElasticsearchRepository from "../app/Repositories/Elasticsearch/Index/Catalog/CatalogProductElasticsearchRepository";
import RegionElasticsearchRepository from "../app/Repositories/Elasticsearch/Index/Region/RegionElasticsearchRepository";
import CatalogAttributeSearchRepository from "../app/Repositories/Elasticsearch/Search/Catalog/Attribute/CatalogAttributeSearchRepository";
import CatalogCategorySearchRepository from "../app/Repositories/Elasticsearch/Search/Catalog/Category/CatalogCategorySearchRepository";
import CatalogProductSearchRepository from "../app/Repositories/Elasticsearch/Search/Catalog/Product/CatalogProductSearchRepository";
import RegionSearchRepository from "../app/Repositories/Elasticsearch/Search/Region/RegionSearchRepository";
import CatalogAttributeAppService from "../app/Services/Frontend/Catalog/CatalogAttributeAppService";
import CatalogCategoryAppService from "../app/Services/Frontend/Catalog/CatalogCategoryAppService";
import CatalogProductAppService from "../app/Services/Frontend/Catalog/CatalogProductAppService";
import CustomerAppService from "../app/Services/Frontend/Customer/CustomerAppService";
import RegionAppService from "../app/Services/Frontend/Region/RegionAppService";
import CatalogAttributesIndexToDbService from "../app/Services/Index/Db/Catalog/CatalogAttributesIndexToDbService";
import CatalogCategoryIndexToDbService from "../app/Services/Index/Db/Catalog/CatalogCategoryIndexToDbService";
import CatalogProductIndexToDbService from "../app/Services/Index/Db/Catalog/CatalogProductIndexToDbService";
import RegionIndexToDbService from "../app/Services/Index/Db/Region/RegionIndexToDbService";
import CatalogAttributeIndexToElasticService from "../app/Services/Index/Elastic/Catalog/CatalogAttributeIndexToElasticService";
import CatalogCategoryIndexToElasticService from "../app/Services/Index/Elastic/Catalog/CatalogCategoryIndexToElasticService";
import CatalogProductIndexToElasticService from "../app/Services/Index/Elastic/Catalog/CatalogProductIndexToElasticService";
import RegionIndexToElasticService from "../app/Services/Index/Elastic/Region/RegionIndexToElasticService";
import CustomLoggerService from "../app/Services/Logger/CustomLoggerService";

/**
 * IOC Config, please register here your services
 */
const iocConfig = {
    /**
     * Singleton services
     *
     * How to register:
     * YourServiceName: () => new YourServiceName()
     *
     * How to use:
     * const logger = IOC.makeSingleton(LoggerService) as LoggerService;
     */
    singletons: {
    },

    /**
     * Non singleton services
     *
     * How to register:
     * YourServiceName: () => new YourServiceName()
     *
     * How to use:
     * const logger = IOC.make(LoggerService) as LoggerService;
     */
    nonSingletons: {
        /**
         * This service is included in the core out of the box
         * If you want to override LoggerService just uncomment this code and import all necessary modules
         */


        CustomLoggerService: () => {
            return new CustomLoggerService(new LogRepository(), IOC.make(LoggerService));
        },

        // Catalog category Index
        CatalogCategoryIndexToDbService: () => {
            return new CatalogCategoryIndexToDbService(new CatalogCategoryMagentoDbRepository(), new CatalogCategoryDbRepository(), new StoreRepository());
        },
        CatalogCategoryIndexToElasticService: () => {
            return new CatalogCategoryIndexToElasticService(new CatalogCategoryDbRepository(), new CatalogCategoryElasticsearchRepository(), new StoreRepository());
        },

        // Catalog product index
        CatalogProductIndexToDbService: () => {
            return new CatalogProductIndexToDbService(new CatalogProductMagentoDbRepository(), new CatalogProductDbRepository(), new StoreRepository());
        },
        CatalogProductIndexToElasticService: () => {
            return new CatalogProductIndexToElasticService(new CatalogProductDbRepository(), new CatalogProductElasticsearchRepository(), new StoreRepository());
        },

        // Region index
        RegionIndexToDbService: () => {
            return new RegionIndexToDbService(new RegionMagentoDbRepository(), new RegionDbRepository(), new StoreRepository());
        },
        RegionIndexToElasticService: () => {
            return new RegionIndexToElasticService(new RegionDbRepository(), new RegionElasticsearchRepository(), new StoreRepository());
        },

        // Catalog attribute index
        CatalogAttributesIndexToDbService: () => {
            return new CatalogAttributesIndexToDbService(new CatalogAttributesMagentoDbRepository(), new CatalogAttributeDbRepository(), new CatalogProductDbRepository(), new StoreRepository());
        },
        CatalogAttributeIndexToElasticService: () => {
            return new CatalogAttributeIndexToElasticService(new CatalogAttributeDbRepository(), new CatalogAttributeElasticsearchRepository(), new StoreRepository());
        },








        /**
         * APP FRONTEND
         */

        // RegionAppService
        RegionAppService: () => {
            return new RegionAppService(new StoreRepository(), new RegionSearchRepository());
        },
        // CatalogCategoryAppService
        CatalogCategoryAppService: () => {
            return new CatalogCategoryAppService(new StoreRepository(), new CatalogCategorySearchRepository());
        },
        // CatalogProductAppService
        CatalogProductAppService: () => {
            return new CatalogProductAppService(new StoreRepository(), new CatalogProductSearchRepository());
        },
        // CatalogProductAppService
        CustomerAppService: () => {
            return new CustomerAppService(new StoreRepository(), new CustomerRepository());
        },
        // CatalogAttributeAppService
        CatalogAttributeAppService: () => {
            return new CatalogAttributeAppService(new StoreRepository(), new CatalogAttributeSearchRepository());
        },



    }
};

export default iocConfig;
