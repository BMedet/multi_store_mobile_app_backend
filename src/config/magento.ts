/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

const magentoConfig = {
    homePageUrl: {
        1: 'https://meloman.kz',
        3: 'https://marwin.kz',
        4: 'https://komfort.kz'
    },
    media: {
        plugImage: '/media/catalog/product/placeholder/default/Selection_01222_8.png',
        cdn: 'https://simg2.marwin.kz'
    },
    category: {
        thumbnailPathPrefix: '/media/catalog/category/'
    },
    product: {
        imagePrefix: '/media/catalog/product'
    },
    api: {
        baseUrl: process.env.MAGENTO_API_BASE_URL || 'https://meloman.kz',
        consumerKey: process.env.MAGENTO_API_CONSUMER_KEY,
        consumerSecret: process.env.MAGENTO_API_CONSUMER_SECRET,
        accessToken: process.env.MAGENTO_API_ACCESS_TOKEN,
        accessTokenSecret: process.env.MAGENTO_API_ACCESS_TOKEN_SECRET,
    },
};

export default magentoConfig;
