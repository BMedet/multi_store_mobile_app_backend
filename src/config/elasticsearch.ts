/**
 * Elasticsearch config
 *
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */


const elasticsearchConfig =
{
    url: `${process.env.ELASTICSEARCH_HOST || "localhost"}:${Number(process.env.ELASTICSEARCH_POST || 9200)}`,
    index: {substractDays: 2}
};




export default elasticsearchConfig;
