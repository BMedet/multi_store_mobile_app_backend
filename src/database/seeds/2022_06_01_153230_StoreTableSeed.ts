/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import BaseSchema from 'sosise-core/build/Database/BaseSchema';
import * as faker from 'faker';

/**
 * If you need more information, see: http://knexjs.org/#Schema ; https://www.npmjs.com/package/faker
 */
export default class StoreTableSeed extends BaseSchema {
    /**
     * Restrict running the seed only in a local environment (APP_ENV=local)
     */
    protected onlyInLocalEnvironment = false;

    /**
     * Table name where data should be inserted in
     */
    protected tableName = 'store';

    /**
     * Run seed
     */
    public async run(): Promise<void> {
        // Prepare data to seed
        const data: any = [
            {
                label: 'Меломан',
                entity_id: 1,
                code: 'meloman',
                base_url: 'https://meloman.kz',
                token: 'testToken',
                is_active: true,
            },
            {
                label: 'Марвин',
                entity_id: 3,
                code: 'marwin',
                base_url: 'https://marwin.kz',
                token: 'testToken',
                is_active: true,
            },
            {
                label: 'Комфорт',
                entity_id: 4,
                code: 'komfort',
                base_url: 'https://komfort.kz',
                token: 'testToken',
                is_active: true,
            }
        ];

        // Insert to table
        await this.dbConnection.table(this.tableName).insert(data);
    }
}
