/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CreateStoreTable extends BaseSchema {

    protected tableName = 'store';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('entity_id').unsigned().notNullable().index();
            table.string('label').notNullable();
            table.string('code').notNullable().index();
            table.string('base_url').notNullable();
            table.string('token', 32).index().notNullable();
            table.boolean('is_active').defaultTo(true).index();
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
