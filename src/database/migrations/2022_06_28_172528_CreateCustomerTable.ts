import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CreateCustomerAppDataTable extends BaseSchema {

    protected tableName = 'customer';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('store_entity_id').unsigned().index().references('entity_id').inTable('store').onDelete('CASCADE').onUpdate('CASCADE');
            table.boolean('is_authorized').defaultTo(false);
            table.string('mobile_uuid').notNullable().index();
            table.integer('region_entity_id').defaultTo(541).notNullable().references('entity_id').inTable('index_region').onDelete('CASCADE').onUpdate('CASCADE');
            table.string('theme').defaultTo('light').notNullable();
            table.string('listing_type').defaultTo('grid').notNullable();
            table.string('language').defaultTo('ru-RU').notNullable();
            table.dateTime('last_visit').notNullable().defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.dateTime('last_identificate').nullable();
            table.string('platform').nullable();
            table.string('platform_version').nullable();
            table.string('imei').nullable();
            table.string('model').nullable();
            table.string('manufacturer').nullable();
            table.string('serial').nullable();
            table.string('sdk_version').nullable();
            table.string('cordova_version').nullable();
            table.string('pin').nullable();
            table.boolean('is_enabled').defaultTo(true).index();
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            table.unique(['mobile_uuid', 'store_entity_id']);

        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
