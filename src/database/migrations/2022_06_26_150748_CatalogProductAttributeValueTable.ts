import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CatalogProductAttributeValueTable extends BaseSchema {

    protected tableName = 'index_product_attribute_value';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('attribute_id').unsigned().notNullable().references('attribute_id').inTable('index_product_attribute').onDelete('CASCADE').onUpdate('CASCADE');
            table.integer('product_entity_id').notNullable().references('entity_id').inTable('index_catalog_product').onDelete('CASCADE').onUpdate('CASCADE');
            table.string('value').index().nullable();
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
            table.unique(['attribute_id', 'product_entity_id'], { indexName: 'attribute_id_product_entity_id' });
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
