import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CreateCustomerInfoTable extends BaseSchema {

    protected tableName = 'customer_info';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('customer_app_data_id').unsigned().notNullable().references('id').inTable('customer').onDelete('CASCADE').onUpdate('CASCADE');
            table.integer('entity_id').unique().unsigned().notNullable().index();
            table.integer('loyalty_id').unique().unsigned().notNullable().index();
            table.string('name').nullable();
            table.string('last_name').nullable();
            table.string('patronymic').nullable();
            table.string('email').unique().nullable();
            table.string('mobile').unique().nullable();
            table.dateTime('dob').nullable();
            table.string('sex', 10).nullable();
            table.string('password').nullable();
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
