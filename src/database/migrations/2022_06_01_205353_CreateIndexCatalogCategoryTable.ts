/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CreateIndexCatalogCategoryTable extends BaseSchema {

    protected tableName = 'index_catalog_category';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('entity_id').unsigned().notNullable().index();
            table.string('name').notNullable();
            table.string('name_in_menu').nullable();
            table.string('url_path').nullable();
            table.integer('level').notNullable().unsigned();
            table.string('thumbnail_image').nullable();
            table.boolean('show_thumbnail').defaultTo(false);
            table.integer('parent_id').unsigned().notNullable().index();
            table.integer('position').unsigned();
            table.integer('store_entity_id').unsigned().index().references('entity_id').inTable('store').onDelete('CASCADE').onUpdate('CASCADE');
            table.boolean('is_active').defaultTo(true).index();
            table.boolean('is_hidden').defaultTo(false).index();
            table.boolean('include_in_menu').defaultTo(true).index();
            table.text('filterable_attributes').nullable();
            table.timestamp('magento_created_at');
            table.timestamp('magento_updated_at');
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
            table.unique(['entity_id', 'store_entity_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
