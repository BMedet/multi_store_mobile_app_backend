/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CreateIndexRegionTable extends BaseSchema {

    protected tableName = 'index_region';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('entity_id').notNullable().index();
            table.string('name').notNullable();
            table.integer('store_entity_id').unsigned().index().references('entity_id').inTable('store').onDelete('CASCADE').onUpdate('CASCADE');
            table.string('code').notNullable();
            table.integer('position').notNullable();
            table.boolean('is_active').notNullable().defaultTo(false).index();
            table.boolean('is_visible').notNullable().defaultTo(false).index();
            table.string('declension_name').nullable();
            table.text('polygon').nullable();
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
            table.unique(['entity_id', 'store_entity_id']);

        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
