/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import BaseSchema from 'sosise-core/build/Database/BaseSchema';

export default class CreateStoreLogTable extends BaseSchema {

    protected tableName = 'store_log';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('store_entity_id').unsigned().index();
            table.integer('level', 4).unsigned().notNullable().index();
            table.string('channel', 64).notNullable().index();
            table.text('message').notNullable();
            table.text('params').nullable();
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
