import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CreateProductAttributeOptionTable extends BaseSchema {

    protected tableName = 'product_attribute_option';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('attribute_id').unsigned().notNullable().references('attribute_id').inTable('index_product_attribute').onDelete('CASCADE').onUpdate('CASCADE');
            table.string('option_id').notNullable().index();
            table.string('value').index();
            table.integer('value_id').unsigned().notNullable().index();
            table.integer('sort_order').unsigned();
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
            table.unique(['attribute_id', 'value_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
