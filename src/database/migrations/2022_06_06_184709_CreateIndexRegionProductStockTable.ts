import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CreateIndexRegionProductStockTable extends BaseSchema {

    protected tableName = 'index_region_product_stock';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('region_id').unsigned().notNullable().references('id').inTable('index_region').onDelete('CASCADE').onUpdate('CASCADE');
            table.integer('product_id').unsigned().notNullable().references('id').inTable('index_catalog_product').onDelete('CASCADE').onUpdate('CASCADE');
            table.decimal('qty', 12, 4).notNullable().defaultTo(0);
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
            table.unique(['region_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
