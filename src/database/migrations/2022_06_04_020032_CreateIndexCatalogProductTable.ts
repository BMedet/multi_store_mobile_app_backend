/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class CreateIndexCatalogProductTable extends BaseSchema {

    protected tableName = 'index_catalog_product';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('entity_id').notNullable().index();
            table.string('name').notNullable();
            table.string('sku').notNullable().index();
            table.decimal('price', 20, 6).notNullable().defaultTo(0);
            table.decimal('special_price', 20, 6).nullable();
            table.decimal('old_price', 20, 6).nullable();
            table.text('description').nullable();
            table.integer('store_entity_id').unsigned().index().references('entity_id').inTable('store').onDelete('CASCADE').onUpdate('CASCADE');
            table.decimal('qty', 12, 4).notNullable();
            table.boolean('is_active').notNullable().index();
            table.boolean('is_visible').notNullable().index();
            table.string('thumbnail').nullable();
            table.text('images').nullable();
            table.string('url').nullable();
            table.string('category_ids').nullable().index();
            table.integer('sales_qty').notNullable().defaultTo(0).index();
            table.timestamp('magento_created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
            table.unique(['entity_id', 'store_entity_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
