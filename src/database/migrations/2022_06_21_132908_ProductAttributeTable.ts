import BaseSchema from 'sosise-core/build/Database/BaseSchema';

/**
 * If you need more information, see: http://knexjs.org/#Schema
 */
export default class ProductAttributeTable extends BaseSchema {

    protected tableName = 'index_product_attribute';

    /**
     * Run the migrations.
     */
    public async up(): Promise<void> {
        await this.dbConnection.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.boolean('enabled').defaultTo(true).index();
            table.integer('attribute_id').unique().unsigned().notNullable().index();
            table.string('attribute_code').notNullable().index();
            table.string('frontend_label').nullable();
            table.string('frontend_input').nullable();
            table.boolean('is_visible_on_front').defaultTo(false).index();
            table.boolean('is_visible_on_grid').defaultTo(false).index();
            table.boolean('is_filterable').defaultTo(false).index();
            table.boolean('is_multiselect').defaultTo(false);
            table.integer('position').defaultTo(0);
            table.string('backend_type').notNullable();
            table.timestamp('created_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP'));
            table.timestamp('updated_at').defaultTo(this.dbConnection.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')).index();
        });
    }

    /**
     * Reverse the migrations.
     */
    public async down(): Promise<void> {
        await this.dbConnection.schema.dropTable(this.tableName);
    }
}
