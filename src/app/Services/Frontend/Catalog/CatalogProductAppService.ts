/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import lodash, { isSet } from "lodash";
import Helper from "sosise-core/build/Helper/Helper";
import IOC from "sosise-core/build/ServiceProviders/IOC";
import ProductSourceEnum from "../../../Enums/ProductSourceEnum";
import StoreRepository from "../../../Repositories/Db/LocalStorage/Store/StoreRepository";
import CatalogProductSearchRepositoryInterface from "../../../Repositories/Elasticsearch/Search/Catalog/Product/CatalogProductSearchRepositoryInterface";
import CatalogProductSearchResultType from "../../../Types/Elasticsearch/CatalogProductSearchResultType";
import ProductFilterableAttributeType from "../../../Types/Frontend/ProductFilterableAttributeType";
import GetProductListUnifier from "../../../Unifiers/GetProductListUnifier";
import GetProductUnifier from "../../../Unifiers/GetProductUnifier";
import FrontendService from "../FrontendService";
import CatalogCategoryAppService from "./CatalogCategoryAppService";

export default class CatalogProductAppService extends FrontendService {

    /**
     * Product repository
     */
    private productRepository: CatalogProductSearchRepositoryInterface;

    /**
     * Catalog category app service
     */
    private categoryService: CatalogCategoryAppService;

    /**
     * Constructor
     */
    constructor(
        storeRepository: StoreRepository,
        productRepository: CatalogProductSearchRepositoryInterface
    ) {
        super(storeRepository);
        this.productRepository = productRepository;
        this.categoryService = IOC.make(CatalogCategoryAppService);
    }

    /**
     * Get products by source id
     */
    public async getProductsBySourceId(getProductListUnifier: GetProductListUnifier, source: ProductSourceEnum): Promise<CatalogProductSearchResultType> {

        // Set store
        await this.setStore(getProductListUnifier.storeId);

        switch (source) {
            case ProductSourceEnum.catalogCategory:
                return await this.getProductsByCategoryId(getProductListUnifier);

            default:
                return await this.getProductsByCategoryId(getProductListUnifier);
        }
    }

    /**
     * Get product by id
     */
    public async getProductById(getProductUnifier: GetProductUnifier): Promise<any> {

        // Set store
        await this.setStore(getProductUnifier.storeId);

        return await this.productRepository.getProductById(
            this.STORE,
            getProductUnifier.id,
            getProductUnifier.regionId
        );
    }

    /**
     * Get products by category id
     */
    protected async getProductsByCategoryId(getProductListUnifier: GetProductListUnifier): Promise<CatalogProductSearchResultType> {

        // Get category filterable attributes
        const category = await this.categoryService.getCategoryById(getProductListUnifier.sourceId, getProductListUnifier.storeId);
        const categoryFilterableAttributes = category?.filterableAttributes?.split(',') ?? [];

        // Get product by category id
        const products = await this.productRepository.getProductsByCategoryId(
            getProductListUnifier.sourceId,
            this.STORE, getProductListUnifier.regionId,
            getProductListUnifier.sortField,
            getProductListUnifier.page,
            getProductListUnifier.filter);

        products.filterableAttributes = await this.getFilterableAttributes(getProductListUnifier, ProductSourceEnum.catalogCategory, categoryFilterableAttributes);

        // Prepare filter
        for (const filterableAttribute of products.filterableAttributes) {
            if (getProductListUnifier.filter.attribute[filterableAttribute.attributeId]) {
                continue;
            }
            getProductListUnifier.filter.attribute[filterableAttribute.attributeId] = new Array();
        }
        products.filter = getProductListUnifier.filter;
        return products;
    }

    /**
     * Get filterable attributes
     */
    protected async getFilterableAttributes(getProductListUnifier: GetProductListUnifier, source: ProductSourceEnum, categoryFilterbaleAttributesId: string[]): Promise<ProductFilterableAttributeType[]> {

        let attributes = new Array();
        if (source === ProductSourceEnum.catalogCategory) {
            attributes = await this.productRepository.getProductsFilterableAttributesByCategoryId(
                getProductListUnifier.sourceId,
                this.STORE,
                getProductListUnifier.regionId,
                getProductListUnifier.filter);
        }

        const preparedAttributes = new Array();
        for (const attribute of attributes) {

            if (!categoryFilterbaleAttributesId.includes(String(attribute.attributeId)) || attribute.label === attribute.value) continue;

            // Prepare per attribute
            const preparedAttribute: ProductFilterableAttributeType = {
                attributeId: attribute.attributeId,
                attributeCode: attribute.attributeCode,
                frontendLabel: attribute.frontendLabel,
                frontendInput: attribute.frontendInput,
                isMultiselect: attribute.isMultiselect,
                options: [{
                    value: attribute.value,
                    label: attribute.label.charAt(0).toUpperCase() + attribute.label.slice(1),
                    count: 1
                }]
            };

            const issetAttribute = lodash.find(preparedAttributes, function (o) { return o.attributeId === preparedAttribute.attributeId });

            if (issetAttribute) {
                this.concatOptions(issetAttribute, preparedAttribute);
            } else {
                preparedAttributes.push(preparedAttribute);
            }
        }

        return preparedAttributes;
    }

    /**
     * Concat attribute options 
     */
    private concatOptions(issetAttribute: ProductFilterableAttributeType, preparedAttribute: ProductFilterableAttributeType): void {
        const issetOption = lodash.find(issetAttribute.options, function (o) { return o.value === preparedAttribute.options[0].value });
        if (issetOption) {
            issetOption.count++;
        } else {
            issetAttribute.options.push(preparedAttribute.options[0]);
        }
        issetAttribute.options = lodash.sortBy(issetAttribute.options, ['label'])
    }
}
