/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import CatalogAttributeDbRepositoryInterface from "../../../Repositories/Elasticsearch/Search/Catalog/Attribute/CatalogAttributeSearchRepositoryInterface";
import StoreRepository from "../../../Repositories/Db/LocalStorage/Store/StoreRepository";
import CatalogCategorySearchType from "../../../Types/Elasticsearch/CatalogCategorySearchType";
import FrontendService from "../FrontendService";
import { isNull } from "lodash";
import FilterableAttributeType from "../../../Types/Elasticsearch/FilterableAttributeType";

export default class CatalogAttributeAppService extends FrontendService {

    /**
     * Attribute repository
     */
    private attributeRepository: CatalogAttributeDbRepositoryInterface;


    /**
     * Constructor
     */
    constructor(
        storeRepository: StoreRepository,
        attributeRepository: CatalogAttributeDbRepositoryInterface
    ) {
        super(storeRepository);
        this.attributeRepository = attributeRepository;
    }

    /**
     * Get filterable attributes by ids  @todo DELETE THIS METHOD??? NOT USED
     */
    public async getFilterableAttributesByIds(category: CatalogCategorySearchType | null): Promise<FilterableAttributeType[] | []> {
        // Check is null
        if(isNull(category) || isNull(category.filterableAttributes) ) return [];

        // Get attributes by id
        return this.attributeRepository.getAttributesById(category.filterableAttributes.split(','));
    }

    
}
