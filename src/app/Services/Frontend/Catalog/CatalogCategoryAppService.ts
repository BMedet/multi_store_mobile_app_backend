/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import StoreRepository from "../../../Repositories/Db/LocalStorage/Store/StoreRepository";
import CatalogCategorySearchRepositoryInterface from "../../../Repositories/Elasticsearch/Search/Catalog/Category/CatalogCategorySearchRepositoryInterface";
import CatalogCategorySearchType from "../../../Types/Elasticsearch/CatalogCategorySearchType";
import FrontendService from "../FrontendService";

export default class CatalogCategoryAppService extends FrontendService {

    /**
     * Category repository
     */
    private categoryRepository: CatalogCategorySearchRepositoryInterface;


    /**
     * Constructor
     */
    constructor(
        storeRepository: StoreRepository,
        categoryRepository: CatalogCategorySearchRepositoryInterface
    ) {
        super(storeRepository);
        this.categoryRepository = categoryRepository;
    }

    /**
     * Get all active categories tree
     */
    public async getCategoriesTree(storeId: number): Promise<CatalogCategorySearchType[]> {

        // Set store
        await this.setStore(storeId);

        try {
            // Get all active regions for this store
            const categories = await this.categoryRepository.getAllActiveCategories(this.STORE);

            // Prepare tree
            return this.prepareCategoriesTree(categories);

        } catch (e) {
            await this.loggerService.error(`[GET CATEGORIES TREE] Error occured when get categories tree`, this.loggingChannel, null, this.STORE.entityId);
            return [];
        }
    }

    /**
     * Get category by id
     */
    public async getCategoryById(categoryId: number, storeId: number): Promise<CatalogCategorySearchType | null> {

        // Set store
        await this.setStore(storeId);

        try {
            // Get category by id
            const category = await this.categoryRepository.getCategoryById(categoryId, this.STORE);

            return category;

        } catch (e) {
            await this.loggerService.error(`[GET CATEGORY BY ID] Error occured when get category by id`, this.loggingChannel, null, this.STORE.entityId);
            return null;
        }
    }

    /**
     * Prepare categories tree recursively
     */
    protected prepareCategoriesTree(categories: CatalogCategorySearchType[], parentId: number = 47): CatalogCategorySearchType[]  {
        const tree = new Array();
        for (const category of categories) {
            if(!category.includeInMenu) continue;
            if (category.parentId === parentId) {
                category.children = this.prepareCategoriesTree(categories, category.entityId);
                tree.push(category);
            }
        }
        return tree;
    }
}
