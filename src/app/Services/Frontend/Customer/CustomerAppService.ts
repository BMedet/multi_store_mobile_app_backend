/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import IOC from "sosise-core/build/ServiceProviders/IOC";
import CustomerRepositoryInterface from "../../../Repositories/Db/LocalStorage/Customer/CustomerRepositoryInterface";
import StoreRepository from "../../../Repositories/Db/LocalStorage/Store/StoreRepository";
import InitType from "../../../Types/Frontend/InitType";
import CustomerInitType from "../../../Types/LocalStorage/CustomerInitType";
import CustomerInitUnifier from "../../../Unifiers/CustomerInitUnifier";
import CustomerUpdateUnifier from "../../../Unifiers/CustomerUpdateUnifier";
import CatalogCategoryAppService from "../Catalog/CatalogCategoryAppService";
import FrontendService from "../FrontendService";
import RegionAppService from "../Region/RegionAppService";

export default class CustomerAppService extends FrontendService {

    /**
     * Customer repository
     */
    private customerRepository: CustomerRepositoryInterface;
    private catalogCategoryAppService: CatalogCategoryAppService;
    private regionAppService: RegionAppService;


    /**
     * Constructor
     */
    constructor(
        storeRepository: StoreRepository,
        customerRepository: CustomerRepositoryInterface
    ) {
        super(storeRepository);
        this.customerRepository = customerRepository;
        this.catalogCategoryAppService = IOC.make(CatalogCategoryAppService);
        this.regionAppService = IOC.make(RegionAppService);
    }

    /**
     * Init customer
     */
    public async init(customerInitUnifier: CustomerInitUnifier): Promise<InitType | null> {

        // Set store
        await this.setStore(customerInitUnifier.storeId);

        // @todo сдeлать асинхронно!!!!!!!
        const customerData = await this.customerRepository.init(customerInitUnifier);
        const regionOptions = await this.regionAppService.getAllRegions(customerInitUnifier.storeId);
        const categoryTree = await this.catalogCategoryAppService.getCategoriesTree(customerInitUnifier.storeId);

        return {
            customerData, regionOptions, categoryTree
        };
    }

    /**
     * Update customer
     */
    public async update(customerUpdateUnifier: CustomerUpdateUnifier): Promise<CustomerInitType> {

        // Set store
        await this.setStore(customerUpdateUnifier.storeId);

        // Update customer data
        const customerData = await this.customerRepository.update(customerUpdateUnifier);

        return customerData
    }
}
