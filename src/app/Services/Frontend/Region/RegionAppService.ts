/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import Helper from "sosise-core/build/Helper/Helper";
import StoreRepository from "../../../Repositories/Db/LocalStorage/Store/StoreRepository";
import RegionSearchRepositoryInterface from "../../../Repositories/Elasticsearch/Search/Region/RegionSearchRepositoryInterface";
import RegionSearchType from "../../../Types/Elasticsearch/RegionSearchType";
import FrontendService from "../FrontendService";

export default class RegionAppService extends FrontendService {

    /**
     * Region repository
     */
    private regionRepository: RegionSearchRepositoryInterface;


    /**
     * Constructor
     */
    constructor(
        storeRepository: StoreRepository,
        regionRepository: RegionSearchRepositoryInterface
    ) {
        super(storeRepository);
        this.regionRepository = regionRepository;
    }
    /**
     * Get all regions
     */
    public async getAllRegions(storeId: number): Promise<RegionSearchType[]> {

        // Set store
        await this.setStore(storeId);

        try {
            // Get all active regions for this store
            return this.regionRepository.getAllActiveRegions(this.STORE);

        } catch (e) {
            await this.loggerService.error(`[GET ALL REGIONS] Error occured when get all active regions`, this.loggingChannel, null, this.STORE.entityId);
            return [];
        }
    }
}
