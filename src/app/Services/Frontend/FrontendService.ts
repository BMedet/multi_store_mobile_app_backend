/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import IOC from "sosise-core/build/ServiceProviders/IOC";
import StoreRepositoryInterface from "../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import StoreType from "../../Types/LocalStorage/StoreType";
import CustomLoggerService from "../Logger/CustomLoggerService";

export default class FrontendService {
    /**
     * Store repository
     */
    protected storeRepository: StoreRepositoryInterface;
    /**
     * Logger
     */
    protected loggerService: CustomLoggerService;
    /**
     * Logger
     */
    protected STORE: StoreType;

    /**
     * Logging channel
     */
    protected loggingChannel: 'frontend';


    /**
     * Constructor
     */
    constructor(storeRepository: StoreRepositoryInterface) {
        this.storeRepository = storeRepository;
        this.loggerService = IOC.make(CustomLoggerService);
    }

    /**
     * Set store
     */
    protected async setStore(storeEntityId: number | string): Promise<void> {

        this.STORE = await this.storeRepository.getActiveStoreByEntityId(storeEntityId);
    }
}
