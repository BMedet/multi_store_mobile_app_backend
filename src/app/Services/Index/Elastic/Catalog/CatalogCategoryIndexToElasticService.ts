/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import IndexTypeEnum from "../../../../Enums/IndexTypeEnum";
import IndexToLocalStorageRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Index/LocalStorageIndexRepositoryInterface";
import StoreRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import AbstractElasticsearchIndexRepository from "../../../../Repositories/Elasticsearch/Index/AbstractElasticsearchIndexRepository";
import AbstractIndexToElasticService from "../AbstractIndexToElasticService";

export default class CatalogCategoryIndexToElasticService extends AbstractIndexToElasticService {
    protected INDEX_TYPE = IndexTypeEnum.catalogCategory;

    /**
     * Constructor
     */
    constructor(
        catalogCategoryDbRepository: IndexToLocalStorageRepositoryInterface,
        catalogCategoryElasticRepository: AbstractElasticsearchIndexRepository,
        storeRepository: StoreRepositoryInterface
    ) {
        super(catalogCategoryDbRepository, catalogCategoryElasticRepository, storeRepository);
    }
}
