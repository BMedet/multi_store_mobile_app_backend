/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import Helper from "sosise-core/build/Helper/Helper";
import IOC from "sosise-core/build/ServiceProviders/IOC";
import storeConfig from "../../../../../config/store";
import IndexTypeEnum from "../../../../Enums/IndexTypeEnum";
import StoreCodeEnum from "../../../../Enums/StoreCodeEnum";
import CatalogAttributeDbRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Index/Catalog/Attribute/CatalogAttributeDbRepositoryInterface";
import StoreRepository from "../../../../Repositories/Db/LocalStorage/Store/StoreRepository";
import AbstractElasticsearchIndexRepository from "../../../../Repositories/Elasticsearch/Index/AbstractElasticsearchIndexRepository";
import CatalogAttributeElasticsearchRepository from "../../../../Repositories/Elasticsearch/Index/Catalog/CatalogAttributeElasticsearchRepository";
import CustomLoggerService from "../../../Logger/CustomLoggerService";
import AbstractIndexToElasticService from "../AbstractIndexToElasticService";


export default class CatalogAttributeIndexToElasticService extends AbstractIndexToElasticService {

    protected dbRepository: CatalogAttributeDbRepositoryInterface;
    protected elasticRepository: CatalogAttributeElasticsearchRepository;
    protected logger: CustomLoggerService;
    protected loggingChannel: string = 'index';
    protected IS_REINDEX = false;
    protected INDEX_TYPE = IndexTypeEnum.catalogAttribute;

    /**
     * Constructor
     */
    constructor(
        dbRepository: CatalogAttributeDbRepositoryInterface,
        elasticRepository: CatalogAttributeElasticsearchRepository,
        storeRepository: StoreRepository
    ) {
        super(dbRepository, elasticRepository, storeRepository);
        this.dbRepository = dbRepository;
        this.elasticRepository = elasticRepository;
        this.logger = IOC.make(CustomLoggerService);
    }

    /**
     * Index
     */
    public async index(storeCode: StoreCodeEnum = StoreCodeEnum.all, isForce: boolean = true) {
        // Set store
        this.store = storeConfig.defaultStoreType;

        // Set 'is force' variable
        this.IS_FORCE = isForce;

        // // Index attributes
        // await this.indexAttribute(); // @todo uncomment

        // // Index attribute options
        // await this.indexAttributeOption(); // @todo uncomment

        // Bind products
        await this.bindAttributeToCatalogProduct();
    }

    /**
     * Index attributes
     */
    protected async indexAttribute(): Promise<void> {
        this.INDEX_TYPE = IndexTypeEnum.catalogAttribute;
        await this.makeIndex();
    }

    /**
     * Index attribute options
     */
    protected async indexAttributeOption(): Promise<void> {
        this.INDEX_TYPE = IndexTypeEnum.catalogAttributeOption;
        this.elasticRepository.setIndexType(IndexTypeEnum.catalogAttributeOption);
        await this.makeIndex();
    }

    /**
     * Run indexer
     */
    protected async makeIndex(): Promise<void> {

        // Logging
        await this.logger.info(`[${this.store.code}] Start "${this.INDEX_TYPE}" index to Elastic`, this.loggingChannel, null, this.store.entityId);

        // Generate url
        await this.generateIndexPath();

        // Index enabled indexes
        await this.indexToElastic();

        // Add new index to alias
        await this.updateAlias();

        // Remove old index
        await this.deleteOldIndex();

        // Logging
        await this.logger.info(`[${this.store.code}] Finish "${this.INDEX_TYPE}" index to Elastic`, this.loggingChannel, null, this.store.entityId);
    }

    /**
     * Index to elastic
     */
    protected async indexToElastic() {

        // Logging
        await this.logger.info(`[${this.store.code}] Upload to elastic`, this.loggingChannel, null, this.store.entityId);

        let canIterate = true;
        let skipCount = 0;
        const perIterationNumber = 500;
        while (canIterate) {

            // Get Items from db
            const itemsForIndex = await this.getItemsForIndex(skipCount, perIterationNumber);

            if (!itemsForIndex.length) return;

            try {
                // Index to elastic
                await this.elasticRepository.index(itemsForIndex, this.NEW_INDEX_PATH, this.store);
            } catch (error) {

                // Logging
                await this.logger.critical(`[${this.store.code}] An error occurred while indexing "${this.INDEX_TYPE}", "${this.NEW_INDEX_PATH}" index will be deleted`, this.loggingChannel, { error }, this.store.entityId);

                // Delete index
                await this.elasticRepository.deleteIndex(this.NEW_INDEX_PATH, this.store);

                process.exit(1);
            }

            // Check can iterate
            canIterate = itemsForIndex.length === perIterationNumber;

            // Paginate
            skipCount += perIterationNumber;
        }
    }

    /**
     * Get items for index (attribute | attributeOption)
     */
    private async getItemsForIndex(skipCount: number, perIterationNumber: number): Promise<any[]> {

        if (this.INDEX_TYPE === IndexTypeEnum.catalogAttribute) {
            return await this.localStorageIndexRepository.getUpdatedInLastDateIndexes(skipCount, perIterationNumber, this.IS_FORCE, this.store);
        }
        return await this.dbRepository.getAttributeOptions(skipCount, perIterationNumber, this.IS_FORCE);
    }

    /**
     * Index attribute values
     */
    protected async bindAttributeToCatalogProduct() {
        // Logging
        await this.logger.info(`[${this.store.code}] Binding attribute to catalog product`, this.loggingChannel, null, this.store.entityId);

        // Iterate indicator
        let canIterate = true;

        // First page number
        let pageNumber = 1;

        // Per page product count (limit)
        const productCountOnPerPage = 500;

        while (canIterate) {

            // Get attribue value with product entity id
            const attributesWithProductEntityId = await this.dbRepository.getAttributeValueWithProductEntityId(productCountOnPerPage, pageNumber);

            // Get active stores
            const stores = await this.storeRepository.getAllActiveStores();

            // Index
            await this.elasticRepository.bindAttributesToProduct(attributesWithProductEntityId, stores);

            // Check can iterate
            canIterate = Object.keys(attributesWithProductEntityId).length > 0;

            // Paginate
            pageNumber++;
        }
        
        // Logging
        await this.logger.info(`[${this.store.code}] Binding an attribute to a catalog product is completed`, this.loggingChannel, null, this.store.entityId);
    }
}
