/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import { isNull } from "lodash";
import IndexTypeEnum from "../../../../Enums/IndexTypeEnum";
import IndexToLocalStorageRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Index/LocalStorageIndexRepositoryInterface";
import StoreRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import AbstractElasticsearchIndexRepository from "../../../../Repositories/Elasticsearch/Index/AbstractElasticsearchIndexRepository";
import AbstractIndexToElasticService from "../AbstractIndexToElasticService";

export default class RegionIndexToElasticService extends AbstractIndexToElasticService {

    protected INDEX_TYPE = IndexTypeEnum.region;

    /**
     * Constructor
     */
    constructor(
        localStorageRepository: IndexToLocalStorageRepositoryInterface,
        elasticRepository: AbstractElasticsearchIndexRepository,
        storeRepository: StoreRepositoryInterface
    ) {
        super(localStorageRepository, elasticRepository, storeRepository);
    }

}
