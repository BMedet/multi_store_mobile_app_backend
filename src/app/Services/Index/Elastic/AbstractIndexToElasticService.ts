/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import { isNull } from "lodash";
import IndexTypeEnum from "../../../Enums/IndexTypeEnum";
import LocalStorageIndexRepositoryInterface from "../../../Repositories/Db/LocalStorage/Index/LocalStorageIndexRepositoryInterface";
import StoreRepositoryInterface from "../../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import AbstractElasticsearchIndexRepository from "../../../Repositories/Elasticsearch/Index/AbstractElasticsearchIndexRepository";
import AbstractIndexService from "../AbstractIndexService";

export default abstract class AbstractIndexToElasticService extends AbstractIndexService {
    protected elasticRepository: AbstractElasticsearchIndexRepository;
    protected localStorageIndexRepository: LocalStorageIndexRepositoryInterface;
    protected NEW_INDEX_PATH: string;
    protected CURRENT_INDEX_PATH: string | null;
    protected IS_REINDEX = false;

    protected abstract INDEX_TYPE: IndexTypeEnum;

    /**
     * Constructor
     */
    constructor(
        localStorageIndexRepository: LocalStorageIndexRepositoryInterface,
        elasticRepository: AbstractElasticsearchIndexRepository,
        storeRepository: StoreRepositoryInterface
    ) {
        super(storeRepository);
        this.elasticRepository = elasticRepository;
        this.localStorageIndexRepository = localStorageIndexRepository;
    }

    /**
     * Index per one store catalog categories
     */
    protected async indexPerOneStore(): Promise<void> {

        // Logging
        await this.logger.info(`[${this.store.code}] Start "${this.INDEX_TYPE}" index to Elastic`, this.loggingChannel, null, this.store.entityId);

        // Generate url
        await this.generateIndexPath();

        // Index enabled indexes
        await this.indexToElastic();

        // Add new index to alias
        await this.updateAlias();

        // Remove old index
        await this.deleteOldIndex();

        // Update settings
        await this.elasticRepository.updateLimitSettings(this.store);

        // Logging
        await this.logger.info(`[${this.store.code}] Finish "${this.INDEX_TYPE}" index to Elastic`, this.loggingChannel, null, this.store.entityId);
    }

    /**
     * Generate index url
     */
    protected async generateIndexPath(): Promise<void> {

        // Get current index URL
        this.CURRENT_INDEX_PATH = await this.elasticRepository.getCurrentIndexPath(this.store);

        // Set is_reindex flag
        this.IS_REINDEX = !isNull(this.CURRENT_INDEX_PATH);

        if (!this.IS_REINDEX || !this.IS_FORCE) {
            // Create new index
            this.NEW_INDEX_PATH = await this.elasticRepository.createIndex(this.store);
        } else {

            // Clone index
            this.NEW_INDEX_PATH = await this.elasticRepository.cloneCurrentIndex(this.store);

        }

        // Logging
        await this.logger.info(`[${this.store.code}] Generate new index path`, this.loggingChannel, {
            isReindex: this.IS_REINDEX,
            currentIndexPath: this.CURRENT_INDEX_PATH,
            newIndexPath: this.NEW_INDEX_PATH,
            force: this.IS_FORCE
        }, this.store.entityId);
    }

    /**
     * Index to elastic
     */
    protected async indexToElastic() {

        // Logging
        await this.logger.info(`[${this.store.code}]  Upload to elastic`, this.loggingChannel, null, this.store.entityId);

        let canIterate = true;
        let skipCount = 0;
        const perIterationNumber = 500;
        while (canIterate) {

            // Get Items from db
            const itemsForIndex = await this.localStorageIndexRepository.getUpdatedInLastDateIndexes(skipCount, perIterationNumber, this.IS_FORCE, this.store);

            if (!itemsForIndex.length) return;

            try {
                // Index to elastic
                await this.elasticRepository.index(itemsForIndex, this.NEW_INDEX_PATH, this.store);
            } catch (error) {

                // Logging
                await this.logger.critical(`[${this.store.code}] An error occurred while indexing "${this.INDEX_TYPE}", "${this.NEW_INDEX_PATH}" index will be deleted`, this.loggingChannel, { error }, this.store.entityId);

                // Delete index
                await this.elasticRepository.deleteIndex(this.NEW_INDEX_PATH, this.store);

                process.exit(1);
            }

            // Check can iterate
            canIterate = itemsForIndex.length === perIterationNumber;

            // Paginate
            skipCount += perIterationNumber;
        }
    }

    /**
     * Add new index to alias
     */
    protected async updateAlias() {

        // Logging
        await this.logger.info(`[${this.store.code}] Update alias`, this.loggingChannel, null, this.store.entityId);

        // Add index to alias
        await this.elasticRepository.updateAlias(this.CURRENT_INDEX_PATH, this.NEW_INDEX_PATH, this.store);
    }

    /**
     * Delete old index
     */
    protected async deleteOldIndex() {

        if (isNull(this.CURRENT_INDEX_PATH)) return;

        // Logging
        await this.logger.info(`[${this.store.code}] Delete old index path`, this.loggingChannel, null, this.store.entityId);

        // Delete old index
        await this.elasticRepository.deleteIndex(this.CURRENT_INDEX_PATH, this.store);
    }
}
