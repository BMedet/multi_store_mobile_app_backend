/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import StoreRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import IndexTypeEnum from "../../../../Enums/IndexTypeEnum";
import AbstractIndexToDbService  from "../AbstractIndexToDbService";
import CatalogCategoryDbRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Index/Catalog/Category/CatalogCategoryDbRepositoryInterface";
import CatalogCategoryMagentoDbRepositoryInterface from "../../../../Repositories/Db/Magento/Catalog/Category/CatalogCategoryMagentoDbRepositoryInterface";
export default class CatalogCategoryIndexToDbService extends AbstractIndexToDbService {

    protected INDEX_TYPE = IndexTypeEnum.catalogCategory;
    protected magentoRepository: CatalogCategoryMagentoDbRepositoryInterface;
    protected localStorageRepository: CatalogCategoryDbRepositoryInterface;
    /**
     * Constructor
     */
    constructor(
        magentoRepository: CatalogCategoryMagentoDbRepositoryInterface,
        catalogCategoryDbRepository: CatalogCategoryDbRepositoryInterface,
        storeRepository: StoreRepositoryInterface
    ) {
        super(storeRepository);
        this.magentoRepository = magentoRepository;
        this.localStorageRepository = catalogCategoryDbRepository;
    }

}
