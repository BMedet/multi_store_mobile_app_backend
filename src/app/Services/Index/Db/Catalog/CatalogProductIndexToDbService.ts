/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import IndexTypeEnum from "../../../../Enums/IndexTypeEnum";
import lodash from "lodash";
import StoreRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import CatalogProductMagentoDbRepositoryInterface from "../../../../Repositories/Db/Magento/Catalog/Product/CatalogProductMagentoDbRepositoryInterface";
import AbstractIndexToDbService from "../AbstractIndexToDbService";
import CatalogProductDbRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Index/Catalog/Product/CatalogProductDbRepositoryInterface";
import ProductIdsType from "../../../../Types/LocalStorage/ProductIdsType";
import RegionIdsType from "../../../../Types/LocalStorage/RegionIdsType";
import Helper from "sosise-core/build/Helper/Helper";

export default class CatalogProductIndexToDbService extends AbstractIndexToDbService {

    protected magentoRepository: CatalogProductMagentoDbRepositoryInterface;
    protected localStorageRepository: CatalogProductDbRepositoryInterface;
    protected INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.catalogProduct;
    /**
     * Constructor
     */
    constructor(
        magentoRepository: CatalogProductMagentoDbRepositoryInterface,
        localStorageRepository: CatalogProductDbRepositoryInterface,
        storeRepository: StoreRepositoryInterface
    ) {
        super(storeRepository);
        this.magentoRepository = magentoRepository;
        this.localStorageRepository = localStorageRepository;
    }

    /**
     * Index per one store items
     */
    protected async indexPerOneStore(): Promise<void> {

        // Logging
        await this.logger.info(`[${this.store.code}] Start "${this.INDEX_TYPE}" index to db`, this.loggingChannel, null, this.store.entityId);

        // Update index
        const updatedItemEntityIds = await this.updateActiveIndex();

        // Disable index
        await this.disableInactiveIndex(updatedItemEntityIds);

        // Set prices
        await this.setPrices(updatedItemEntityIds);

        // Set sales data
        await this.setSalesInfo(updatedItemEntityIds);

        // Set content (images and description)
        await this.setContent(updatedItemEntityIds);

        // Set region data (qty and price)
        await this.setRegionData(updatedItemEntityIds);

        // Logging
        await this.logger.info(`[${this.store.code}] Finish "${this.INDEX_TYPE}" index to db`, this.loggingChannel, null, this.store.entityId);
    }

    /**
     * Get prices
     */
    private async setPrices(updatedItemEntityIds: number[]): Promise<void> {
        // Logging
        await this.logger.info(`[${this.store.code}] Set "${this.INDEX_TYPE}" index items price`, this.loggingChannel, null, this.store.entityId);

        // Get prices
        const productIdsWithPrice = await this.magentoRepository.getProductPrices(updatedItemEntityIds, this.store);

        // Set prices
        for (const perChunkItems of lodash.chunk(productIdsWithPrice, 1000)) {
            await this.localStorageRepository.setProductPrices(perChunkItems, this.store);
        }
    }

    /**
     * Get sales data
     */
    private async setSalesInfo(updatedItemEntityIds: number[]): Promise<void> {
        // Logging
        await this.logger.info(`[${this.store.code}] Set "${this.INDEX_TYPE}" index items sales info`, this.loggingChannel, null, this.store.entityId);

        // Get prices
        const productIdsWithPrice = await this.magentoRepository.getSalesInfo(updatedItemEntityIds, this.store);

        // Set prices
        for (const perChunkItems of lodash.chunk(productIdsWithPrice, 1000)) {
            await this.localStorageRepository.setProductSalesInfo(perChunkItems, this.store);
        }
    }

    /**
     * Set content (images and description)
     */
    private async setContent(updatedItemEntityIds: number[]): Promise<void> {
        // Logging
        await this.logger.info(`[${this.store.code}] Set "${this.INDEX_TYPE}" index items content`, this.loggingChannel, null, this.store.entityId);

        // Get content
        const productIdsContent = await this.magentoRepository.getProductContent(updatedItemEntityIds, this.store);

        // Set content
        for (const perChunkItems of lodash.chunk(productIdsContent, 1000)) {
            await this.localStorageRepository.setProductContent(perChunkItems, this.store);
        }
    }

    /**
     * Set region data (qty & price)
     */
    private async setRegionData(updatedItemEntityIds: number[]): Promise<void> {
        // Logging
        await this.logger.info(`[${this.store.code}] Set "${this.INDEX_TYPE}" index items region data`, this.loggingChannel, null, this.store.entityId);


        // Get store regions
        const regions = await this.getRegionsWithWarehouse();

        // Update product region data
        let canIterate = true;
        const perIterateNumber = 500;
        let paginator = 0;
        let ttt = 0
        while (canIterate) {
            // Get products from local storage
            const localProducts = await this.localStorageRepository.getAllProductIds(updatedItemEntityIds, paginator, perIterateNumber, this.store);
            ttt += localProducts.length;

            if (!localProducts.length) {
                break;
            }
            // Set region stock data
            await this.setRegionStockData(localProducts, regions);

            // Set region price data
            await this.setRegionPriceData(localProducts, regions);

            if (localProducts.length < perIterateNumber) {
                canIterate = false;
            }
            paginator += perIterateNumber;
        }
    }

    /**
     * Set region stock data
     */
    private async setRegionStockData(products: ProductIdsType[], regions: RegionIdsType[]) {

        // Get stock data from magento
        const productsWidthRegionStockData = await this.magentoRepository.getRegionStockData(products, regions, this.store);
        try {
            // Save to local storage
            this.localStorageRepository.setProductRegionStockData(productsWidthRegionStockData);
        } catch (e) {
            // Logging
            await this.logger.error('ERROR', this.loggingChannel, productsWidthRegionStockData, this.store.entityId);
        }

    }

    /**
     * Get regions
     */
    private async getRegionsWithWarehouse(): Promise<RegionIdsType[]> {

        // Get regions from local storage
        const regions = await this.localStorageRepository.getAllRegionEntityIds(this.store);

        // Add warehoise ids
        return await this.magentoRepository.getRegionStockIds(regions);
    }

    /**
     * Set region price data
     */
    private async setRegionPriceData(products: ProductIdsType[], regions: RegionIdsType[]) {
        // Get price data from magento
        const productsWidthRegionPriceData = await this.magentoRepository.getRegionPriceData(products, regions, this.store);

        // Save to local storage
        await this.localStorageRepository.setProductRegionPriceData(productsWidthRegionPriceData);
    }
}
