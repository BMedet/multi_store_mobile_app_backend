/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import IndexTypeEnum from "../../../../Enums/IndexTypeEnum";
import StoreRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import StoreCodeEnum from "../../../../Enums/StoreCodeEnum";
import CatalogAttributesMagentoDbRepositoryInterface from "../../../../Repositories/Db/Magento/Catalog/Attribute/CatalogAttributesMagentoDbRepositoryInterface";
import CatalogAttributeDbRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Index/Catalog/Attribute/CatalogAttributeDbRepositoryInterface";
import CustomLoggerService from "../../../Logger/CustomLoggerService";
import IOC from "sosise-core/build/ServiceProviders/IOC";
import StoreType from "../../../../Types/LocalStorage/StoreType";
import storeConfig from "../../../../../config/store";
import lodash from "lodash";
import CatalogProductDbRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Index/Catalog/Product/CatalogProductDbRepositoryInterface";
import CatalogAttributeBackendTypeEnum from "../../../../Enums/CatalogAttributeBackendTypeEnum";

export default class CatalogAttributesIndexToDbService {

    protected magentoRepository: CatalogAttributesMagentoDbRepositoryInterface;
    protected attributeLocalStorageRepository: CatalogAttributeDbRepositoryInterface;
    protected productLocalStorageRepository: CatalogProductDbRepositoryInterface;
    protected storeRepository: StoreRepositoryInterface;
    protected logger: CustomLoggerService;
    protected INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.catalogAttribute;
    protected loggingChannel: string = 'index';
    protected store: StoreType = storeConfig.defaultStoreType;

    /**
     * Constructor
     */
    constructor(
        magentoRepository: CatalogAttributesMagentoDbRepositoryInterface,
        attributeLocalStorageRepository: CatalogAttributeDbRepositoryInterface,
        productLocalStorageRepository: CatalogProductDbRepositoryInterface,
        storeRepository: StoreRepositoryInterface
    ) {
        this.storeRepository = storeRepository;
        this.magentoRepository = magentoRepository;
        this.attributeLocalStorageRepository = attributeLocalStorageRepository;
        this.productLocalStorageRepository = productLocalStorageRepository;
        this.logger = IOC.make(CustomLoggerService);
    }


    /**
     * Index catalog attributes to db
     */
    public async index() {

        // Index Attributes
        const attributeIds = await this.indexAttributes();

        // Index attribute values
        await this.indexAttributeOptions(attributeIds);

        // Bind products
        await this.bindAttributeToCatalogProduct();
    }

    /**
     * Index attributes
     */
    protected async indexAttributes() {

        // Logging
        await this.logger.info(`[${this.store.code}] Start "${this.INDEX_TYPE}" index to db`, this.loggingChannel, null, this.store.entityId);

        // Get attributes
        const attributes = await this.magentoRepository.getAllActiveAttributes();

        // Save attributes
        const attributeIds = await this.attributeLocalStorageRepository.index(attributes, this.store);

        // Disable inactive attributes
        await this.attributeLocalStorageRepository.disableInactiveIndexes(attributeIds, this.store);

        return attributeIds;
    }

    /**
     * Index attribute options
     */
    protected async indexAttributeOptions(attributeIds: number[]): Promise<void> {
        // Logging
        await this.logger.info(`[${this.store.code}] Start "attribute options" index to db`, this.loggingChannel, null, this.store.entityId);

        // Chunk
        const chunkedAttributeIds = lodash.chunk(attributeIds, 100);

        for (const perOneChunkAttributeIds of chunkedAttributeIds) {

            // Get options
            const options = await this.magentoRepository.getAttributeOptions(perOneChunkAttributeIds);

            // Save attributes
            await this.attributeLocalStorageRepository.saveAttributeOptions(options);
        }
    }

    /**
     * Index attribute values
     */
    protected async bindAttributeToCatalogProduct() {
        // Logging
        await this.logger.info(`[${this.store.code}] Binding attribute to catalog product`, this.loggingChannel, null, this.store.entityId);

        // Get all active attribute ids
        const allActiveVarcharAttributes = await this.attributeLocalStorageRepository.getAllActiveAttributeIds(CatalogAttributeBackendTypeEnum.varchar);
        const allActiveIntAttributes = await this.attributeLocalStorageRepository.getAllActiveAttributeIds(CatalogAttributeBackendTypeEnum.int);
        const catalogProductEntityIds = await this.productLocalStorageRepository.getAllStoresActiveUniqueProductIds();

        const chunkedIds = lodash.chunk(catalogProductEntityIds, 100);

        for (const perChunkedIds of chunkedIds) {

            // Get int type attrubites
            const intAttributeValues = await this.magentoRepository.getAttributeValues(perChunkedIds, allActiveIntAttributes, CatalogAttributeBackendTypeEnum.int);

            // Get varchar type attrubites
            const varcharAttributeValues = await this.magentoRepository.getAttributeValues(perChunkedIds, allActiveVarcharAttributes, CatalogAttributeBackendTypeEnum.varchar);

            const mergedValues = intAttributeValues.concat(varcharAttributeValues);

            // Save
            await this.attributeLocalStorageRepository.saveAttributeValue(mergedValues);

        }
    }

    /**
     * Get stores by code
     */
    protected async getStoreByCode(storeCode: StoreCodeEnum): Promise<StoreType[]> {

        try {

            // Get stores
            return await this.storeRepository.getActiveStoreByCode(storeCode);

        } catch (error) {

            // Logging
            await this.logger.critical(error.message, error.loggingChannel, error.params, error.storeId);
            process.exit(1);
        }
    }
}
