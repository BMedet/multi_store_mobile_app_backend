/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import StoreRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import IndexTypeEnum from "../../../../Enums/IndexTypeEnum";
import AbstractIndexToDbService from "../AbstractIndexToDbService";
import RegionMagentoDbRepositoryInterface from "../../../../Repositories/Db/Magento/Region/RegionMagentoDbRepositoryInterface";
import RegionDbRepositoryInterface from "../../../../Repositories/Db/LocalStorage/Index/Region/RegionDbRepositoryInterface";
export default class RegionIndexToDbService extends AbstractIndexToDbService {

    protected INDEX_TYPE = IndexTypeEnum.region;
    protected magentoRepository: RegionMagentoDbRepositoryInterface;
    protected localStorageRepository: RegionDbRepositoryInterface;

    /**
     * Constructor
     */
    constructor(
        regionMagentoDbRepository: RegionMagentoDbRepositoryInterface,
        regionDbRepositoryInterface: RegionDbRepositoryInterface,
        storeRepository: StoreRepositoryInterface
    ) {
        super(storeRepository);
        this.magentoRepository = regionMagentoDbRepository;
        this.localStorageRepository = regionDbRepositoryInterface;
    }

}
