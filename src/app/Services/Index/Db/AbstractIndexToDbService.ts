/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import IndexTypeEnum from "../../../Enums/IndexTypeEnum";
import StoreRepositoryInterface from "../../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import AbstractIndexService from "../AbstractIndexService";
import lodash from "lodash";
import IndexToLocalStorageRepositoryInterface from "../../../Repositories/Db/LocalStorage/Index/LocalStorageIndexRepositoryInterface";
import MagentoDbRepositoryInterface from "../../../Repositories/Db/Magento/MagentoDbRepositoryInterface";
import Helper from "sosise-core/build/Helper/Helper";

export default abstract class AbstractIndexToDbService extends AbstractIndexService {

    protected abstract INDEX_TYPE: IndexTypeEnum;

    protected abstract magentoRepository: MagentoDbRepositoryInterface;
    protected abstract localStorageRepository: IndexToLocalStorageRepositoryInterface;

    /**
     * Constructor
     */
    constructor(
        storeRepository: StoreRepositoryInterface
    ) {
        super(storeRepository);
    }

    /**
     * Index per one store items
     */
    protected async indexPerOneStore(): Promise<void> {

        try {
            // Logging
            await this.logger.info(`[${this.store.code}] Start "${this.INDEX_TYPE}" index to db`, this.loggingChannel, null, this.store.entityId);

            // Update index
            const updatedItemIds = await this.updateActiveIndex();

            // Disable index
            await this.disableInactiveIndex(updatedItemIds);

            // Logging
            await this.logger.info(`[${this.store.code}] Finish "${this.INDEX_TYPE}" index to db`, this.loggingChannel, null, this.store.entityId);
        } catch (error) {

            // Logging
            await this.logger.error(`[${this.store.code}] [${this.INDEX_TYPE}] An error occurred while indexing to DB`, this.loggingChannel, error, this.store.entityId);
        }
    }

    /**
     * Update index
     */
    protected async updateActiveIndex(): Promise<number[]> {
        const newLodash = lodash as any;
        // Get items from magento
        const itemsFromSourceRepository = await this.magentoRepository.getAllActiveItems(this.IS_FORCE, this.store);
        let updatedItemIds = new Array();
        for (const perChunkItems of newLodash.chunk(itemsFromSourceRepository, 1000)) {
            const ids = await this.localStorageRepository.index(perChunkItems, this.store);
            updatedItemIds = updatedItemIds.concat(ids);
        }

        // Logging
        await this.logger.info(`[${this.store.code}] Active "${this.INDEX_TYPE}" indexes updated in db`, this.loggingChannel, null, this.store.entityId);

        return updatedItemIds;
    }

    /**
     * Disable inactive index
     */
    protected async disableInactiveIndex(updatedItemIds: number[]) {

        if(this.IS_FORCE) return;

        // Disable inactive indexes
        await this.localStorageRepository.disableInactiveIndexes(updatedItemIds, this.store);

        // Logging
        await this.logger.info(`[${this.store.code}] Inactive "${this.INDEX_TYPE}" indexes disabled in db`, this.loggingChannel, null, this.store.entityId);
    }
}
