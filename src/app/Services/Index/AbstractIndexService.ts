/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import Helper from "sosise-core/build/Helper/Helper";
import IOC from "sosise-core/build/ServiceProviders/IOC";
import StoreCodeEnum from "../../Enums/StoreCodeEnum";
import StoreRepositoryInterface from "../../Repositories/Db/LocalStorage/Store/StoreRepositoryInterface";
import StoreType from "../../Types/LocalStorage/StoreType";
import CustomLoggerService from "../Logger/CustomLoggerService";

export default abstract class AbstractIndexService {

    protected storeRepository: StoreRepositoryInterface;
    protected logger: CustomLoggerService;

    protected store: StoreType;
    protected loggingChannel = 'index';
    protected IS_FORCE: boolean;

    /**
     * Constructor
     */
    constructor(
        storeRepository: StoreRepositoryInterface
    ) {
        this.storeRepository = storeRepository;
        this.logger = IOC.make(CustomLoggerService);
    }


    /**
     * Index catalog categories to db
     */
    public async index(storeCode: StoreCodeEnum = StoreCodeEnum.all, isForce: boolean = true) {
        // Set "isForce" property
        this.IS_FORCE = isForce;
        // Get stores
        const stores = await this.getStoreByCode(storeCode);

        for (const store of stores) {
            // Set store property
            this.store = store;

            // Index per one store categories
            await this.indexPerOneStore();
        }
    }

    /**
     * Inde per one store catalog categories
     */
    protected abstract indexPerOneStore(): Promise<void>;

    /**
     * Get stores by code
     */
    protected async getStoreByCode(storeCode: StoreCodeEnum): Promise<StoreType[]> {

        try {

            // Get stores
            return await this.storeRepository.getActiveStoreByCode(storeCode);

        } catch (error) {

            // Logging
            await this.logger.critical(error.message, error.loggingChannel, error.params, error.storeId);
            process.exit(1);
        }
    }
}
