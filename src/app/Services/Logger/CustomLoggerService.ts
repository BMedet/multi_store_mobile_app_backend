/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import LoggerService from "sosise-core/build/Services/Logger/LoggerService";
import { isNull } from "lodash";
import LogRepositoryInterface from "../../Repositories/Db/LocalStorage/Log/LogRepositoryInterface";

export default class CustomLoggerService {
    private logRepository: LogRepositoryInterface;
    private loggerService: LoggerService;

    /**
     * Constructor
     */
    public constructor(logRepository: LogRepositoryInterface, loggerService: LoggerService) {
        this.logRepository = logRepository;
        this.loggerService = loggerService;
    }

    /**
     * Debug log
     */
    public async debug(message: string, channel: string = 'default', params: any | null = null,  storeId: number = 0): Promise<void> {
        // Log to file
        this.loggerService.debug(message, params, channel);

        // Log to db
        await this.logRepository.debug(message, channel, this.stringifyParams(params), storeId);
    }

    /**
     * Info log
     */
    public async info(message: string, channel: string = 'default', params: any | null = null,  storeId: number = 0): Promise<void> {

        // Log to file
        this.loggerService.info(message, params, channel);

        // Log to db
        await this.logRepository.info(message, channel, this.stringifyParams(params), storeId);
    }

    /**
     * Warning log
     */
    public async warning(message: string, channel: string = 'default', params: any | null = null,  storeId: number = 0): Promise<void> {

        // Log to file
        this.loggerService.warning(message, params, channel);

        // Log to db
        await this.logRepository.warning(message, channel, this.stringifyParams(params), storeId);

    }

    /**
     * Error log
     */
    public async error(message: string, channel: string = 'default', params: any | null = null,  storeId: number = 0): Promise<void> {

        // Log to file
        this.loggerService.error(message, params, channel);

        // Log to db
        await this.logRepository.error(message, channel, this.stringifyParams(params), storeId);

    }

    /**
     * Critical log
     */
    public async critical(message: string, channel: string = 'default', params: any | null = null,  storeId: number = 0): Promise<void> {

        // Log to file
        this.loggerService.critical(message, params, channel);

        // Log to db
        await this.logRepository.critical(message, channel, this.stringifyParams(params), storeId);
    }

    /**
     *  Typecast params
     */
    private stringifyParams(params: any): string | null {
        if(isNull(params)) {
            return null;
        }

        if(typeof(params) === 'string') {
            return params;
        }

        return JSON.stringify(params);
    }
}
