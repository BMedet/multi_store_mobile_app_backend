/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum StoreCodeEnum {
    all = 'all',
    meloman = 'meloman',
    marwin = 'marwin',
    komfort = 'komfort',
}
export default StoreCodeEnum;
