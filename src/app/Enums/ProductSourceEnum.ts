/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum ProductSourceEnum {

    catalogCategory = 'category',
    promotion = 'promotion',
}
export default ProductSourceEnum;