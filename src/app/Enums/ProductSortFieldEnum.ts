/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum ProductSortFieldEnum {
    bestseller = 'bestseller',
    date = 'magentoCreatedAt',
    priceUp = 'priceUp',
    priceDown = 'priceDown',
    price = 'price',
    discount = 'discount',
}
export default ProductSortFieldEnum;