/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum ElasticAliasNameEnum {
    catalogCategory = '{store}_app_catalog_category_alias',
    catalogProduct = '{store}_app_catalog_product_alias',
    catalogAttribute = '{store}_app_catalog_attribute_alias',
    catalogAttributeOption = '{store}_app_catalog_attribute_option_alias',
    region = '{store}_app_region_alias',
    postCategory = '{store}_app_post_category_alias',
    postOffer = '{store}_app_post_offer_alias'
}
export default ElasticAliasNameEnum;
