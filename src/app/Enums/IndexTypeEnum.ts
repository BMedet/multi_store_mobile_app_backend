/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum IndexTypeEnum {
    catalogCategory = 'catalog_category',
    catalogProduct = 'catalog_product',
    catalogAttribute = 'catalog_attribute',
    catalogAttributeOption = 'catalog_attribute_option',
    region = 'region',
    postOffer = 'post_offer',
    postCategory = 'post_category',
}
export default IndexTypeEnum;
