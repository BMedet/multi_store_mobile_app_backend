/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum CatalogAttributeBackendTypeEnum {
    varchar = 'varchar',
    int = 'int'
}
export default CatalogAttributeBackendTypeEnum;
