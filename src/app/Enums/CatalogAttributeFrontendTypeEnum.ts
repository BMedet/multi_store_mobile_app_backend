/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum CatalogAttributeFrontendTypeEnum {
    text = 'text',
    select = 'select',
    multiselect = 'multiselect',

}
export default CatalogAttributeFrontendTypeEnum;
