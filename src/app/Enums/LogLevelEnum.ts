/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum LogLevelEnum {
    debug = 100,
    info = 200,
    warning = 300,
    error = 400,
    critical = 500
}
export default LogLevelEnum;
