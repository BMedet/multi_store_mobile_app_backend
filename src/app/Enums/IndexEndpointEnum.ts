/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

enum IndexEndpointEnum {
    all = 'all',
    db = 'database',
    elastic = 'elastic'
}

export default IndexEndpointEnum;
