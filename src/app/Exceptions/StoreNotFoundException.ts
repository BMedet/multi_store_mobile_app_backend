import Exception from 'sosise-core/build/Exceptions/Exception';
import ExceptionResponse from 'sosise-core/build/Types/ExceptionResponse';

export default class StoreNotFoundException extends Exception {

    public params: any;

    // HTTP Code of the response with this exception
    protected httpCode = 404;
    protected storeId: number;

    // Error code which is rendered in the response
    protected code = 4004;

    // If set to false no exception will be sent to sentry
    protected sendToSentry = true;

    // In which logging channel should this exception be logged, see src/config/logging.ts
    protected loggingChannel = 'default';

    /**
     * Constructor
     */
    constructor(storeId: number = 0, message: string = 'Store not found', loggingChannel: string = 'default', params: any = null) {
        super(message);

        this.params = params;
        this.storeId = storeId;
        this.loggingChannel = loggingChannel;
    }

    /**
     * Handle exception
     */
    public handle(exception: this): ExceptionResponse {
        const response: ExceptionResponse = {
            code: exception.code,
            httpCode: exception.httpCode,
            message: 'Store not found',
            data: {
                storeId: exception.storeId,
                details: exception.message,
                params: this.params,
                loggingChannel: this.loggingChannel
            }
        };
        return response;
    }
}
