/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import Exception from 'sosise-core/build/Exceptions/Exception';
import Helper from 'sosise-core/build/Helper/Helper';
import ExceptionResponse from 'sosise-core/build/Types/ExceptionResponse';

export default class ElasticsearchRequestException extends Exception {

    public params: any;
    public response: any;
    public storeId: number;

    public httpCode = 500;

    // Error code which is rendered in the response
    protected code = 5000;

    // If set to false no exception will be sent to sentry
    protected sendToSentry = true;

    // In which logging channel should this exception be logged, see src/config/logging.ts
    protected loggingChannel = 'default';


    /**
     * Constructor
     */
    constructor(storeId: number = 0, message: string = 'Exception occured during request to Elasticsearch', loggingChannel: string = 'default', params: any = null, response: any = null) {

        super(message);

        this.params = params;
        this.response = response;
        this.storeId = storeId;
        this.loggingChannel = loggingChannel;
    }

    /**
     * Handle exception
     */
    public handle(exception: this): ExceptionResponse {
        const httpResponse: ExceptionResponse = {
            message: 'Exception occured during request to Elastic',
            httpCode: exception.httpCode,
            data: {
                storeId: exception.storeId,
                details: exception.message,
                params: this.params,
                response: this.response,
                loggingChannel: this.loggingChannel
            }
        };
        return httpResponse;
    }
}
