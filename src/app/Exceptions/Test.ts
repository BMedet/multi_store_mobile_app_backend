import Exception from 'sosise-core/build/Exceptions/Exception';
import ExceptionResponse from 'sosise-core/build/Types/ExceptionResponse';

export default class Test extends Exception {

    // This variable is optional, you may remove it
    public exampleVariable: string;

    // HTTP Code of the response with this exception
    protected httpCode = 400;

    // Error code which is rendered in the response
    protected code = 1012;

    // If set to false no exception will be sent to sentry
    protected sendToSentry = true;

    // In which logging channel should this exception be logged, see src/config/logging.ts
    protected loggingChannel = 'default';

    /**
     * Constructor
     */
    constructor(message: string, exampleVariable: string) {
        super(message);

        // This is just an example
        this.exampleVariable = exampleVariable;
    }

    /**
     * Handle exception
     */
    public handle(exception: this): ExceptionResponse {
        const response: ExceptionResponse = {
            code: exception.code,
            httpCode: exception.httpCode,
            message: exception.message,
            data: {
                yourCustomData: exception.exampleVariable
            }
        };
        return response;
    }
}
