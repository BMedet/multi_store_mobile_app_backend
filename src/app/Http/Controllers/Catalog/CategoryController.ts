import { Request, Response, NextFunction } from 'express';
import IOC from 'sosise-core/build/ServiceProviders/IOC';
import HttpResponse from 'sosise-core/build/Types/HttpResponse';
import CatalogCategoryAppService from '../../../Services/Frontend/Catalog/CatalogCategoryAppService';

export default class CategoryController {
    /**
     * Example method
     */
    public async getCategoriesTree(request: Request, response: Response, next: NextFunction) {

        // Init service
        const service = IOC.make(CatalogCategoryAppService) as CatalogCategoryAppService;
        try {
            // Get active categories tree
            const categoriesTree = await service.getCategoriesTree(Number(request.params.storeId));

            // Send response
            return response.send(categoriesTree);
        } catch (error) {
            next(error);
        }
    }
}
