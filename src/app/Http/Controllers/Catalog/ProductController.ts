import { Request, Response, NextFunction } from 'express';
import IOC from 'sosise-core/build/ServiceProviders/IOC';
import ProductSourceEnum from '../../../Enums/ProductSourceEnum';
import CatalogProductAppService from '../../../Services/Frontend/Catalog/CatalogProductAppService';
import GetProductListUnifier from '../../../Unifiers/GetProductListUnifier';
import GetProductUnifier from '../../../Unifiers/GetProductUnifier';

export default class ProductController {

    /**
     * Get products list by category id
     */
    public async getProductsByCategoryId(request: Request, response: Response, next: NextFunction) {

        const getProductListUnifier = new GetProductListUnifier(request.params, request.query);
        // Init service
        const productService = IOC.make(CatalogProductAppService) as CatalogProductAppService;

        try {
            // Get active prducts
            const productsData = await productService.getProductsBySourceId(getProductListUnifier, ProductSourceEnum.catalogCategory);

            // Send response
            return response.send(productsData);
        } catch (error) {
            next(error);
        }
    }

    /**
     * Get product by id
     */
    public async getProductById(request: Request, response: Response, next: NextFunction) {

        const getProductUnifier = new GetProductUnifier(request.params, request.query);
        // Init service
        const service = IOC.make(CatalogProductAppService) as CatalogProductAppService;
        try {
            // Get active prducts
            const productsData = await service.getProductById(getProductUnifier);

            // Send response
            return response.send(productsData);
        } catch (error) {
            next(error);
        }
    }
}
