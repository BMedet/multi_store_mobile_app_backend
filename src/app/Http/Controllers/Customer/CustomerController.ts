import { Request, Response, NextFunction } from 'express';
import Helper from 'sosise-core/build/Helper/Helper';
import IOC from 'sosise-core/build/ServiceProviders/IOC';
import CustomerAppService from '../../../Services/Frontend/Customer/CustomerAppService';
import RegionAppService from '../../../Services/Frontend/Region/RegionAppService';
import CustomerInitUnifier from '../../../Unifiers/CustomerInitUnifier';
import CustomerUpdateUnifier from '../../../Unifiers/CustomerUpdateUnifier';

export default class CustomerController {
    /**
     * Init customer
     */
    public async initCustomer(request: Request, response: Response, next: NextFunction) {

        const customerInitUnifier = new CustomerInitUnifier(request.body, request.params.storeId);

        // Init service
        const service = IOC.make(CustomerAppService) as CustomerAppService;

        try {
            const result = await service.init(customerInitUnifier);
            return response.send(result);

        } catch (error) {
            next(error);
        }
    }

    /**
     * update customer
     */
    public async updateCustomer(request: Request, response: Response, next: NextFunction) {

        // Init unifier
        const customerInitUnifier = new CustomerUpdateUnifier(request.body, request.params.storeId);

        // Init service
        const service = IOC.make(CustomerAppService) as CustomerAppService;

        try {
            const result = await service.update(customerInitUnifier);
            return response.send({ customerData: result });

        } catch (error) {
            next(error);
        }
    }
}
