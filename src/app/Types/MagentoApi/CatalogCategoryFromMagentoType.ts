/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface CatalogCategoryFromMagentoType {
    id?:number;
    entityId: number;
    name: string;
    urlPath: string | null;
    thumbnailImage: string | null;
    showThumbnail: boolean;
    parentId: number;
    position: number;
    isActive: boolean;
    isHidden: boolean;
    level: number;
    nameInMenu: string | null;
    includeInMenu: boolean;
    filterableAttributes: string | null;
    magentoCreatedAt: string;
    magentoUpdatedAt: string;
    createdAt?:Date;
    updateddAt?:Date;
}
