/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import SortValueEnum from "../../Enums/SortValueEnum";
import ProductFilterableAttributeType from "../Frontend/ProductFilterableAttributeType";
import ProductListingFilterType from "../Frontend/ProductListingFilterType";
import CatalogProductSearchType from "./CatalogProductSearchType";

export default interface CatalogProductSearchResultType {
    totalCount: number;
    count: number;
    page: number;
    products: CatalogProductSearchType[];
    sort: {
        field: string,
        value: SortValueEnum
    };
    aggregation: {
        minPrice: number;
        maxPrice: number;
    },
    filterableAttributes?: ProductFilterableAttributeType[];
    filter?: ProductListingFilterType;

}
