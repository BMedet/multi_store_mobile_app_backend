/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface CatalogProductSearchType {
    entityId: number;
    name: string;
    sku: string;
    price: number;
    oldPrice: number | null;
    description: string | null;
    thumbnail: string | null;
    images: string[] | null;
    url: string;
    isInStock: boolean;
    qty: number;
    categoryIds: string;
}
