import ProductAttributeOptionType from "../MagentoDb/ProductAttributeOptionType";

export default interface FilterableAttributeType {
    id: number;
    enabled: boolean;
    attributeId: number;
    attributeCode: string;
    frontendLabel: string;
    frontendInput: string;
    isVisibleOnFront: boolean;
    isVisibleOnGrid: boolean;
    isFilterable: boolean;
    isMultiselect: boolean;
    position: number;
    backendType: string;
    options: ProductAttributeOptionType[] | [];
}
