import CatalogCategorySearchType from "../Elasticsearch/CatalogCategorySearchType";
import RegionSearchType from "../Elasticsearch/RegionSearchType";
import CustomerInitType from "../LocalStorage/CustomerInitType";

export default interface InitType {
    customerData: CustomerInitType;
    regionOptions: RegionSearchType[];
    categoryTree: CatalogCategorySearchType[];
}
