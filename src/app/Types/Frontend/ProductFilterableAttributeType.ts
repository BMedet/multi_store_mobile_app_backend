/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface ProductFilterableAttributeType {
    attributeId: number;
    attributeCode: string;
    frontendLabel: string;
    frontendInput: string;
    isMultiselect: boolean;
    options: {
        value: string;
        label: string;
        count: number
    }[]
}
