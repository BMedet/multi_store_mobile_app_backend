/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface ProductListingFilterType {
    price: {
        min: number;
        max: number;
    },
    attribute: {
        [key: string]: string[];
    },
    stock: string[];
}
