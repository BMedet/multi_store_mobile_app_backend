export default interface RegionFromMagentoType {
    id?: number;
    entityId: number;
    name: string;
    isActive: boolean;
    isVisible: boolean;
    code: string;
    position: number;
    declensionName: string | null;
    polygon: string | null;
    createdAt?: string;
    updatedAt?: string;
}
