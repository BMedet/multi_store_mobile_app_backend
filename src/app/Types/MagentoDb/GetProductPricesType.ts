export default interface GetProductPricesType {
    entityId: number;
    oldPrice: number | null;
    specialPrice: number | null;
    price: number;
}
