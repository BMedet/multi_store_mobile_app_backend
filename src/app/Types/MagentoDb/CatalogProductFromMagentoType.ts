export default interface CatalogProductFromMagentoType {
    id?: number;
    entityId: number;
    name: string;
    sku: string;
    isVisible: number;
    isActive: boolean;
    qty: number;
    thumbnail: string;
    categoryIds: string;
    magentoCreatedAt?: string;
}
