export default interface ProductAttributeOptionType {
    id?: number;
    attributeId: number;
    optionId: number;
    value: string | null;
    valueId: number;
    sortOrder: number;
    createdAt?: string;
    updatedAt?: string;
}
