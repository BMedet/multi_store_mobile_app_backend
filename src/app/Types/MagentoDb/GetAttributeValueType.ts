export default interface GetAttributeValueType {
    entityId: number;
    attributeId: number;
    attributeValue: string | null;
}
