export default interface GetProductContentType {
    entityId: number;
    images: string | null;
    description: string | null;
    url: string;
}
