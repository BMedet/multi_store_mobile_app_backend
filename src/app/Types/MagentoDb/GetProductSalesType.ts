export default interface GetProductSalesType {
    entityId: number;
    qty: number;
}
