export default interface ProductAttributeType {
    id?: number;
    enabled?: boolean;
    attributeId: number;
    attributeCode: string;
    frontendLabel: string | null;
    frontendInput: string;
    position: number;
    isVisibleOnFront: boolean;
    isVisibleOnGrid: boolean;
    isFilterable: boolean;
    isMultiselect: boolean;
    backendType: string;
    createdAt?: string;
    updatedAt?: string;
}
