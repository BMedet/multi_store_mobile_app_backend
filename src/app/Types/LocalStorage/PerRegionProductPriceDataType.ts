export default interface PerRegionProductPriceDataType {
    productEntityId: number;
    productId: number;
    regionEntityId: number;
    regionId: number;
    price: number;
}

