export default interface PerRegionProductStockDataType {
    productEntityId: number;
    productId: number;
    regionEntityId: number;
    regionId: number;
    qty: number;
}

