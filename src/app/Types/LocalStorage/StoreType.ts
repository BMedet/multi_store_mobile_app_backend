/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface StoreType {
    id: number;
    entityId: number;
    label: string;
    code: string;
    baseUrl: string;
    token: string;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}
