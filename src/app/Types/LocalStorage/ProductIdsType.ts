export default interface ProductIdsType {
    id: number;
    entityId: number;
    price: number;
}
