export default interface CatalogProductFromLocalStorageType {
    id: number;
    entityId: number;
    name: string;
    sku: string;
    price: number;
    discount: number;
    specialPrice: number | null;
    oldPrice: number | null;
    description: string | null;
    thumbnail: string | null;
    images: string | null;
    isActive: boolean;
    isVisible: boolean;
    url: string;
    categoryIds: string;
    regionData: {
        [key: string]: {
            isInStock: boolean
            qty: number,
            price: number
        }
    };
    bestseller: number;
    magentoCreatedAt: string;
    createdAt: string;
    updatedAt: string;
}

