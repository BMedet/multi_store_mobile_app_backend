import CatalogProductAttributeWithValueType from "./CatalogProductAttributeWithValueType"

export default interface BindAttributeToProductType {
    [key: string]: { // Product entity id
        [key: string]: CatalogProductAttributeWithValueType
    }[]
}
