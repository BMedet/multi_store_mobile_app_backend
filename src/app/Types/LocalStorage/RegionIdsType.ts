export default interface RegionIdsType {
    id: number;
    entityId: number;
    stockIds?: string;
}

