export default interface CatalogProductAttributeWithValueType {
    attributeId: number;
    storeIds: string[],
    attributeCode: string;
    frontendLabel: string;
    frontendInput: string;
    isVisibleOnFront: boolean;
    isVisibleOnGrid: boolean;
    isFilterable: boolean;
    isMultiselect: boolean;
    enabled: boolean;
    value: string;
    label: string;
}
