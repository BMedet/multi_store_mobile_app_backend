import Validator from 'validatorjs';
import ValidationException from 'sosise-core/build/Exceptions/Validation/ValidationException';
import Helper from 'sosise-core/build/Helper/Helper';

/**
 * If you need more validation rules, see: https://github.com/mikeerickson/validatorjs
 */
export default class CustomerInitUnifier {

    private params: any;
    public mobileUuid: string; // "null'71af9c61390fea87'
    public platform: string | null; // 'Android'
    public platformVersion: string | null; // '11'
    public model: string | null; // 'sdk_gphone_x86'
    public manufacturer: string | null;
    public serial: string | null;
    public sdkVersion: string | null;
    public cordovaVersion: string | null;
    public storeId: number;

    /**
     * Constructor
     */
    constructor(params: any, storeId: string) {
        // Remember incoming params
        this.params = params;

        this.storeId = Number(storeId);
        // Validate, await is important otherwise we could not catch the exception
        this.validate();

        // Map data
        this.map();
    }

    /**
     * Request data validation
     */
    private validate() {
        // Create validator
        const validator = new Validator(this.params, {
            mobileUuid: ['string'],
            platform: ['string'],
            platformVersion: ['string'],
            model: ['string'],
            manufacturer: ['string'],
            serial: ['string'],
            sdkVersion: ['string'],
            cordovaVersion: ['string'],
        });

        // If it fails throw exception
        if (validator.fails()) {
            throw new ValidationException('Validation exception', (validator.errors.all() as any));
        }
    }

    /**
     * Request data mapping
     */
    private map() {
        this.mobileUuid = this.params.mobileUuid;
        this.platform = this.params.platform;
        this.platformVersion = this.params.platformVersion;
        this.model = this.params.model;
        this.manufacturer = this.params.manufacturer;
        this.serial = this.params.serial;
        this.sdkVersion = this.params.sdkVersion;
        this.cordovaVersion = this.params.cordovaVersion;
    }
}
