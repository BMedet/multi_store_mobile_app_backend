import Validator from 'validatorjs';
import ValidationException from 'sosise-core/build/Exceptions/Validation/ValidationException';
import Helper from 'sosise-core/build/Helper/Helper';

/**
 * If you need more validation rules, see: https://github.com/mikeerickson/validatorjs
 */
export default class CustomerUpdateUnifier {

    private params: any;
    public storeId: number;
    public customerId: number;
    public regionId: number;
    public language: string;
    public theme: string;
    public name: string | null;
    public lastName: string | null;
    public patronymic: string | null;
    public email: string | null;
    public mobile: string | null;
    public dob: string | null;
    public sex: string | null;

    /**
     * Constructor
     */
     constructor(params: any, storeId: string) {
        // Remember incoming params
        this.params = params;

        this.storeId = Number(storeId);
        // Validate, await is important otherwise we could not catch the exception
        this.validate();

        // Map data
        this.map();
    }


    /**
     * Request data validation
     */
    private validate() {
        // Create validator
        const validator = new Validator(this.params, {
            customerId: ['required', 'numeric', 'min:1'],
            regionId: ['required', 'numeric', 'min:1'],
            language: ['required', 'string', 'min:1'],
            theme: ['required', 'string', 'min:1'],
            name: ['string', 'min:1'],
            lastName: ['string', 'min:1'],
            patronymic: ['string', 'min:1'],
            email: ['string', 'min:1'],
            mobile: ['string', 'min:1'],
            dob: ['string', 'min:1'],
            sex: ['string', 'min:1']  
        });

        // If it fails throw exception
        if (validator.fails()) {
            throw new ValidationException('Validation exception', (validator.errors.all() as any));
        }
    }

    /**
     * Request data mapping
     */
    private map() {
        this.customerId = this.params.customerId;
        this.regionId = this.params.regionId;
        this.language = this.params.language;
        this.theme = this.params.theme;
        this.name = this.params.name;
        this.lastName = this.params.lastName;
        this.patronymic = this.params.patronymic;
        this.email = this.params.email;
        this.mobile = this.params.mobile;
        this.dob = this.params.dob;
        this.sex = this.params.sex;
    }
}
