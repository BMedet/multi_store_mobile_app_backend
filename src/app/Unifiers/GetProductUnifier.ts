import Validator from 'validatorjs';
import lodash from 'lodash';
import ValidationException from 'sosise-core/build/Exceptions/Validation/ValidationException';
import SortValueEnum from '../Enums/SortValueEnum';
import ProductSortFieldEnum from '../Enums/ProductSortFieldEnum';

/**
 * If you need more validation rules, see: https://github.com/mikeerickson/validatorjs
 */
export default class GetProductUnifier {

    private params: any;
    public storeId: number;
    public regionId: number;
    public id: number;

    /**
     * Constructor
     */
    constructor(params: any, query: any) {
        // Remember incoming params
        this.params = { ...params, ...query };

        // Validate, await is important otherwise we could not catch the exception
        this.validate();

        // Map data
        this.map();
    }

    /**
     * Request data validation
     */
    private validate() {
        // Create validator
        const validator = new Validator(this.params, {
            storeId: ['required', 'numeric', 'min:1'],
            id: ['required', 'numeric', 'min:1'],
            regionId: ['required', 'numeric', 'min:1'],
        });

        // If it fails throw exception
        if (validator.fails()) {
            throw new ValidationException('Validation exception', (validator.errors.all() as any));
        }
    }

    /**
     * Request data mapping
     */
    private map() {
        this.storeId = this.params.storeId;
        this.regionId = lodash.get(this.params, 'regionId', null);
        this.id = lodash.get(this.params, 'id', null);
    }
}
