import Validator from 'validatorjs';
import lodash from 'lodash';
import ValidationException from 'sosise-core/build/Exceptions/Validation/ValidationException';
import ProductSortFieldEnum from '../Enums/ProductSortFieldEnum';
import ProductListingFilterType from '../Types/Frontend/ProductListingFilterType';
/**
 * If you need more validation rules, see: https://github.com/mikeerickson/validatorjs
 */
export default class GetProductListUnifier {

    private params: any;
    public storeId: number;
    public sourceId: number;
    public sortField: ProductSortFieldEnum;
    public filter: ProductListingFilterType;
    public regionId: number;
    public page: number;

    /**
     * Constructor
     */
    constructor(params: any, query: any) {
        // Remember incoming params
        this.params = { ...params, ...query };

        // Validate, await is important otherwise we could not catch the exception
        this.validate();

        // Map data
        this.map();
    }

    /**
     * Request data validation
     */
    private validate() {
        // Create validator
        const validator = new Validator(this.params, {
            storeId: ['required', 'numeric', 'min:1'],
            id: ['required', 'numeric', 'min:1'],
            regionId: ['required', 'numeric', 'min:1'],
            page: ['required', 'numeric', 'min:1'], // @todo validate filter
        });

        // If it fails throw exception
        if (validator.fails()) {
            throw new ValidationException('Validation exception', (validator.errors.all() as any));
        }
    }

    /**
     * Request data mapping
     */
    private map() {
        this.storeId = this.params.storeId;
        this.sourceId = this.params.id;
        this.sortField = lodash.get(this.params, 'sortField', 'bestseller');
        this.filter = this.typecastFilter();
        this.regionId = lodash.get(this.params, 'regionId');
        this.page = lodash.get(this.params, 'page', null);
    }

    /**
     * Typecast filter
     */
    private typecastFilter(): ProductListingFilterType {
        // Parse
        const parsedFilter = JSON.parse(this.params.filter)

        // Typecast
        const preparedFilter = {
            price: parsedFilter.price,
            attribute: parsedFilter.attribute, 
            stock: parsedFilter.stock
        }

        return preparedFilter;
    }
}
