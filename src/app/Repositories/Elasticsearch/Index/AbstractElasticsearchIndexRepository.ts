/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import lodash, { isNull } from "lodash";
import Helper from "sosise-core/build/Helper/Helper";
import ElasticHelper from "../../../../Helper/ElasticHelper";
import IndexTypeEnum from "../../../Enums/IndexTypeEnum";
import ElasticsearchRequestException from "../../../Exceptions/ElasticsearchRequestException";
import CatalogproductFromLocalStorageType from "../../../Types/LocalStorage/CatalogproductFromLocalStorageType";
import StoreType from "../../../Types/LocalStorage/StoreType";
import CatalogCategoryFromMagentoType from "../../../Types/MagentoApi/CatalogCategoryFromMagentoType";
import ProductAttributeOptionType from "../../../Types/MagentoDb/ProductAttributeOptionType";
import ProductAttributeType from "../../../Types/MagentoDb/ProductAttributeType";
import RegionFromMagentoType from "../../../Types/MagentoDb/RegionFromMagentoType";
import AbstractElasticsearchRepository from "../AbstractElasticsearchRepository";

export default abstract class AbstractElasticsearchIndexRepository extends AbstractElasticsearchRepository {


    /**
     * Create catalog category index
     */
    public async createIndex(store: StoreType): Promise<string> {

        // Prepare url
        const url = this.prepareUrl(store);

        // Prepare header
        const headers = {
            'Content-Type': 'application/json'
        };

        // Make request
        await this.makeRequest(url, 'PUT', store, null, {}, headers);
        return url;
    }

    /**
     * Get index path by store code
     */
    public async getCurrentIndexPath(store: StoreType): Promise<string | null> {
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);
        try {
            // Get current index
            const response = await this.makeRequest(`_alias/${alias}`, 'GET', store);
            const indexPath = Object.keys(response.data)[0];
            return indexPath;
        } catch (error) {
            return null;
        }
    }

    /**
     * Clone index
     */
    public async cloneCurrentIndex(store: StoreType): Promise<string> {

        // Get current index path
        const currentIndexPath = await this.getCurrentIndexPath(store);

        // Prepare new index path
        const newIndexPath = this.prepareUrl(store);


        try {
            // Block index
            await this.setUpIndexBlocking(currentIndexPath!, true, store);

            // Clone index
            await this.cloneIndex(currentIndexPath!, newIndexPath, store);

            // Unblock current index
            await this.setUpIndexBlocking(currentIndexPath!, false, store);

            // Unblock new index
            await this.setUpIndexBlocking(newIndexPath!, false, store);

            return newIndexPath;
        } catch (error) {

            // Unblock index
            await this.setUpIndexBlocking(currentIndexPath!, false, store);

            // Throw exception
            throw new ElasticsearchRequestException(error.storeId, error.message, error.loggingChannel, error.params, error.response);
        }
    }

    /**
     * Index
     */
    public async index(indexes: CatalogCategoryFromMagentoType[] | RegionFromMagentoType[] | CatalogproductFromLocalStorageType[] | ProductAttributeType[] | ProductAttributeOptionType[], newIndexPath: string, store: StoreType): Promise<void> {
        // Prepare header
        const headers = {
            'Content-Type': 'application/x-ndjson'
        };

        let body = ``;
        for (const index of indexes) {
            if (lodash.get(index, 'sku', false)) {
                index['attributes'] = null;
            }

            body += `{"index": {"_index":"${newIndexPath}","_type": "_doc", "_id":${lodash.get(index, 'entityId', lodash.get(index, 'id'))}}}\n${JSON.stringify(index)} \n`;
        }


        await this.makeRequest('_bulk', 'POST', store, null, body, headers);
    }

    /**
     * Set index type
     */
    public setIndexType(indexType: IndexTypeEnum): void {
        this.INDEX_TYPE = indexType;
    }


    /**
     * Block index
     */
    private async setUpIndexBlocking(indexPath: string, blockValue: boolean, store: StoreType): Promise<void> {
        // Prepare body
        const body = {
            "settings": {
                "index.blocks.write": blockValue
            }
        };

        // Prepare headers
        const headers = {
            'Content-Type': 'application/json'
        };

        await this.makeRequest(`${indexPath}/_settings`, 'PUT', store, null, body, headers);
    }

    /**
     * Clone index
     */
    private async cloneIndex(currentIndexPath: string, newIndexPath: string, store: StoreType): Promise<void> {

        await this.makeRequest(`${currentIndexPath}/_clone/${newIndexPath}`, 'POST', store);
    }

    /**
     * Delete index
     */
    public async deleteIndex(indexPath: string, store: StoreType): Promise<void> {
        try {
            // Make request
            await this.makeRequest(indexPath, 'DELETE', store, null);
        } catch (error) {
            // Return If not exists
            if (error.httpCode === 404) {
                return;
            }
            // Else throw exception
            throw new ElasticsearchRequestException(error.storeId, error.message, error.loggingChannel, error.params, error.response);
        }
    }

    /**
     * Update alias (add new index to alias)
     */
    public async updateAlias(currentIndexPath: string | null, newIndexPath: string, store: StoreType): Promise<void> {
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);

        // Prepare body
        const body = {
            actions: [
                { "add": { "index": newIndexPath, alias } }
            ]
        } as any;

        if (!isNull(currentIndexPath)) {
            body.actions.push({ "remove": { "index": currentIndexPath, alias } });
        }

        // Make request
        await this.makeRequest(`_aliases`, 'POST', store, null, body);
    }
    /**
     * Update settings
     */
    public async updateLimitSettings(store: StoreType): Promise<void> {
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);

        // Prepare body
        const body = {
            "index.mapping.total_fields.limit": 1000000,
            "index": { "max_result_window": 500000 }
        }; 

        // Prepare headers
        const headers = {
            'Content-Type': 'application/json'
        };

        // Setup total_fields limit
        await this.makeRequest(`${alias}/_settings`, 'PUT', store, null, body, headers);
    }
}
