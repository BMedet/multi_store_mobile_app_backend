/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */


import Helper from 'sosise-core/build/Helper/Helper';
import lodash, { isEmpty } from 'lodash';
import ElasticHelper from '../../../../../Helper/ElasticHelper';
import IndexTypeEnum from '../../../../Enums/IndexTypeEnum';
import BindAttributeToProductType from '../../../../Types/LocalStorage/BindAttributeToProductType';
import StoreType from '../../../../Types/LocalStorage/StoreType';
import ProductAttributeOptionType from '../../../../Types/MagentoDb/ProductAttributeOptionType';
import ProductAttributeType from '../../../../Types/MagentoDb/ProductAttributeType';
import AbstractElasticsearchIndexRepository from '../AbstractElasticsearchIndexRepository';


export default class CatalogAttributeElasticsearchRepository extends AbstractElasticsearchIndexRepository {

    public INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.catalogAttribute;

    /**
     * Create catalog category index
     */
    public async createIndex(store: StoreType): Promise<string> {
        return await super.createIndex(store);
    }

    /**
     * Get current index path
     */
    public async getCurrentIndexPath(store: StoreType): Promise<string | null> {
        return await super.getCurrentIndexPath(store);
    }

    /**
     * Clone current index
     */
    public async cloneCurrentIndex(store: StoreType): Promise<string> {
        return await super.cloneCurrentIndex(store);
    }

    /**
     * Index catalog category
     */
    public async index(attributes: ProductAttributeType[] | ProductAttributeOptionType[], newIndexPath: string, store: StoreType): Promise<void> {
        await super.index(attributes, newIndexPath, store);
    }

    /**
     * Delete index
     */
    public async deleteIndex(indexPath: string, store: StoreType): Promise<void> {
        await super.deleteIndex(indexPath, store);
    }

    /**
     * Update alias (add new index to alias)
     */
    public async updateAlias(currentIndexPath: string | null, newIndexPath: string, store: StoreType): Promise<void> {
        super.updateAlias(currentIndexPath, newIndexPath, store);
    }

    /**
     * Bind attributes to product
     */
    public async bindAttributesToProduct(attributesWithProductEntityId: BindAttributeToProductType, stores: StoreType[]): Promise<void> {

        for (const store of stores) {
 
            try {
                await this.bindPerStoreAttributes(attributesWithProductEntityId, store);
            } catch (e) {
                continue;
            }
        }

    }

    /**
     * Bind per one store attributes
     */
    protected async bindPerStoreAttributes(attributesWithProductEntityId: BindAttributeToProductType, store: StoreType) {
        // Prepare header
        const headers = {
            'Content-Type': 'application/x-ndjson'
        };

        // Init body
        let body = ``;

        // prepare alias
        const alias = ElasticHelper.getAliasName(IndexTypeEnum.catalogProduct, store.code);

        for (const [productEntityId, attributes] of Object.entries(attributesWithProductEntityId)) {

            if (!lodash.includes(attributes[Object.keys(attributes)[0]].storeIds, String(store.entityId))) continue;

            body += `{"update": {"_index":"${alias}", "_id":${productEntityId}}}\n{"doc": ${JSON.stringify({ attributes })}} \n`;
        }

        if (!body.length) return;

        await this.makeRequest('_bulk', 'POST', store, null, body, headers);
    }
}
