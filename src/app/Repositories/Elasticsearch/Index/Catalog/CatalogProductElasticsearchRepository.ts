/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */


import IndexTypeEnum from '../../../../Enums/IndexTypeEnum';
import CatalogProductFromLocalStorageType from '../../../../Types/LocalStorage/CatalogproductFromLocalStorageType';
import StoreType from '../../../../Types/LocalStorage/StoreType';
import AbstractElasticsearchIndexRepository from '../AbstractElasticsearchIndexRepository';


export default class CatalogProductElasticsearchRepository extends AbstractElasticsearchIndexRepository {

    protected INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.catalogProduct;
    /**
     * Create catalog product index
     */
    public async createIndex(store: StoreType): Promise<string> {
        return await super.createIndex(store);
    }

    /**
     * Get current index path
     */
    public async getCurrentIndexPath(store: StoreType): Promise<string | null> {
        return await super.getCurrentIndexPath(store);
    }

    /**
     * Clone current index
     */
    public async cloneCurrentIndex(store: StoreType): Promise<string> {
        return await super.cloneCurrentIndex(store);
    }

    /**
     * Index catalog product
     */
    public async index(products: CatalogProductFromLocalStorageType[], newIndexPath: string, store: StoreType): Promise<void> {
        await super.index(products, newIndexPath, store);
    }

    /**
     * Delete index
     */
    public async deleteIndex(indexPath: string, store: StoreType): Promise<void> {
        await super.deleteIndex(indexPath, store);
    }

    /**
     * Update alias (add new index to alias)
     */
    public async updateAlias(currentIndexPath: string | null, newIndexPath: string, store: StoreType): Promise<void> {
        super.updateAlias(currentIndexPath, newIndexPath, store);
    }
}
