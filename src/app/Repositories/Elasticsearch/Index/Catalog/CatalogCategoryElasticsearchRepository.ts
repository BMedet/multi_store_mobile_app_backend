/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import StoreType from '../../../../Types/LocalStorage/StoreType';
import IndexTypeEnum from '../../../../Enums/IndexTypeEnum';
import AbstractIndexRepository from '../AbstractElasticsearchIndexRepository';
import CatalogCategoryFromMagentoType from '../../../../Types/MagentoApi/CatalogCategoryFromMagentoType';

export default class CatalogCategoryElasticsearchRepository extends AbstractIndexRepository {

    protected INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.catalogCategory;

    /**
     * Create catalog category index
     */
    public async createIndex(store: StoreType): Promise<string> {
        return await super.createIndex(store);
    }

    /**
     * Get current index path
     */
    public async getCurrentIndexPath(store: StoreType): Promise<string | null> {
        return await super.getCurrentIndexPath(store);
    }

    /**
     * Clone current index
     */
    public async cloneCurrentIndex(store: StoreType): Promise<string> {
        return await super.cloneCurrentIndex(store);
    }

    /**
     * Index catalog category
     */
    public async index(categories: CatalogCategoryFromMagentoType[], newIndexPath: string, store: StoreType): Promise<void> {
        await super.index(categories, newIndexPath, store);
    }

    /**
     * Delete index
     */
    public async deleteIndex(indexPath: string, store: StoreType): Promise<void> {
        await super.deleteIndex(indexPath, store);
    }

    /**
     * Update alias (add new index to alias)
     */
    public async updateAlias(currentIndexPath: string | null, newIndexPath: string, store: StoreType): Promise<void> {
        super.updateAlias(currentIndexPath, newIndexPath, store);
    }
}
