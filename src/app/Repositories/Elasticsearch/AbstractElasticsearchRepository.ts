/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import axios, { AxiosInstance } from 'axios';
import dayjs from 'dayjs';
import lodash, { isNull } from 'lodash';
import elasticsearchConfig from '../../../config/elasticsearch';
import IndexTypeEnum from '../../Enums/IndexTypeEnum';
import ElasticsearchRequestException from '../../Exceptions/ElasticsearchRequestException';
import StoreType from '../../Types/LocalStorage/StoreType';

export default abstract class AbstractElasticsearchRepository {
    private MAX_SEND_RETRIES = 5;
    private DELAY_BETWEEN_RETRIES_IN_MS = 2000;
    private httpClient: AxiosInstance;

    protected abstract INDEX_TYPE: IndexTypeEnum;

    /**
     * Constructor
     */
    constructor() {
        this.httpClient = axios.create({
            baseURL: `http://${elasticsearchConfig.url}`,
            timeout: 2000
        });
    }

    protected prepareUrl(store: StoreType): string {
        // Current date time
        const now = dayjs().format('YYYY-MM-DD_HH-mm-ss');

        switch (this.INDEX_TYPE) {

            // Catalog category index
            case IndexTypeEnum.catalogCategory:
                return `app_${store.code}_${IndexTypeEnum.catalogCategory}_${now}`;

            // Catalog product index
            case IndexTypeEnum.catalogProduct:
                return `app_${store.code}_${IndexTypeEnum.catalogProduct}_${now}`;

            // Catalog product attribute index
            case IndexTypeEnum.catalogAttribute:
                return `app_${store.code.toLowerCase()}_${IndexTypeEnum.catalogAttribute}_${now}`;

            // Catalog product attribute option index
            case IndexTypeEnum.catalogAttributeOption:
                return `app_${store.code.toLowerCase()}_${IndexTypeEnum.catalogAttributeOption}_${now}`;

            // Region index
            case IndexTypeEnum.region:
                return `app_${store.code}_${IndexTypeEnum.region}_${now}`;

            // Post category index
            case IndexTypeEnum.postCategory:
                return `app_${store.code}_${IndexTypeEnum.postCategory}_${now}`;

            // Post offer index
            case IndexTypeEnum.postOffer:
                return `app_${store.code}_${IndexTypeEnum.postOffer}_${now}`;

        }
    }

    /**
     * Make request
     */
    protected async makeRequest(url: string, method: "GET" | "DELETE" | "POST" | "PUT", store: StoreType, params: any = null, body: any = null, headers: any = null): Promise<any> {
        // Current amount of tries
        let tries = 0;

        // Try to send get request in endless loop
        while (true) {
            // Increment try
            tries++;

            try {
                // Make request
                const response = await this.httpClient.request({
                    url,
                    method,
                    params,
                    data: body,
                    headers
                });

                // Send response, everything is fine
                return response;
            } catch (error) {
                // Check for max tries
                if (tries === this.MAX_SEND_RETRIES) {

                    // Add url to params
                    if (isNull(params)) {
                        params = { url, method };
                    } else {
                        params.url = url;
                        params.method = method;
                    }
                    throw new ElasticsearchRequestException(
                        store.entityId,
                        `Maximum amount of ${this.MAX_SEND_RETRIES} tries is reached while requesting Elastic`,
                        'index',
                        params,
                        lodash.get(error, 'response.data', '')
                    );
                }
                // Wait some time
                await new Promise(resolve => setTimeout(resolve, this.DELAY_BETWEEN_RETRIES_IN_MS));
            }
        }
    }
}
