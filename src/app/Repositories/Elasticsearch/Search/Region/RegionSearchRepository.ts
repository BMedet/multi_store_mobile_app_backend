/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import ElasticsearchAbstractRepository from '../../AbstractElasticsearchRepository';
import SearchRepositoryInterface from './RegionSearchRepositoryInterface';
import IndexTypeEnum from '../../../../Enums/IndexTypeEnum';
import StoreType from '../../../../Types/LocalStorage/StoreType';
import lodash from 'lodash';
import RegionSearchType from '../../../../Types/Elasticsearch/RegionSearchType';
import Helper from 'sosise-core/build/Helper/Helper';
import ElasticHelper from '../../../../../Helper/ElasticHelper';


export default class RegionSearchRepository extends ElasticsearchAbstractRepository implements SearchRepositoryInterface {

    protected INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.region;

    /**
     * Get all active regions
     */
    public async getAllActiveRegions(store: StoreType): Promise<RegionSearchType[]> {
        // Prepare alias
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);

        // Prepare header
        const header = { 'Content-type': 'application/json' };

        // Prepare body
        const body = {
            "sort": [
                { "position": { "order": "asc" } }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "isActive": 1
                            }
                        },
                        {
                            "term": {
                                "isVisible": 1
                            }
                        }
                    ]
                }
            }
        };

        // Make request
        const response = await this.makeRequest(`/${alias}/_search?size=100`, 'POST', store, null, body, header);
        const hits = lodash.get(response, 'data.hits.hits', []);

        // Typecast
        const preparedRegions = new Array();
        for (const data of hits) {
            const preparedRegion: RegionSearchType = {
                entityId: data._source.entityId,
                name: data._source.name,
                code: data._source.code,
                declensionName: lodash.get(data, '_source.declensionName', null),
                polygon: lodash.get(data, '_source.polygon', null)
            };

            preparedRegions.push(preparedRegion);
        }
        return preparedRegions;
    }

}
