/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import RegionSearchType from "../../../../Types/Elasticsearch/RegionSearchType";
import StoreType from "../../../../Types/LocalStorage/StoreType";

export default interface RegionSearchRepositoryInterface {
    /**
     * Get all active regions
     */
    getAllActiveRegions(store: StoreType): Promise<RegionSearchType[]>;

}
