/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import CatalogCategorySearchType from "../../../../../Types/Elasticsearch/CatalogCategorySearchType";
import StoreType from "../../../../../Types/LocalStorage/StoreType";

export default interface CatalogCategorySearchRepositoryInterface {
    /**
     * Get all active categories
     */
    getAllActiveCategories(store: StoreType): Promise<CatalogCategorySearchType[]>;

    /**
     * Get category by id
     */
    getCategoryById(categoryId: number, store: StoreType): Promise<CatalogCategorySearchType | null>;
}
