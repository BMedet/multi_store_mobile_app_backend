/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import { isEmpty, isNull } from 'lodash';
import lodash from 'lodash';
import StoreType from '../../../../../Types/LocalStorage/StoreType';
import ElasticsearchAbstractRepository from '../../../AbstractElasticsearchRepository';
import SearchRepositoryInterface from './CatalogCategorySearchRepositoryInterface';
import IndexTypeEnum from '../../../../../Enums/IndexTypeEnum';
import ElasticHelper from '../../../../../../Helper/ElasticHelper';
import Helper from 'sosise-core/build/Helper/Helper';
import CatalogCategorySearchType from '../../../../../Types/Elasticsearch/CatalogCategorySearchType';
import magentoConfig from '../../../../../../config/magento';


export default class CatalogCategorySearchRepository extends ElasticsearchAbstractRepository implements SearchRepositoryInterface {

    protected INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.catalogCategory;

    /**
     * Get all active categories
     */
    public async getAllActiveCategories(store: StoreType): Promise<CatalogCategorySearchType[]> {
        // Prepare alias
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);

        // Prepare header
        const header = { 'Content-type': 'application/json' };

        // Prepare body
        const body =
        {
            "sort": [
                { "position": { "order": "asc" } },
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "isActive": 1
                            }
                        },
                        {
                            "term": {
                                "isHidden": 0
                            }
                        }
                    ]
                }
            }
        };
        // Make request
        const response = await this.makeRequest(`/${alias}/_search?size=10000`, 'POST', store, null, body, header);
        const hits = lodash.get(response, 'data.hits.hits', []);

        // Typecast
        const preparedCategories = new Array();
        for (const data of hits) {
            const thumbnail = !isNull(data._source.thumbnailImage) ? `${magentoConfig.category.thumbnailPathPrefix}${data._source.thumbnailImage}` : `${magentoConfig.media.plugImage}`;
            const preparedCategory: CatalogCategorySearchType = {
                entityId: data._source.entityId,
                name: data._source.name,
                urlPath: data._source.urlPath,
                thumbnailImage: `${magentoConfig.homePageUrl[store.entityId]}${thumbnail}`,
                showThumbnail: data._source.showThumbnail > 0,
                parentId: data._source.parentId,
                position: data._source.position,
                level: data._source.level,
                nameInMenu: data._source.nameInMenu,
                includeInMenu: data._source.includeInMenu > 0,
                filterableAttributes: data._source.filterableAttributes
            };

            preparedCategories.push(preparedCategory);
        }
        return preparedCategories;
    }

    /**
     * Get category by id
     */
    public async getCategoryById(categoryId: number, store: StoreType): Promise<CatalogCategorySearchType | null> {
        // Prepare alias
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);

        // Prepare header
        const header = { 'Content-type': 'application/json' };

        // Prepare body
        const body =
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "entityId": categoryId
                            }
                        }
                    ]
                }
            }
        };
        // Make request
        const response = await this.makeRequest(`/${alias}/_search?size=10000`, 'POST', store, null, body, header);
        const hits = lodash.get(response, 'data.hits.hits', []);

        if (!hits.length) return null;

        const thumbnail = !isNull(hits[0]._source.thumbnailImage) ? `${magentoConfig.category.thumbnailPathPrefix}${hits[0]._source.thumbnailImage}` : `${magentoConfig.media.plugImage}`;
        const preparedCategory: CatalogCategorySearchType = {
            entityId: hits[0]._source.entityId,
            name: hits[0]._source.name,
            urlPath: hits[0]._source.urlPath,
            thumbnailImage: `${magentoConfig.media.cdn}${thumbnail}`,
            showThumbnail: hits[0]._source.showThumbnail > 0,
            parentId: hits[0]._source.parentId,
            position: hits[0]._source.position,
            level: hits[0]._source.level,
            nameInMenu: hits[0]._source.nameInMenu,
            includeInMenu: hits[0]._source.includeInMenu > 0,
            filterableAttributes: hits[0]._source.filterableAttributes
        };

        return preparedCategory;
    }

}
