/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import ElasticsearchAbstractRepository from '../../../AbstractElasticsearchRepository';
import SearchRepositoryInterface from './CatalogAttributeSearchRepositoryInterface';
import IndexTypeEnum from '../../../../../Enums/IndexTypeEnum';
import ElasticHelper from '../../../../../../Helper/ElasticHelper';
import storeConfig from '../../../../../../config/store';
import Helper from 'sosise-core/build/Helper/Helper';
import FilterableAttributeType from '../../../../../Types/Elasticsearch/FilterableAttributeType';
import ProductAttributeOptionType from '../../../../../Types/MagentoDb/ProductAttributeOptionType';


export default class CatalogAttributeSearchRepository extends ElasticsearchAbstractRepository implements SearchRepositoryInterface {

    protected INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.catalogAttribute;


    /**
     * Get attributes by id
     */
    public async getAttributesById(attributeIds: string[]): Promise<FilterableAttributeType[]> {
        // Prepare alias
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, storeConfig.defaultStoreType.code);

        // Prepare header
        const header = { 'Content-type': 'application/json' };

        // Prepare body
        const body = {
            "sort": [
                { "position": { "order": "asc" } }
            ],
            "query": {
                "bool": {
                    "filter": {
                        "terms": {
                            "attributeId": attributeIds
                        }
                    }
                }
            }
        }

        // Make request
        const response = await this.makeRequest(`/${alias}/_search`, 'POST', storeConfig.defaultStoreType, null, body, header);

        // Typecast
        const preparedAttributes = new Array();
        for (const attribute of response.data.hits.hits) {
            const preparedAttribute: FilterableAttributeType = {
                id: attribute._source.id,
                enabled: attribute._source.enabled === 1,
                attributeId: attribute._source.attributeId,
                attributeCode: attribute._source.attributeCode,
                frontendLabel: attribute._source.frontendLabel,
                frontendInput: attribute._source.frontendInput,
                isVisibleOnFront: attribute._source.isVisibleOnFront === 1,
                isVisibleOnGrid: attribute._source.isVisibleOnGrid === 1,
                isFilterable: attribute._source.isFilterable === 1,
                isMultiselect: attribute._source.isMultiselect === 1,
                position: attribute._source.position,
                backendType: attribute._source.backendType,
                options: await this.getOptionsByAttributeId(attribute._source.attributeId)
            }
            preparedAttributes.push(preparedAttribute);
        }

        return preparedAttributes;
    }

    /**
     * Get options by attribute id
     */
    protected async getOptionsByAttributeId(attributeId: number): Promise<ProductAttributeOptionType[]> {
        // Prepare alias
        const alias = ElasticHelper.getAliasName(IndexTypeEnum.catalogAttributeOption, storeConfig.defaultStoreType.code);

        // Prepare header
        const header = { 'Content-type': 'application/json' };

        // Prepare body
        const body =
        {
            "sort": [
                { "sort_order": { "order": "asc" } }
            ],
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "attributeId": attributeId
                            }
                        }
                    ]
                }
            }
        };

        // Make request
        const response = await this.makeRequest(`/${alias}/_search`, 'POST', storeConfig.defaultStoreType, null, body, header);

        // Typecast
        const preparedOptions = new Array();
       
        for (const option of response.data.hits.hits) {
            const preparedOption: ProductAttributeOptionType = {
                id: option._source.id,
                attributeId: option._source.attributeId,
                optionId: option._source.optionId,
                value: option._source.value,
                valueId: option._source.valueId,
                sortOrder: option._source.sortOrder,
                createdAt: option._source.createdAt,
                updatedAt: option._source.updatedAt
            }
            preparedOptions.push(preparedOption);
        }
        return preparedOptions;
    }

}
