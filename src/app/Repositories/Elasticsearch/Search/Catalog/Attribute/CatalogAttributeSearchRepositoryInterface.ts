/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import FilterableAttributeType from "../../../../../Types/Elasticsearch/FilterableAttributeType";

export default interface CatalogAttributeSearchRepositoryInterface {

    /**
     * Get attributes by id
     */
    getAttributesById(attributeIds: string[]): Promise<FilterableAttributeType[]>;
}
