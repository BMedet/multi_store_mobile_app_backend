/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */


import ElasticsearchAbstractRepository from '../../../AbstractElasticsearchRepository';
import SearchRepositoryInterface from './CatalogProductSearchRepositoryInterface';
import IndexTypeEnum from '../../../../../Enums/IndexTypeEnum';
import CatalogProductSearchType from '../../../../../Types/Elasticsearch/CatalogProductSearchType';
import ElasticHelper from '../../../../../../Helper/ElasticHelper';
import StoreType from '../../../../../Types/LocalStorage/StoreType';
import lodash, { isNull } from 'lodash';
import ProductSortFieldEnum from '../../../../../Enums/ProductSortFieldEnum';
import SortValueEnum from '../../../../../Enums/SortValueEnum';
import CatalogProductSearchResultType from '../../../../../Types/Elasticsearch/CatalogProductSearchResultType';
import magentoConfig from '../../../../../../config/magento';
import ProductNotFoundException from '../../../../../Exceptions/Search/ProductNotFoundException';
import BindAttributeToProductType from '../../../../../Types/LocalStorage/BindAttributeToProductType';
import CatalogProductAttributeWithValueType from '../../../../../Types/LocalStorage/CatalogProductAttributeWithValueType';
import ProductListingFilterType from '../../../../../Types/Frontend/ProductListingFilterType';
import Helper from 'sosise-core/build/Helper/Helper';

/**
 * Sort params type
 */
type SortParamsType = {
    [key: string]: {
        order: SortValueEnum
    }
}

export default class CatalogProductSearchRepository extends ElasticsearchAbstractRepository implements SearchRepositoryInterface {

    protected INDEX_TYPE: IndexTypeEnum = IndexTypeEnum.catalogProduct;
    protected LOGGING_CHANNEL: string = 'frontend';

    /**
     * Get products by category id
     */
    public async getProductsByCategoryId(categoryId: number, store: StoreType, regionId: number, sortField: ProductSortFieldEnum, page: number, filter: ProductListingFilterType): Promise<CatalogProductSearchResultType> {
        // Prepare alias
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);

        // Prepare header
        const header = { 'Content-type': 'application/x-ndjson' };

        // Sorter params
        const sortParams = this.prepareSortParams(sortField, regionId);

        // Prepare body
        let getProductsBodyTemplate =
            {
                "sort": [sortParams],
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "isActive": 1
                                }
                            },
                            {
                                "term": {
                                    "isVisible": 1
                                }
                            },
                            {
                                "term": {
                                    "categoryIds": categoryId
                                }
                            }
                        ]
                    }
                },
                "aggs": {
                    "max_price": { "max": { "field": `regionData.${regionId}.price` } },
                    "min_price": { "min": { "field": `regionData.${regionId}.price` } }
                }
            } as any;

        // Get products request body
        let getProductsRequestBody = this.prepareRegionDataFilter(regionId, filter, lodash.cloneDeep(getProductsBodyTemplate))
        getProductsRequestBody = this.prepareAttributeFilter(filter, lodash.cloneDeep(getProductsRequestBody))
        getProductsRequestBody = this.preparePagination(lodash.cloneDeep(getProductsRequestBody), page, 40);

        // Get products aggregation request body
        let bodyWithOutPriceFilter = this.prepareRegionDataFilter(regionId, filter, lodash.cloneDeep(getProductsBodyTemplate), false)

        let preparedBody = `{"index": "${alias}"} \n`;
        preparedBody += `${JSON.stringify(getProductsRequestBody)} \n`;
        preparedBody += `{"index": "${alias}"} \n`;
        preparedBody += `${JSON.stringify(bodyWithOutPriceFilter)} \n`;


        // Make request
        const response = await this.makeRequest(`/${alias}/_msearch`, 'POST', store, null, preparedBody, header);
        const hits = lodash.get(response, 'data.responses[0].hits.hits', []);
        
        // Typecast product
        const preparedProducts = new Array();
        for (const data of hits) {
            const images = this.prepareImages(data._source.images, data._source.thumbnail, store);
            const product: CatalogProductSearchType = {

                entityId: data._source.entityId,
                name: data._source.name,
                sku: data._source.sku,
                price: this.preparePrice(data._source.regionData[regionId].price, data._source.specialPrice ?? 0),
                oldPrice: data._source.oldPrice ?? 0,
                description: data._source.description,
                thumbnail: images.thumbnail,
                images: images.images,
                url: data._source.oldPrice,
                categoryIds: data._source.categoryIds,
                qty: data._source.regionData[regionId].qty,
                isInStock: data._source.regionData[regionId].isInStock // @todo inventory rule!!!!!!!!!!! 
            };
            preparedProducts.push(product);
        }

        // Prepare search result
        const result: CatalogProductSearchResultType = {
            totalCount: response.data.responses[0].hits.total.value,
            count: preparedProducts.length,
            page,
            products: preparedProducts,
            sort: {
                field: sortField,
                value: sortParams[Object.keys(sortParams)[1]].order
            },
            aggregation: {
                maxPrice: lodash.get(response, 'data.responses[1].aggregations.max_price.value', 0),
                minPrice: lodash.get(response, 'data.responses[1].aggregations.min_price.value', 0)
            }
        };
        return result;
    }

    /**
     * Get product by id
     */
    public async getProductById(store: StoreType, productId: number, regionId: number): Promise<CatalogProductSearchType> {

        // Prepare alias
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);

        // Prepare header
        const header = { 'Content-type': 'application/json' };

        // Prepare body
        const body =
            {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "entityId": productId
                                }
                            }
                        ]
                    }
                }
            } as any;

        // Make request
        const response = await this.makeRequest(`/${alias}/_search`, 'POST', store, null, body, header);
        const hits = lodash.get(response, 'data.hits.hits', []);

        if (!hits.length) {
            throw new ProductNotFoundException(store.entityId, 'Product not found', this.LOGGING_CHANNEL, { store, productId, regionId });
        }

        const data = hits[0];
        const images = this.prepareImages(data._source.images, data._source.thumbnail, store);
        const product: CatalogProductSearchType = {
            entityId: data._source.entityId,
            name: data._source.name,
            sku: data._source.sku,
            price: this.preparePrice(data._source.regionData[regionId].price, data._source.specialPrice ?? 0),
            oldPrice: data._source.oldPrice ?? 0,
            description: data._source.description,
            thumbnail: images.thumbnail,
            images: images.images,
            url: data._source.oldPrice,
            categoryIds: data._source.categoryIds,
            qty: data._source.regionData[regionId].qty,
            isInStock: data._source.regionData[regionId].isInStock // @todo inventory rule!!!!!!!!!!! 
        };

        return product;
    }

    /**
     * Get attribute options
     */
    public async getProductsFilterableAttributesByCategoryId(categoryId: number, store: StoreType, regionId: number, filter: ProductListingFilterType): Promise<CatalogProductAttributeWithValueType[]> {

        // Prepare alias
        const alias = ElasticHelper.getAliasName(this.INDEX_TYPE, store.code);

        // Prepare header
        const header = { 'Content-type': 'application/json' };

        // Prepare body
        const body =
            {
                "_source": ["attributes"],
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "isActive": 1
                                }
                            },
                            {
                                "term": {
                                    "isVisible": 1
                                }
                            },
                            {
                                "term": {
                                    "categoryIds": categoryId
                                }
                            }
                        ]
                    }
                }
            } as any;


        // Make request
        const response = await this.makeRequest(`/${alias}/_search?size=100000`, 'POST', store, null, body, header);
        const hits = lodash.get(response, 'data.hits.hits', []);

        // Typecast
        const preparedAttributes = new Array();
        for (const data of hits) {

            const attributes: BindAttributeToProductType = data._source.attributes;

            if (isNull(attributes)) continue;

            for (const [attributeId, attribute] of Object.entries(attributes)) {
                const customAttribute = attribute as any;
                if (!customAttribute.isFilterable || !customAttribute.enabled) continue;
                preparedAttributes.push(customAttribute);
            }

        }
        return preparedAttributes;
    }

    /**
     * Prepare images data
     */
    private prepareImages(images: string | null, thumbnail: string | null, store: StoreType): { thumbnail: string, images: string[] } {

        // Get prefix
        const imagePrefix = magentoConfig.product.imagePrefix;

        // Prepare images
        const preparedImages = new Array();
        if (!isNull(images)) {
            for (const image of images.split(',')) {
                if (image.includes('/migrated//')) continue;
                const preparedImage = `${magentoConfig.media.cdn}${imagePrefix}${image.replace(/\\/g, '').trim()}`;
                if (preparedImage === thumbnail) continue;
                preparedImages.push(preparedImage);
            }
        }

        // Prepare thumbnail
        let preparedThumbnail = `${magentoConfig.homePageUrl[store.entityId]}${magentoConfig.media.plugImage}`;
        if (!isNull(thumbnail) && thumbnail !== 'no_selection') {
            preparedThumbnail = `${magentoConfig.media.cdn}${magentoConfig.product.imagePrefix}${thumbnail.replace(/\\/g, '')}`;
        }

        if ((isNull(thumbnail) || thumbnail === 'no_selection') && preparedImages.length) {
            preparedThumbnail = preparedImages[0];
        }

        return { thumbnail: preparedThumbnail, images: preparedImages };
    }

    /**
     * Prepare price
     */
    private preparePrice(price: number, specialPrice: number): number {

        if (specialPrice < price) {
            return specialPrice;
        }

        return price;
    }

    /**
     * Prepare sort params
     */
    private prepareSortParams(sortValue: ProductSortFieldEnum, regionId: number): SortParamsType {
        // Prepare sort field
        let sortParams = {
            field: sortValue as string, order: SortValueEnum.desc
        };
        switch (sortValue) {
            case ProductSortFieldEnum.bestseller:
                sortParams = {
                    field: ProductSortFieldEnum.bestseller, order: SortValueEnum.desc
                };
                break;
            case ProductSortFieldEnum.date:
                sortParams = {
                    field: ProductSortFieldEnum.date, order: SortValueEnum.desc
                };
                break;
            case ProductSortFieldEnum.discount:
                sortParams = {
                    field: ProductSortFieldEnum.discount, order: SortValueEnum.desc
                };
                break;
            case ProductSortFieldEnum.priceDown:
                sortParams = {
                    field: `regionData.${regionId}.${ProductSortFieldEnum.price}`, order: SortValueEnum.desc
                };
                break;
            case ProductSortFieldEnum.priceUp:
                sortParams = {
                    field: `regionData.${regionId}.${ProductSortFieldEnum.price}`, order: SortValueEnum.asc
                };
                break;
        }

        // Prepare sort object
        const sort = {};
        sort[`regionData.${regionId}.isInStock`] = { "order": 'desc' };
        sort[sortParams.field] = { "order": sortParams.order };
        return sort;

    }


    /**
     * Prepare region product qty and price field
     */
    private prepareRegionDataFilter(regionId: number, filter: ProductListingFilterType, body: any, applyFilterPrice: boolean = true): any {

        // Prepare region fields
        const regionQtyQuery = {};
        const regionPriceQuery = {};
        if (filter.stock[0] === 'all') {
            regionQtyQuery[`regionData.${regionId}.qty`] = { "gte": 0 };
        } else {
            regionQtyQuery[`regionData.${regionId}.qty`] = { "gte": 1 };
        }

        regionPriceQuery[`regionData.${regionId}.price`] = {
            "gte": filter.price.min,
            "lte": filter.price.max,
        };

        // Push region query
        body.query.bool.must.push({ range: regionQtyQuery });
        if (filter.price.max && applyFilterPrice) {
            body.query.bool.must.push({ range: regionPriceQuery });
        }

        return body;
    }

    /**
     * Prepare pagination
     */
    private preparePagination(body: any, page: number = 1, pageSize: number = 40): any {
        // Pagination
        body.size = pageSize;
        body.from = (page - 1) * pageSize;
        return body;
    }

    /**
     * Prepare attribute filter
     */
    private prepareAttributeFilter(filter: ProductListingFilterType, body: any): any {

        for(const [attributeId, values] of Object.entries(filter.attribute)) {
            if(!values.length) continue;
            const boolQuery = {
                "bool": {
                    "should": new Array()
                }
            };

            for(const value of values) {

                const termTemplate = {
                    "term": {}
                };
                termTemplate.term[`attributes.${attributeId}.value`] = value;
                boolQuery.bool.should.push(termTemplate)
            }
            
            body.query.bool.must.push(boolQuery)
        }

        return body;
    }

}
