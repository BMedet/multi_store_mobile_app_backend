/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import ProductSortFieldEnum from "../../../../../Enums/ProductSortFieldEnum";
import SortValueEnum from "../../../../../Enums/SortValueEnum";
import CatalogProductSearchResultType from "../../../../../Types/Elasticsearch/CatalogProductSearchResultType";
import CatalogProductSearchType from "../../../../../Types/Elasticsearch/CatalogProductSearchType";
import ProductListingFilterType from "../../../../../Types/Frontend/ProductListingFilterType";
import CatalogProductAttributeWithValueType from "../../../../../Types/LocalStorage/CatalogProductAttributeWithValueType";
import StoreType from "../../../../../Types/LocalStorage/StoreType";

export default interface CatalogProductSearchRepositoryInterface {
    /**
     * Get products by category id
     */
    getProductsByCategoryId(categoryId: number, store: StoreType, regionId: number, sortField: ProductSortFieldEnum, page: number, filter: ProductListingFilterType): Promise<CatalogProductSearchResultType>;

    /**
     * Get products filterable attributes
     */
    getProductsFilterableAttributesByCategoryId(categoryId: number, store: StoreType, regionId: number, filter: ProductListingFilterType): Promise<CatalogProductAttributeWithValueType[]>;

    /**
     * Get product by id
     */
    getProductById(store: StoreType, productId: number, regionId: number): Promise<CatalogProductSearchType>;
}
