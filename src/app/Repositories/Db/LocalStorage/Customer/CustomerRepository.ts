/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import { isNull } from 'lodash';
import Helper from 'sosise-core/build/Helper/Helper';
import CustomerInitType from '../../../../Types/LocalStorage/CustomerInitType';
import CustomerInitUnifier from '../../../../Unifiers/CustomerInitUnifier';
import CustomerUpdateUnifier from '../../../../Unifiers/CustomerUpdateUnifier';
import AbstractLocalStorageRespository from '../AbstractLocalStorageRespository';
import CustomerRepositoryInterface from './CustomerRepositoryInterface';

export default class CustomerRepository extends AbstractLocalStorageRespository implements CustomerRepositoryInterface {

    /**
     * Init customer
     */
    public async init(customerInitUnifier: CustomerInitUnifier): Promise<CustomerInitType> {
        const values = `(${customerInitUnifier.storeId},
            "${customerInitUnifier.mobileUuid}",
            ${!isNull(customerInitUnifier.platform) ? '"' + customerInitUnifier.platform + '"' : null},
            ${!isNull(customerInitUnifier.platformVersion) ? '"' + customerInitUnifier.platformVersion + '"' : null},
            ${!isNull(customerInitUnifier.model) ? '"' + customerInitUnifier.model + '"' : null},
            ${!isNull(customerInitUnifier.manufacturer) ? '"' + customerInitUnifier.manufacturer + '"' : null},
            ${!isNull(customerInitUnifier.serial) ? '"' + customerInitUnifier.serial + '"' : null},
            ${!isNull(customerInitUnifier.sdkVersion) ? '"' + customerInitUnifier.sdkVersion + '"' : null},
            "${Helper.getCurrentDateTime()}",
            ${!isNull(customerInitUnifier.cordovaVersion) ? '"' + customerInitUnifier.cordovaVersion + '"' : null})`;
        const raw = `INSERT INTO customer (store_entity_id, mobile_uuid, platform, platform_version, model, manufacturer, serial, sdk_version, last_visit, cordova_version)
            VALUES ${values}
            ON DUPLICATE KEY UPDATE
                last_visit=VALUES(last_visit)`;

        // Insert
        const response = await this.dbClient.raw(raw);

        // Get customer id
        const customerId = response[0].insertId;

        // Get inserted customer
        return await this.getCustomerById(customerId);
    }

    /**
     * Update customer
     */
    public async update(customerUpdateUnifier: CustomerUpdateUnifier): Promise<CustomerInitType> {
        
        await this.dbClient.table('customer')
            .update({
                region_entity_id: customerUpdateUnifier.regionId, 
                language: customerUpdateUnifier.language, 
                theme: customerUpdateUnifier.theme
            })
            .where('id', customerUpdateUnifier.customerId)

        await this.dbClient.table('customer_info')
            .update({
                name: customerUpdateUnifier.name, 
                last_name: customerUpdateUnifier.lastName, 
                patronymic: customerUpdateUnifier.patronymic, 
                email: customerUpdateUnifier.email, 
                mobile: customerUpdateUnifier.mobile, 
                dob: customerUpdateUnifier.dob, 
                sex: customerUpdateUnifier.sex
            })
            .where('customer_app_data_id', customerUpdateUnifier.customerId)

        // Get customer by id
        return await this.getCustomerById(customerUpdateUnifier.customerId);
    }

    /**
     * Get customer by id
     */
    protected async getCustomerById(customerId: number): Promise<CustomerInitType> {

        return await this.dbClient.table('customer as c')
            .select([
                'c.id as customerId',
                'c.mobile_uuid as mobileUuid',
                'c.platform as platform',
                'c.platform as platformVersion',
                'c.model as model',
                'c.manufacturer as manufacturer',
                'c.serial as serial',
                'c.sdk_version as sdkVersion',
                'c.cordova_version as cordovaVersion',
                'c.region_entity_id as regionId',
                'c.language as language',
                'c.is_authorized as isAuthorized',
                'c.theme as theme',
                'ci.entity_id as entityId',
                'ci.loyalty_id as loyaltyId',
                'ci.name as name',
                'ci.last_name as lastName',
                'ci.patronymic as patronymic',
                'ci.email as email',
                'ci.mobile as mobile',
                'ci.dob as dob',
                'ci.sex as sex'
            ])
            .leftJoin('customer_info as ci', 'ci.customer_app_data_id', 'c.id')
            .where('c.id', customerId)
            .first();
    }
}
