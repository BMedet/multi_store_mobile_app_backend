/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import CustomerInitType from "../../../../Types/LocalStorage/CustomerInitType";
import CustomerInitUnifier from "../../../../Unifiers/CustomerInitUnifier";
import CustomerUpdateUnifier from "../../../../Unifiers/CustomerUpdateUnifier";

export default interface CustomerRepositoryInterface {
    /**
     * Init customer
     */
    init(customerInitUnifier: CustomerInitUnifier): Promise<CustomerInitType>;

    /**
     * Update customer
     */
    update(customerUpdateUnifier: CustomerUpdateUnifier): Promise<CustomerInitType>;
}
