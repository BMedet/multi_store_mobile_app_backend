
/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

export default interface LogRepositoryInterface {
    /**
     * Debug log
     */
    debug(message: string, channel: string, params?: string | null, storeId?: number | null): Promise<void>;

    /**
     * Info log
     */
    info(message: string, channel: string, params?: string | null, storeId?: number | null): Promise<void>;

    /**
     * Warning log
     */
    warning(message: string, channel: string, params?: string | null, storeId?: number | null): Promise<void>;

    /**
     * Error log
     */
    error(message: string, channel: string, params?: string | null, storeId?: number | null): Promise<void>;

    /**
     * Critical log
     */
    critical(message: string, channel: string, params?: string | null, storeId?: number | null): Promise<void>;
}
