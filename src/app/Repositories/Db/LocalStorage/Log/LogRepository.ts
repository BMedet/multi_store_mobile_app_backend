/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import LogRespositoryInterface from './LogRepositoryInterface';
import AbstractLocalStorageRespository from '../AbstractLocalStorageRespository';
import LogLevelEnum from '../../../../Enums/LogLevelEnum';

export default class LogRepository extends AbstractLocalStorageRespository implements LogRespositoryInterface {
    /**
     * Debug log
     */
    public async debug(message: string, channel: string, params: string | null = null, storeId: number = 0): Promise<void> {
        await this.dbClient.table('store_log').insert({
            level: LogLevelEnum.debug,
            channel,
            message,
            params,
            store_entity_id: storeId
        });
    }

    /**
     * Info log
     */
    public async info(message: string, channel: string, params: string | null = null, storeId: number = 0): Promise<void> {
        await this.dbClient.table('store_log').insert({
            level: LogLevelEnum.info,
            channel,
            message,
            params,
            store_entity_id: storeId
        });
    }

    /**
     * Warning log
     */
    public async warning(message: string, channel: string, params: string | null = null, storeId: number = 0): Promise<void> {
        await this.dbClient.table('store_log').insert({
            level: LogLevelEnum.warning,
            channel,
            message,
            params,
            store_entity_id: storeId
        });
    }

    /**
     * Error log
     */
    public async error(message: string, channel: string, params: string | null = null, storeId: number = 0): Promise<void> {
        await this.dbClient.table('store_log').insert({
            level: LogLevelEnum.error,
            channel,
            message,
            params,
            store_entity_id: storeId
        });
    }

    /**
     * Critical log
     */
    public async critical(message: string, channel: string, params: string | null = null, storeId: number = 0): Promise<void> {
        await this.dbClient.table('store_log').insert({
            level: LogLevelEnum.critical,
            channel,
            message,
            params,
            store_entity_id: storeId
        });
    }
}
