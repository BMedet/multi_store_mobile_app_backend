/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import StoreRepositoryInterface from './StoreRepositoryInterface';
import AbstractLocalStorageRespository from '../AbstractLocalStorageRespository';
import StoreCodeEnum from '../../../../Enums/StoreCodeEnum';
import StoreType from '../../../../Types/LocalStorage/StoreType';
import StoreNotFoundException from '../../../../Exceptions/StoreNotFoundException';

export default class StoreRepository extends AbstractLocalStorageRespository implements StoreRepositoryInterface {


    /**
     * Get store by code
     */
    public async getActiveStoreByCode(code: StoreCodeEnum): Promise<StoreType[]> {

        // Init stores
        let result = new Array();
        const stores = new Array();
        // Get all stores
        if (code === StoreCodeEnum.all) {
            // Get all stores
            result = await this.dbClient.table('store')
                .where('is_active', true);
        } else {
            result = await this.dbClient.table('store')
                .where('code', code)
                .andWhere('is_active', true);
        }

        // If not found
        if (!result.length) {
            throw new StoreNotFoundException(0, 'Store not found in db', 'index', { code });
        }

        // Typecast
        for (const store of result) {
            const preparedStore: StoreType = {
                id: store.id,
                entityId: store.entity_id,
                label: store.label,
                code: store.code,
                baseUrl: store.base_url,
                token: store.token,
                isActive: store.is_active,
                createdAt: store.created_at,
                updatedAt: store.updated_at,
            };

            stores.push(preparedStore);
        }

        return stores;
    }

    /**
     * Get store by code
     */
    public async getActiveStoreByEntityId(entityId: string | number): Promise<StoreType> {

        const response = await this.dbClient.table('store')
            .where('entity_id', entityId)
            .andWhere('is_active', true)
            .first();

        // If not found
        if (!response) {
            throw new StoreNotFoundException(0, 'Store not found in db', 'frontend', { entityId });
        }

        // Typecast
        const preparedStore: StoreType = {
            id: response.id,
            entityId: response.entity_id,
            label: response.label,
            code: response.code,
            baseUrl: response.base_url,
            token: response.token,
            isActive: response.is_active,
            createdAt: response.created_at,
            updatedAt: response.updated_at,
        };

        return preparedStore;
    }


    /**
     * Get all active stores
     */
    public async getAllActiveStores(): Promise<StoreType[]> {
        const response = await this.dbClient.table('store')
            .andWhere('is_active', true);

        // Typecast
        const preparedStores = new Array();
        for (const store of response) {
            const preparedStore: StoreType = {
                id: store.id,
                entityId: store.entity_id,
                label: store.label,
                code: store.code,
                baseUrl: store.base_url,
                token: store.token,
                isActive: store.is_active,
                createdAt: store.created_at,
                updatedAt: store.updated_at,
            };

            preparedStores.push(preparedStore);
        }
        return preparedStores;
    }
}
