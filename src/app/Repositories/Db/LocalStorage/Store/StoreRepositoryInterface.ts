/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import StoreCodeEnum from "../../../../Enums/StoreCodeEnum";
import StoreType from "../../../../Types/LocalStorage/StoreType";

export default interface StoreRepositoryInterface {

    /**
     * Get store by code
     */
    getActiveStoreByCode(code: StoreCodeEnum): Promise<StoreType[]>;

    /**
     * Get active store by entityId
     */
    getActiveStoreByEntityId(entityId: number | string): Promise<StoreType>;

    /**
     * Get all active stores
     */
    getAllActiveStores(): Promise<StoreType[]>;
}
