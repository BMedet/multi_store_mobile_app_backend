/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import Database from 'sosise-core/build/Database/Database';
import { Knex } from 'knex';
import { isNull } from 'lodash';

export default abstract class AbstractLocalStorageRespository {

    protected dbClient: Knex;

    /**
     * Constructor
     */
    constructor() {
        this.dbClient = Database.getConnection(process.env.DB_PROJECT_CONNECTION as string).client;
    }


    /**
     * Convert string
     */
    protected mysqlEscape(stringToEscape: string | null): string | null {
        if (isNull(stringToEscape)) return null;

        return stringToEscape.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, (char: string): any => {
            switch (char) {
                case "\0":
                    return "\\0";
                case "\x08":
                    return "\\b";
                case "\x09":
                    return "\\t";
                case "\x1a":
                    return "\\z";
                case "\n":
                    return "\\n";
                case "\r":
                    return "\\r";
                case "\"":
                case "\\":
                    return " ";
                case "%":
                case "'":
                    return char;
            }
        });
    }

}
