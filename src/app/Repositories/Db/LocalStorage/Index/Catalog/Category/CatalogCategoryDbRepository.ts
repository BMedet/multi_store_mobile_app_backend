/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import CatalogCategoryFromMagentoType from '../../../../../../Types/MagentoApi/CatalogCategoryFromMagentoType';
import StoreType from '../../../../../../Types/LocalStorage/StoreType';
import { isNull } from 'lodash';
import dayjs from 'dayjs';
import elasticsearchConfig from '../../../../../../../config/elasticsearch';
import CatalogCategoryDbRepositoryInterface from './CatalogCategoryDbRepositoryInterface';
import AbstractLocalStorageRespository from '../../../AbstractLocalStorageRespository';
import Helper from 'sosise-core/build/Helper/Helper';

export default class CatalogCategoryDbRepository extends AbstractLocalStorageRespository implements CatalogCategoryDbRepositoryInterface {

    /**
     * Index catalog categories
     */
    public async index(catalogCategories: CatalogCategoryFromMagentoType[], store: StoreType): Promise<number[]> {

        const ids = new Array();

        // Init query string
        let values = '';
        const categoriesCount = catalogCategories.length;
        let i = 0;

        // Prepare query string
        for (const category of catalogCategories) {

            ids.push(category.entityId);

            i++;
            values += `(${category.entityId},
                "${this.mysqlEscape(category.name)}",
                ${!isNull(category.nameInMenu) ? '"' + this.mysqlEscape(category.nameInMenu) + '"' : null},
                ${!isNull(category.urlPath) ? '"' + category.urlPath + '"' : null},
                ${!isNull(category.thumbnailImage) ? '"' + category.thumbnailImage + '"' : null},
                ${category.showThumbnail},
                ${category.parentId},
                ${category.position},
                ${store.entityId},
                ${category.isActive},
                ${category.isHidden},
                ${category.includeInMenu},
                ${!isNull(category.filterableAttributes) ? '"' + category.filterableAttributes + '"' : null},
                ${category.level},
                "${category.magentoCreatedAt}",
                "${category.magentoUpdatedAt}")`;
            if (i === categoriesCount) break;

            values += ', ';
        }

        const raw = `INSERT INTO index_catalog_category (entity_id, name, name_in_menu, url_path, thumbnail_image, show_thumbnail, parent_id, position, store_entity_id, is_active, is_hidden, include_in_menu, filterable_attributes, level, magento_created_at, magento_updated_at)
                    VALUES ${values}
                    ON DUPLICATE KEY UPDATE
                        name=VALUES(name),
                        name_in_menu=VALUES(name_in_menu),
                        url_path=VALUES(url_path),
                        thumbnail_image=VALUES(thumbnail_image),
                        show_thumbnail=VALUES(show_thumbnail),
                        parent_id=VALUES(parent_id),
                        position=VALUES(position),
                        is_active=VALUES(is_active),
                        is_hidden=VALUES(is_hidden),
                        include_in_menu=VALUES(include_in_menu),
                        filterable_attributes=VALUES(filterable_attributes),
                        level=VALUES(level),
                        magento_created_at=VALUES(magento_created_at),
                        magento_updated_at=VALUES(magento_updated_at)`;

        await this.dbClient.raw(raw);

        return ids;
    }

    /**
     * Disable inactive category ids
     */
    public async disableInactiveIndexes(updatedCategoryIds: number[], store: StoreType): Promise<void> {
        await this.dbClient.table('index_catalog_category')
            .where('store_entity_id', store.entityId)
            .whereNotIn('entity_id', updatedCategoryIds)
            .update('is_active', false);
    }

    /**
     * Get updated catalog categories
     */
    public async getUpdatedInLastDateIndexes(skipCount: number, perIterationNumber: number, isForce: boolean, store: StoreType): Promise<CatalogCategoryFromMagentoType[]> {

        const categories = await this.dbClient.table('index_catalog_category')
            .select([
                'id',
                'entity_id as entityId',
                'name',
                'url_path as urlPath',
                'thumbnail_image as thumbnailImage',
                'show_thumbnail as showThumbnail',
                'parent_id as parentId',
                'position',
                'is_active as isActive',
                'is_hidden as isHidden',
                'level',
                'name_in_menu as nameInMenu',
                'include_in_menu as includeInMenu',
                'filterable_attributes as filterableAttributes',
                'magento_created_at as magentoCreatedAt',
                'magento_updated_at as magentoUpdatedAt',
                'created_at as createdAt',
                'updated_at as updateddAt',
            ])
            .where('store_entity_id', store.entityId)
            .modify(function(queryBuilder) {
                if (isForce) {
                    queryBuilder.andWhere('updated_at', '>', dayjs().subtract(elasticsearchConfig.index.substractDays, 'days').format('YYYY-MM-DD HH:mm:ss'));
                }
            })
            .offset(skipCount)
            .limit(perIterationNumber);
        return categories;

    }
}
