/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import dayjs from 'dayjs';
import { isNull, isSet } from 'lodash';
import Helper from 'sosise-core/build/Helper/Helper';
import elasticsearchConfig from '../../../../../../../config/elasticsearch';
import CatalogAttributeBackendTypeEnum from '../../../../../../Enums/CatalogAttributeBackendTypeEnum';
import BindAttributeToProductType from '../../../../../../Types/LocalStorage/BindAttributeToProductType';
import StoreType from '../../../../../../Types/LocalStorage/StoreType';
import GetAttributeValueType from '../../../../../../Types/MagentoDb/GetAttributeValueType';
import ProductAttributeOptionType from '../../../../../../Types/MagentoDb/ProductAttributeOptionType';
import ProductAttributeType from '../../../../../../Types/MagentoDb/ProductAttributeType';
import AbstractLocalStorageRespository from '../../../AbstractLocalStorageRespository';
import CatalogProductDbRepositoryInterface from './CatalogAttributeDbRepositoryInterface';


export default class CatalogAttributeDbRepository extends AbstractLocalStorageRespository implements CatalogProductDbRepositoryInterface {

    /**
     * Get attributes for index to elastic
     */
    public async getUpdatedInLastDateIndexes(skipCount: number, perIterationNumber: number, isForce: boolean, store: StoreType): Promise<ProductAttributeType[]> {

        const attributes = await this.dbClient.table('index_product_attribute as ipa')
            .select([
                'ipa.id as id',
                'ipa.enabled as enabled',
                'ipa.attribute_id as attributeId',
                'ipa.attribute_code as attributeCode',
                'ipa.frontend_label as frontendLabel',
                'ipa.frontend_input as frontendInput',
                'ipa.is_visible_on_front as isVisibleOnFront',
                'ipa.is_visible_on_grid as isVisibleOnGrid',
                'ipa.is_filterable as isFilterable',
                'ipa.is_multiselect as isMultiselect',
                'ipa.position as position',
                'ipa.backend_type as backendType'
            ])
            .modify(function (queryBuilder) {
                if (isForce) {
                    queryBuilder.andWhere('updated_at', '>', dayjs().subtract(elasticsearchConfig.index.substractDays, 'days').format('YYYY-MM-DD HH:mm:ss'));
                }
            })
            .offset(skipCount)
            .limit(perIterationNumber);

        return attributes;
    }

    /**
     * Index attributes
     */
    public async index(attributes: ProductAttributeType[]): Promise<number[]> {
        const ids = new Array();

        // Init query string
        let values = '';
        const attributesCount = attributes.length;
        let i = 0;

        // Prepare query string
        for (const attribute of attributes) {
            ids.push(attribute.attributeId);

            i++;
            values += `(${attribute.attributeId},
                "${attribute.attributeCode}",
                ${!isNull(attribute.frontendLabel) ? "'" + this.mysqlEscape(attribute.frontendLabel) + "'" : null},
                "${attribute.frontendInput}",
                ${attribute.isVisibleOnFront},
                ${attribute.isVisibleOnGrid},
                ${attribute.isFilterable},
                ${attribute.isMultiselect},
                "${attribute.backendType}",
                ${attribute.position})`;
            if (i === attributesCount) break;

            values += ', ';
        }

        const raw = `INSERT INTO index_product_attribute (attribute_id, attribute_code, frontend_label, frontend_input, is_visible_on_front, is_visible_on_grid, is_filterable, is_multiselect, backend_type, position)
                    VALUES ${values}
                    ON DUPLICATE KEY UPDATE
                        attribute_code=VALUES(attribute_code),
                        frontend_label=VALUES(frontend_label),
                        frontend_input=VALUES(frontend_input),
                        is_visible_on_front=VALUES(is_visible_on_front),
                        is_visible_on_grid=VALUES(is_visible_on_grid),
                        is_filterable=VALUES(is_filterable),
                        is_multiselect=VALUES(is_multiselect),
                        backend_type=VALUES(backend_type),
                        position=VALUES(position)`;

        await this.dbClient.raw(raw);

        return ids;
    }

    /**
     * Disable inactive attributes
     */
    public async disableInactiveIndexes(attributeIds: number[], store: StoreType) {
        await this.dbClient.table('index_product_attribute')
            .whereNotIn('attribute_id', attributeIds)
            .update('enabled', false);
    }

    /**
     * Get all active attributes
     */
    public async getAllActiveAttributeIds(backendType: CatalogAttributeBackendTypeEnum): Promise<number[]> {

        const response = await this.dbClient.table('index_product_attribute')
            .select([
                'attribute_id as attributeId'
            ])
            .where('enabled', 1)
            .andWhere('backend_type', backendType);

        const preparedIds = new Array();
        for (const attribute of response) {
            preparedIds.push(attribute.attributeId);
        }
        return preparedIds;
    }

    /**
     * Save attribute options
     */
    public async saveAttributeOptions(attributeOptions: ProductAttributeOptionType[]): Promise<void> {
        // Init query string
        let values = '';
        const optionsCount = attributeOptions.length;
        let i = 0;

        // Prepare query string
        for (const option of attributeOptions) {
            i++;
            values += `(${option.attributeId},
                  "${option.optionId}",
                  ${!isNull(option.value) ? '"' + this.mysqlEscape(option.value) + '"' : null},
                  ${option.valueId},
                  ${option.sortOrder})`;
            if (i === optionsCount) break;

            values += ', ';
        }

        const raw = `INSERT INTO product_attribute_option (attribute_id, option_id, value, value_id, sort_order)
                      VALUES ${values}
                      ON DUPLICATE KEY UPDATE
                        option_id=VALUES(option_id),
                        value=VALUES(value),
                        value_id=VALUES(value_id),
                        sort_order=VALUES(sort_order)`;

        await this.dbClient.raw(raw);
    }

    /**
     * Save attribute value
     */
    public async saveAttributeValue(attributeValues: GetAttributeValueType[]): Promise<void> {
        // Init query string
        let values = '';
        const valueCount = attributeValues.length;
        let i = 0;

        // Prepare query string
        for (const value of attributeValues) {
            i++;
            values += `(${value.attributeId},
                    ${value.entityId},
                    ${!isNull(value.attributeValue) ? "'" + this.mysqlEscape(String(value.attributeValue)) + "'" : null})`;
            if (i === valueCount) break;

            values += ', ';
        }

        const raw = `INSERT INTO index_product_attribute_value (attribute_id, product_entity_id, value)
                      VALUES ${values}
                      ON DUPLICATE KEY UPDATE
                        value=VALUES(value)`;

        await this.dbClient.raw(raw);
    }

    /**
     * Get attribute options
     */
    public async getAttributeOptions(skipCount: number, perIterationNumber: number, isForce: boolean): Promise<ProductAttributeOptionType[]> {
        const attributeOptions = await this.dbClient.table('product_attribute_option as pao')
            .select([
                'pao.id as id',
                'pao.attribute_id as attributeId',
                'pao.option_id as optionId',
                'pao.value as value',
                'pao.value_id as valueId',
                'pao.sort_order as sortOrder',
                'pao.created_at as createdAt',
                'pao.updated_at as updatedAt'
            ])
            .modify(function (queryBuilder) {
                if (isForce) {
                    queryBuilder.andWhere('updated_at', '>', dayjs().subtract(elasticsearchConfig.index.substractDays, 'days').format('YYYY-MM-DD HH:mm:ss'));
                }
            })
            .offset(skipCount)
            .limit(perIterationNumber);

        return attributeOptions;
    }

    /**
     * Get attribute value with product entity id
     */
    public async getAttributeValueWithProductEntityId(limit: number, page: number): Promise<BindAttributeToProductType> {

        // Prepare query
        const query = `SELECT 
            ipav.attribute_id attribute_id,
            ipav.product_entity_id as entity_id,
            icp.store_ids as store_ids,
            ipav.value as value,
            IF(pao_option.value IS NOT NULL, pao_option.value, ipav.value) as label,
            ipa.enabled,
            ipa.attribute_code,
            ipa.frontend_label,
            ipa.frontend_input,
            ipa.is_visible_on_front,
            ipa.is_visible_on_grid,
            ipa.is_filterable,
            ipa.is_multiselect,
            ipa.position,
            ipa.backend_type
        FROM (SELECT 
                entity_id, 
                GROUP_CONCAT(DISTINCT(store_entity_id) SEPARATOR ',') as store_ids 
            FROM index_catalog_product
            WHERE is_active = 1
            GROUP BY entity_id 
            LIMIT ${(page - 1) * limit},${limit}) as icp
        LEFT JOIN index_product_attribute_value ipav ON ipav.product_entity_id = icp.entity_id
        LEFT JOIN index_product_attribute ipa ON ipa.attribute_id = ipav .attribute_id 
        LEFT JOIN product_attribute_option pao_option ON pao_option.option_id  = ipav.value
        WHERE ipav.value IS NOT NULL`;

        // Make request
        const response = await this.dbClient.raw(query);

        // Typecast
        const preparedValues = {};
        for (const perItem of response[0]) {
            // Prepare attribute
            const preparedAttribute = {
                storeIds: perItem.store_ids.split(','),
                attributeId: perItem.attribute_id,
                attributeCode: perItem.attribute_code,
                frontendLabel: perItem.frontend_label,
                frontendInput: perItem.frontend_input === 1,
                isVisibleOnFront: perItem.is_visible_on_front === 1,
                isVisibleOnGrid: perItem.is_visible_on_grid === 1,
                isFilterable: perItem.is_filterable === 1,
                isMultiselect: perItem.is_multiselect === 1,
                enabled: perItem.enabled === 1,
                value: perItem.value,
                label: perItem.label,
            };

            if (preparedValues[perItem.entity_id]) {
                preparedValues[perItem.entity_id][preparedAttribute.attributeId] = preparedAttribute
            } else {
                preparedValues[perItem.entity_id] = {};
                preparedValues[perItem.entity_id][preparedAttribute.attributeId] = preparedAttribute
            }

        }

        return preparedValues;
    }
}
