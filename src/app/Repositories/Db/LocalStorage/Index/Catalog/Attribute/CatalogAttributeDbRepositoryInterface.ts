/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import CatalogAttributeBackendTypeEnum from '../../../../../../Enums/CatalogAttributeBackendTypeEnum';
import BindAttributeToProductType from '../../../../../../Types/LocalStorage/BindAttributeToProductType';
import GetAttributeValueType from '../../../../../../Types/MagentoDb/GetAttributeValueType';
import ProductAttributeOptionType from '../../../../../../Types/MagentoDb/ProductAttributeOptionType';
import ProductAttributeType from '../../../../../../Types/MagentoDb/ProductAttributeType';
import LocalStorageIndexRepositoryInterface from '../../LocalStorageIndexRepositoryInterface';


export default interface CatalogAttributeDbRepositoryInterface extends LocalStorageIndexRepositoryInterface {

    /**
     * Save attribute options
     */
    saveAttributeOptions(attributeOptions: ProductAttributeOptionType[]): Promise<void>;

    /**
     * Get all active attribute ids
     */
    getAllActiveAttributeIds(frontendInput: CatalogAttributeBackendTypeEnum): Promise<number[]>;

    /**
     * Save attribute value
     */
    saveAttributeValue(attributeValues: GetAttributeValueType[]): Promise<void>;

    /**
     * Get attribute options
     */
    getAttributeOptions(skipCount: number, perIterationNumber: number, isForce: boolean): Promise<ProductAttributeOptionType[]>;

    /**
     * Get attribute value with product entity id
     */
    getAttributeValueWithProductEntityId(limit: number, page: number): Promise<BindAttributeToProductType>;
}
