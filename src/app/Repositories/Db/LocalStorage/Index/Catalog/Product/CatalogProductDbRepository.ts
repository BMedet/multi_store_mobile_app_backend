/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import dayjs from 'dayjs';
import { isNull } from 'lodash';
import Helper from 'sosise-core/build/Helper/Helper';
import elasticsearchConfig from '../../../../../../../config/elasticsearch';
import CatalogProductFromLocalStorageType from '../../../../../../Types/LocalStorage/CatalogproductFromLocalStorageType';
import PerRegionProductPriceDataType from '../../../../../../Types/LocalStorage/PerRegionProductPriceDataType';
import PerRegionProductStockDataType from '../../../../../../Types/LocalStorage/PerRegionProductStockDataType';
import ProductIdsType from '../../../../../../Types/LocalStorage/ProductIdsType';
import RegionIdsType from '../../../../../../Types/LocalStorage/RegionIdsType';
import StoreType from '../../../../../../Types/LocalStorage/StoreType';
import FirstIterationCatalogProductFromMagentoType from '../../../../../../Types/MagentoDb/CatalogProductFromMagentoType';
import GetProductContentType from '../../../../../../Types/MagentoDb/GetProductContentType';
import GetProductPricesType from '../../../../../../Types/MagentoDb/GetProductPricesType';
import GetProductSalesType from '../../../../../../Types/MagentoDb/GetProductSalesType';
import AbstractLocalStorageRespository from '../../../AbstractLocalStorageRespository';
import CatalogProductDbRepositoryInterface from './CatalogProductDbRepositoryInterface';


export default class CatalogProductDbRepository extends AbstractLocalStorageRespository implements CatalogProductDbRepositoryInterface {
    /**
     * Index catalog product
     */
    public async index(catalogProducts: FirstIterationCatalogProductFromMagentoType[], store: StoreType): Promise<number[]> {
        const ids = new Array();

        // Init query string
        let values = '';
        const productsCount = catalogProducts.length;
        let i = 0;

        // Prepare query string
        for (const product of catalogProducts) {
            ids.push(product.entityId);

            i++;
            values += `(${product.entityId},
                "${this.mysqlEscape(product.name)}",
                "${product.sku}",
                ${product.isVisible !== 0},
                ${product.isActive},
                ${store.entityId},
                ${product.qty},
                ${!isNull(product.thumbnail) ? '"' + product.thumbnail + '"' : null},
                ${!isNull(product.categoryIds) ? '"' + product.categoryIds + '"' : null},
                ${0},
                ${null},
                ${null},
                ${null},
                ${null},
                "${dayjs(product.magentoCreatedAt).format('YYYY-MM-DD HH:mm:ss')}")`;

            if (i === productsCount) break;
            values += ', ';
        }

        const raw = `INSERT INTO index_catalog_product (entity_id, name, sku, is_visible, is_active, store_entity_id, qty, thumbnail, category_ids, price, special_price, old_price, description, url, magento_created_at)
                    VALUES ${values}
                    ON DUPLICATE KEY UPDATE
                        name=VALUES(name),
                        sku=VALUES(sku),
                        is_visible=VALUES(is_visible),
                        is_active=VALUES(is_active),
                        qty=VALUES(qty),
                        thumbnail=VALUES(thumbnail),
                        magento_created_at=VALUES(magento_created_at),
                        category_ids=VALUES(category_ids)`;

        await this.dbClient.raw(raw);

        return ids;
    }

    /**
     * Set product sales info
     */
    public async setProductSalesInfo(products: GetProductSalesType[], store: StoreType): Promise<void> {
        // Init query string
        let enityIds = '(';
        let qtyCase = '(CASE entity_id';
        const productsCount = products.length;
        let i = 0;

        // Prepare query string
        for (const product of products) {

            i++;
            qtyCase += `
                 WHEN ${product.entityId} THEN ${product.qty}
             `;

            enityIds += `${product.entityId}`;

            if (i === productsCount) {
                qtyCase += 'END)';
                enityIds += ')';
                break;
            }
            enityIds += ',';
        }

        const raw = `UPDATE index_catalog_product SET
                     sales_qty = ${qtyCase}
                     WHERE entity_id in ${enityIds} AND store_entity_id = ${store.entityId}
                     `;
        await this.dbClient.raw(raw);
    }

    /**
     * Disable inactive product ids
     */
    public async disableInactiveIndexes(updatedProductIds: number[], store: StoreType): Promise<void> {
        await this.dbClient.table('index_catalog_product')
            .where('store_entity_id', store.entityId)
            .whereNotIn('entity_id', updatedProductIds)
            .update('is_active', false);
    }

    /**
     * Set product prices
     */
    public async setProductPrices(productsWithPrices: GetProductPricesType[], store: StoreType): Promise<void> {

        // Init query string
        let enityIds = '(';
        let priceCase = '(CASE entity_id';
        let specialPriceCase = '(CASE entity_id';
        let oldPriceCase = '(CASE entity_id';
        const productsCount = productsWithPrices.length;
        let i = 0;

        // Prepare query string
        for (const product of productsWithPrices) {

            i++;
            priceCase += `
                WHEN ${product.entityId} THEN ${product.price}
            `;
            specialPriceCase += `
                WHEN ${product.entityId} THEN ${product.specialPrice}
            `;
            oldPriceCase += `
                WHEN ${product.entityId} THEN ${product.oldPrice}
            `;
            enityIds += `${product.entityId}`;

            if (i === productsCount) {
                priceCase += 'END),';
                specialPriceCase += 'END),';
                oldPriceCase += 'END)';
                enityIds += ')';
                break;
            }
            enityIds += ',';
        }

        const raw = `UPDATE index_catalog_product SET
                    price = ${priceCase}
                    special_price = ${specialPriceCase}
                    old_price = ${oldPriceCase}
                    WHERE entity_id in ${enityIds} AND  store_entity_id = ${store.entityId}
                    `;
        await this.dbClient.raw(raw);
    }

    /**
     * Set product content
     */
    public async setProductContent(productsWithContent: GetProductContentType[], store: StoreType): Promise<void> {
        // Init query string
        let enityIds = '(';
        let urlCase = '(CASE entity_id';
        let imagesCase = '(CASE entity_id';
        let descriptionCase = '(CASE entity_id';
        const productsCount = productsWithContent.length;
        let i = 0;

        // Prepare query string
        for (const product of productsWithContent) {

            i++;
            urlCase += `
                WHEN ${product.entityId} THEN "${product.url}"
            `;
            imagesCase += `
                WHEN ${product.entityId} THEN ${!isNull(product.images) ? '"' + product.images + '"' : null}
            `;
            descriptionCase += `
                WHEN ${product.entityId} THEN ${!isNull(product.description) ? '"' + this.mysqlEscape(product.description) + '"' : null}
            `;
            enityIds += `${product.entityId}`;

            if (i === productsCount) {
                urlCase += 'END),';
                imagesCase += 'END),';
                descriptionCase += 'END)';
                enityIds += ')';
                break;
            }
            enityIds += ',';
        }

        // Prepare query string
        const raw = `UPDATE index_catalog_product SET
                    url = ${urlCase}
                    images = ${imagesCase}
                    description = ${descriptionCase}
                    WHERE entity_id in ${enityIds} AND  store_entity_id = ${store.entityId}
                    `;

        // Make request
        await this.dbClient.raw(raw);
    }

    /**
     * Get all active product ids
     */
    public async getAllProductIds(updatedItemEntityIds: number[], skipCount: number, perIterationNumber: number, store: StoreType): Promise<ProductIdsType[]> {

        const products = await this.dbClient.table('index_catalog_product')
            .select([
                'id',
                'entity_id as entityId',
                'price'
            ])
            .whereIn('entity_id', updatedItemEntityIds)
            .where('store_entity_id', store.entityId)
            .offset(skipCount)
            .limit(perIterationNumber);
        return products;
    }

    /**
     * Get all region ids
     */
    public async getAllRegionEntityIds(store: StoreType): Promise<RegionIdsType[]> {
        const regions = await this.dbClient.table('index_region')
            .select([
                'id',
                'entity_id as entityId'
            ])
            .where('store_entity_id', store.entityId);

        return regions;
    }

    /**
     * Set product region stock data
     */
    public async setProductRegionStockData(productsWidthRegionStockData: PerRegionProductStockDataType[]): Promise<void> {

        // Init query string
        let values = '';
        const productsCount = productsWidthRegionStockData.length;
        let i = 0;

        // Prepare query string
        for (const data of productsWidthRegionStockData) {

            i++;
            values += `(${data.regionId},
                ${data.productId},
                ${data.qty})`;

            if (i === productsCount) break;
            values += ', ';
        }

        if (!i) return;

        const raw = `INSERT INTO index_region_product_stock (region_id, product_id, qty)
                    VALUES ${values}
                    ON DUPLICATE KEY UPDATE
                        qty=VALUES(qty)`;

        await this.dbClient.raw(raw);
    }

    /**
     * Set product region price data
     */
    public async setProductRegionPriceData(productsWidthRegionPriceData: PerRegionProductPriceDataType[]): Promise<void> {
        if (!productsWidthRegionPriceData.length) return;

        // Init query string
        let values = '';
        const productsCount = productsWidthRegionPriceData.length;
        let i = 0;

        // Prepare query string
        for (const data of productsWidthRegionPriceData) {

            i++;
            values += `(${data.regionId},
                ${data.productId},
                ${data.price})`;

            if (i === productsCount) break;
            values += ', ';
        }

        const raw = `INSERT INTO index_region_product_price (region_id, product_id, price)
                    VALUES ${values}
                    ON DUPLICATE KEY UPDATE
                        price=VALUES(price)`;

        await this.dbClient.raw(raw);
    }

    /**
     * Get updated catalog categories
     */
    public async getUpdatedInLastDateIndexes(skipCount: number, perIterationNumber: number, isForce: boolean, store: StoreType): Promise<CatalogProductFromLocalStorageType[]> {

        // Prepare condition
        let condition = `store_entity_id = ${store.entityId} AND is_active=1 AND is_visible=1`;

        // Is force index
        if (isForce) {
            condition += ` AND updated_at > "${dayjs().subtract(elasticsearchConfig.index.substractDays, 'days').format('YYYY-MM-DD HH:mm:ss')}"`;
        }

        // Pagination
        condition += ` LIMIT ${skipCount},${perIterationNumber}`;

        // Prepare query string
        const raw =
            `SELECT
                icp.id,
                icp.entity_id as entityId,
                icp.name as name,
                icp.sku as sku,
                IF(irpp.price IS NOT NULL AND irpp.price <> 0, irpp.price, icp.price) as price,
                icp.special_price as specialPrice,
                icp.old_price as oldPrice,
                icp.description as description,
                icp.thumbnail as thumbnail,
                icp.images as images,
                icp.sales_qty as salesQty,
                IF(icp.is_active <> 1, false, true) as isActive,
                IF(icp.is_visible <> 1, false, true) as isVisible,
                icp.url as url,
                icp.category_ids as categoryIds,
                ir.entity_id as regionEntityId,
                irps.qty as qty,
                icp.magento_created_at as magentoCreatedAt,
                icp.created_at as createdAt,
                icp.updated_at as updatedAt
            FROM (SELECT * FROM index_catalog_product WHERE ${condition}) AS icp
            LEFT JOIN index_region_product_price irpp ON irpp.product_id = icp.id
            LEFT JOIN index_region_product_stock irps ON irps.product_id = icp.id
            LEFT JOIN index_region ir ON ir.id = irps.region_id`;

        // Make request
        const result = await this.dbClient.raw(raw);

        const preparedItems = new Array();

        for (const item of result[0]) {
            if (!preparedItems.length || preparedItems[preparedItems.length - 1].id !== item.id) {

                const price = item.specialPrice < item.price && item.specialPrice ? item.specialPrice : item.price;
                const oldPrice = !isNull(item.oldPrice) && item.oldPrice > price ? item.oldPrice : price;
                const preparedProduct: CatalogProductFromLocalStorageType = {
                    id: item.id,
                    entityId: item.entityId,
                    name: item.name,
                    sku: item.sku,
                    price: price,
                    discount: (oldPrice - price) * 100 / oldPrice,
                    specialPrice: item.specialPrice,
                    oldPrice: item.oldPrice,
                    description: item.description,
                    thumbnail: item.thumbnail,
                    images: item.images,
                    bestseller: item.salesQty,
                    isActive: item.isActive,
                    isVisible: item.isVisible,
                    url: item.url,
                    regionData: { [item.regionEntityId]: { isInStock: item.qty > 0, qty: item.qty, price: item.price } },
                    categoryIds: item.categoryIds,
                    magentoCreatedAt: item.magentoCreatedAt,
                    createdAt: item.createdAt,
                    updatedAt: item.updatedAt
                };
                preparedItems.push(preparedProduct);
            } else {
                preparedItems[preparedItems.length - 1].regionData[item.regionEntityId] = { isInStock: item.qty > 0, qty: item.qty, price: item.price };
            }
        }

        return preparedItems;
    }

    /**
     * Get all stores active unique product ids
     */
    public async getAllStoresActiveUniqueProductIds(): Promise<number[]> {

        const response = await this.dbClient.table('index_catalog_product')
            .select(['entity_id as entityId'])
            .where('is_active', 1)
            .andWhere('is_visible', 1)
            .orderBy('entity_id', 'desc');

        // Typecast
        const preparedIds = new Array();
        for (const product of response) {
            preparedIds.push(product.entityId);
        }

        return preparedIds;
    }
}
