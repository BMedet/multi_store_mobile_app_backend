/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import PerRegionProductPriceDataType from '../../../../../../Types/LocalStorage/PerRegionProductPriceDataType';
import PerRegionProductStockDataType from '../../../../../../Types/LocalStorage/PerRegionProductStockDataType';
import ProductIdsType from '../../../../../../Types/LocalStorage/ProductIdsType';
import RegionIdsType from '../../../../../../Types/LocalStorage/RegionIdsType';
import StoreType from '../../../../../../Types/LocalStorage/StoreType';
import GetProductContentType from '../../../../../../Types/MagentoDb/GetProductContentType';
import GetProductPricesType from '../../../../../../Types/MagentoDb/GetProductPricesType';
import GetProductSalesType from '../../../../../../Types/MagentoDb/GetProductSalesType';
import IndexToLocalStorageRepositoryInterface from '../../LocalStorageIndexRepositoryInterface';


export default interface CatalogProductDbRepositoryInterface extends IndexToLocalStorageRepositoryInterface {

    /**
     * Set product prices
     */
    setProductPrices(productsWithPrices: GetProductPricesType[], store: StoreType): Promise<void>;

    /**
     * Set product sales info
     */
    setProductSalesInfo(productEntityIds: GetProductSalesType[], store: StoreType): Promise<void>;

    /**
     * Set product content
     */
    setProductContent(productsWithContent: GetProductContentType[], store: StoreType): Promise<void>;

    /**
     * Get all active product ids
     */
    getAllProductIds(updatedItemEntityIds: number[], skipCount: number, perIterationNumber: number, store: StoreType): Promise<ProductIdsType[]>;

    /**
     * Get all region ids
     */
    getAllRegionEntityIds(store: StoreType): Promise<RegionIdsType[]>;

    /**
     * Set product region stock data
     */
    setProductRegionStockData(productsWidthRegionStockData: PerRegionProductStockDataType[]): Promise<void>;

    /**
     * Set product region price data
     */
    setProductRegionPriceData(productsWidthRegionStockData: PerRegionProductPriceDataType[]): Promise<void>;

    /**
     * Get all stores active unique product ids
     */
    getAllStoresActiveUniqueProductIds(): Promise<number[]>;
}
