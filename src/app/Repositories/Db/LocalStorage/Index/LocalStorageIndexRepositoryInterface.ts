/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import CatalogCategoryFromMagentoType from '../../../../Types/MagentoApi/CatalogCategoryFromMagentoType';
import StoreType from '../../../../Types/LocalStorage/StoreType';
import RegionFromMagentoType from '../../../../Types/MagentoDb/RegionFromMagentoType';
import FirstIterationCatalogProductFromMagentoType from '../../../../Types/MagentoDb/CatalogProductFromMagentoType';
import CatalogproductFromLocalStorageType from '../../../../Types/LocalStorage/CatalogproductFromLocalStorageType';
import ProductAttributeType from '../../../../Types/MagentoDb/ProductAttributeType';

// Types
type itemsForIndexType = RegionFromMagentoType | CatalogCategoryFromMagentoType | FirstIterationCatalogProductFromMagentoType | ProductAttributeType;
export default interface LocalStorageIndexRepositoryInterface {

    /**
     * Index catalog categories
     */
    index(items: itemsForIndexType[], store: StoreType): Promise<number[]>;

    /**
     * Disable inactive category ids
     */
    disableInactiveIndexes(updatedCategoryIds: number[], store: StoreType): Promise<void>;

    /**
     * Get updated catalog categories
     */
    getUpdatedInLastDateIndexes(skipCount: number, perIterationNumber: number, isForce:boolean, store: StoreType): Promise<CatalogCategoryFromMagentoType[] | RegionFromMagentoType[] | CatalogproductFromLocalStorageType[] | ProductAttributeType[]>;
}
