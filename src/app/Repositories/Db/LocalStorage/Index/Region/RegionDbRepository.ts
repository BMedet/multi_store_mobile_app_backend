/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import CatalogCategoryFromMagentoType from '../../../../../Types/MagentoApi/CatalogCategoryFromMagentoType';
import StoreType from '../../../../../Types/LocalStorage/StoreType';
import { isNull } from 'lodash';
import dayjs from 'dayjs';
import elasticsearchConfig from '../../../../../../config/elasticsearch';
import RegionFromMagentoType from '../../../../../Types/MagentoDb/RegionFromMagentoType';
import RegionDbRepositoryInterface from './RegionDbRepositoryInterface';
import AbstractLocalStorageRespository from '../../AbstractLocalStorageRespository';

export default class RegionDbRepository extends AbstractLocalStorageRespository implements RegionDbRepositoryInterface {

    /**
     * Index regions
     */
    public async index(regions: RegionFromMagentoType[], store: StoreType): Promise<number[]> {

        const ids = new Array();

        // Init query string
        let values = '';
        const regionsCount = regions.length;
        let i = 0;

        // Prepare query string
        for (const region of regions) {
            ids.push(region.entityId);

            i++;
            values += `(${region.entityId},
                "${this.mysqlEscape(region.name)}",
                "${this.mysqlEscape(region.code)}",
                ${region.isActive},
                ${region.isVisible},
                ${!isNull(region.declensionName) ? '"' + region.declensionName + '"' : null},
                ${!isNull(region.polygon) ? '"' + region.polygon + '"' : null},
                ${store.entityId},
                ${region.position})`;
            if (i === regionsCount) break;

            values += ', ';
        }

        const raw = `INSERT INTO index_region (entity_id, name, code, is_active, is_visible, declension_name, polygon, store_entity_id, position)
                    VALUES ${values}
                    ON DUPLICATE KEY UPDATE
                        name=VALUES(name),
                        code=VALUES(code),
                        is_active=VALUES(is_active),
                        is_visible=VALUES(is_visible),
                        declension_name=VALUES(declension_name),
                        polygon=VALUES(polygon),
                        position=VALUES(position),
                        position=VALUES(position)`;

        await this.dbClient.raw(raw);

        return ids;
    }

    /**
     * Disable inactive regions
     */
    public async disableInactiveIndexes(updatedRegionIds: number[], store: StoreType): Promise<void> {
        await this.dbClient.table('index_region')
            .where('store_entity_id', store.entityId)
            .whereNotIn('entity_id', updatedRegionIds)
            .update('is_active', false);
    }

    /**
     * Get updated catalog categories
     */
    public async getUpdatedInLastDateIndexes(skipCount: number, perIterationNumber: number, isForce: boolean, store: StoreType): Promise<RegionFromMagentoType[]> {
        const categories = await this.dbClient.table('index_region')
            .select([
                'id',
                'entity_id as entityId',
                'name',
                'is_active as isActive',
                'is_visible as isVisible',
                'code as code',
                'position as position',
                'declension_name as declensionName',
                'polygon as polygon',
                'created_at as createdAt',
                'updated_at as updatedAt',
            ])
            .where('store_entity_id', store.entityId)
            .modify(function(queryBuilder) {
                if (isForce) {
                    queryBuilder.andWhere('created_at', '>', dayjs().subtract(elasticsearchConfig.index.substractDays, 'days').format('YYYY-MM-DD HH:mm:ss'));
                }
            })
            .offset(skipCount)
            .limit(perIterationNumber);

        return categories;

    }
}
