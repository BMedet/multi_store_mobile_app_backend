/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import IndexToLocalStorageRepositoryInterface from "../LocalStorageIndexRepositoryInterface";

export default interface RegionDbRepositoryInterface extends IndexToLocalStorageRepositoryInterface {
}
