/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import PerRegionProductPriceDataType from "../../../../../Types/LocalStorage/PerRegionProductPriceDataType";
import PerRegionProductStockDataType from "../../../../../Types/LocalStorage/PerRegionProductStockDataType";
import ProductIdsType from "../../../../../Types/LocalStorage/ProductIdsType";
import RegionIdsType from "../../../../../Types/LocalStorage/RegionIdsType";
import StoreType from "../../../../../Types/LocalStorage/StoreType";
import GetProductContentType from "../../../../../Types/MagentoDb/GetProductContentType";
import GetProductPricesType from "../../../../../Types/MagentoDb/GetProductPricesType";
import GetProductSalesType from "../../../../../Types/MagentoDb/GetProductSalesType";
import MagentoDbRepositoryInterface from "../../MagentoDbRepositoryInterface";

export default interface CatalogProductMagentoDbRepositoryInterface extends MagentoDbRepositoryInterface {

    /**
     * Get product prices
     */
    getProductPrices(productIds: number[], store: StoreType): Promise<GetProductPricesType[]>;

    /**
     * Get sales info
     */
    getSalesInfo(productIds: number[], store: StoreType): Promise<GetProductSalesType[]>;

    /**
     * Get product content
     */
    getProductContent(productIds: number[], store: StoreType): Promise<GetProductContentType[]>;

    /**
     * Get region stockIds
     */
    getRegionStockIds(regions: RegionIdsType[]): Promise<RegionIdsType[]>;

    /**
     * Get region stock data
     */
    getRegionStockData(products: ProductIdsType[], regions: RegionIdsType[], store: StoreType): Promise<PerRegionProductStockDataType[]>;

    /**
     * Get region price data
     */
    getRegionPriceData(products: ProductIdsType[], regions: RegionIdsType[], store: StoreType): Promise<PerRegionProductPriceDataType[]>;
}
