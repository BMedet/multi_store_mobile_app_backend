/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import AbstractMagentoRepository from '../../AbstractMagentoRepository';
import StoreType from '../../../../../Types/LocalStorage/StoreType';
import CatalogProductMagentoDbRepositoryInterface from './CatalogProductMagentoDbRepositoryInterface';
import CatalogProductFromMagentoType from '../../../../../Types/MagentoDb/CatalogProductFromMagentoType';
import GetProductPricesType from '../../../../../Types/MagentoDb/GetProductPricesType';
import GetProductContentType from '../../../../../Types/MagentoDb/GetProductContentType';
import dayjs from 'dayjs';
import lodash from 'lodash';
import elasticsearchConfig from '../../../../../../config/elasticsearch';
import ProductIdsType from '../../../../../Types/LocalStorage/ProductIdsType';
import RegionIdsType from '../../../../../Types/LocalStorage/RegionIdsType';
import PerRegionProductStockDataType from '../../../../../Types/LocalStorage/PerRegionProductStockDataType';
import PerRegionProductPriceDataType from '../../../../../Types/LocalStorage/PerRegionProductPriceDataType';
import GetProductSalesType from '../../../../../Types/MagentoDb/GetProductSalesType';
import Helper from 'sosise-core/build/Helper/Helper';

export default class CatalogProductMagentoDbRepository extends AbstractMagentoRepository implements CatalogProductMagentoDbRepositoryInterface {

    /**
     * Get all active products
     */
    public async getAllActiveItems(isForce: boolean, store: StoreType): Promise<CatalogProductFromMagentoType[]> {
        // Prepare sql query string
        let sqlQuery = `
            SELECT e.entity_id as entityId ,
            e.sku as sku,
            e.created_at as magentoCreatedAt,
            IF(at_visibility.value_id > 0, at_visibility.value, at_visibility_default.value) AS isVisible,
            IF(at_status.value_id > 0, at_status.value, at_status_default.value) AS isActive,
            at_qty.qty,
            IF(name.value is NOT NULL,name.value,name_default.value) AS name,
            IF(thumbnail.value is NOT NULL,thumbnail.value,thumbnail_default.value) AS thumbnail,
            GROUP_CONCAT(DISTINCT(ccp.category_id) SEPARATOR ', ') as categoryIds
            FROM catalog_product_entity AS e
            LEFT JOIN catalog_product_website AS product_website ON product_website.product_id = e.entity_id AND product_website.website_id = 1
            LEFT JOIN catalog_category_product AS ccp ON (ccp.product_id = e.entity_id)
            LEFT JOIN catalog_product_entity_varchar AS thumbnail ON (thumbnail.entity_id = e.entity_id) AND (thumbnail.attribute_id = '89') AND thumbnail.store_id = ${store.entityId}
            LEFT JOIN catalog_product_entity_varchar AS thumbnail_default ON (thumbnail_default.entity_id = e.entity_id) AND (thumbnail_default.attribute_id = '89') AND thumbnail_default.store_id = 0
            LEFT JOIN catalog_product_entity_varchar AS name ON (name.entity_id = e.entity_id) AND (name.attribute_id = '73') AND name.store_id = ${store.entityId}
            LEFT JOIN catalog_product_entity_varchar AS name_default ON (name_default.entity_id = e.entity_id) AND (name_default.attribute_id = '73') AND name_default.store_id = 0
            LEFT JOIN catalog_product_entity_int AS at_visibility_default ON (at_visibility_default.entity_id = e.entity_id) AND (at_visibility_default.attribute_id = '99') AND at_visibility_default.store_id = 0
            LEFT JOIN catalog_product_entity_int AS at_visibility ON (at_visibility.entity_id = e.entity_id) AND (at_visibility.attribute_id = '99') AND (at_visibility.store_id = ${store.entityId})
            LEFT JOIN catalog_category_entity_int AS category_enabled ON (category_enabled.entity_id = ccp.category_id) AND (category_enabled.attribute_id = '46') AND category_enabled.store_id = ${store.entityId}
            LEFT JOIN catalog_category_entity_int AS category_enabled_default ON (category_enabled_default.entity_id = ccp.category_id) AND (category_enabled_default.attribute_id = '46') AND (category_enabled_default.store_id = 0)
            LEFT JOIN catalog_product_entity_int AS at_status_default ON (at_status_default.entity_id = e.entity_id) AND (at_status_default.attribute_id = '97') AND at_status_default.store_id = 0
            LEFT JOIN catalog_product_entity_int AS at_status ON (at_status.entity_id = e.entity_id) AND (at_status.attribute_id = '97') AND (at_status.store_id = ${store.entityId})
            LEFT JOIN cataloginventory_stock_item AS at_qty ON (at_qty.product_id=e.entity_id) AND (at_qty.stock_id=1)
            WHERE (IF(at_visibility.value_id > 0, at_visibility.value, at_visibility_default.value) = '4') AND (IF(category_enabled.value_id > 0, category_enabled.value, category_enabled_default.value) = '1') AND (IF(at_status.value_id > 0, at_status.value, at_status_default.value) = '1') AND (at_qty.qty >= 1) AND (e.type_id = 'simple') `;

        if (isForce) {
            sqlQuery += `AND updated_at > "${dayjs().subtract(elasticsearchConfig.index.substractDays, 'days').format('YYYY-MM-DD HH:mm:ss')}" `;
        }

        sqlQuery += `GROUP BY e.entity_id ORDER BY e.created_at DESC`;

        const products = await this.dbClient.raw(sqlQuery);

        return products[0];
    }

    /**
     * Get sales info
     */
    public async getSalesInfo(productIds: number[], store: StoreType): Promise<GetProductSalesType[]> {
        // Init values
        let values = '';
        const productsCount = productIds.length;
        let i = 0;

        // Convert to string
        for (const productId of productIds) {
            i++;
            values += productId;
            if (i === productsCount) break;
            values += ', ';
        }

        // Prepare query string
        const sqlQuery = `
        SELECT
            soi.product_id AS entityId,
            SUM(soi.qty_ordered) AS qty
            FROM sales_order_item AS soi
            WHERE soi.product_id IN (${values}) GROUP BY soi.product_id`;

        // Make request
        const response = await this.dbClient.raw(sqlQuery);

        return response[0];
    }

    /**
     * Get product prices
     */
    public async getProductPrices(productIds: number[], store: StoreType): Promise<GetProductPricesType[]> {
        // Init values
        let values = '';
        const productsCount = productIds.length;
        let i = 0;

        // Convert to string
        for (const productId of productIds) {
            i++;
            values += productId;
            if (i === productsCount) break;
            values += ', ';
        }

        // Prepare sql query
        const sqlQuery = `
            SELECT e.entity_id as entityId,
            IF(old_price.value is NOT NULL,old_price.value,old_price_default.value) AS oldPrice,
            IF(spec_price.value is NOT NULL,spec_price.value,spec_price_default.value) AS specialPrice,
            IF(price.value is NOT NULL,price.value,price_default.value) AS price
            FROM catalog_product_entity AS e
            # old price
            LEFT JOIN catalog_product_entity_decimal AS old_price ON (old_price.entity_id = e.entity_id) AND (old_price.attribute_id = '1952') AND old_price.store_id = ${store.entityId}
            LEFT JOIN catalog_product_entity_decimal AS old_price_default ON (old_price_default.entity_id = e.entity_id) AND (old_price_default.attribute_id = '1952') AND old_price_default.store_id = 0
            # special price
            LEFT JOIN catalog_product_entity_decimal AS spec_price ON (spec_price.entity_id = e.entity_id) AND (spec_price.attribute_id = '78') AND spec_price.store_id = ${store.entityId}
            LEFT JOIN catalog_product_entity_decimal AS spec_price_default ON (spec_price_default.entity_id = e.entity_id) AND (spec_price_default.attribute_id = '78') AND spec_price_default.store_id = 0
            # price
            LEFT JOIN catalog_product_entity_decimal AS price ON (price.entity_id = e.entity_id) AND (price.attribute_id = '77') AND price.store_id = ${store.entityId}
            LEFT JOIN catalog_product_entity_decimal AS price_default ON (price_default.entity_id = e.entity_id) AND (price_default.attribute_id = '77') AND price_default.store_id = 0
            WHERE  e.entity_id IN (${values}) GROUP BY e.entity_id
        `;

        const response = await this.dbClient.raw(sqlQuery);

        return response[0];
    }

    /**
     * Get product content
     */
    public async getProductContent(productIds: number[], store: StoreType): Promise<GetProductContentType[]> {
        // Init values
        let values = '';
        const productsCount = productIds.length;
        let i = 0;

        // Convert to string
        for (const productId of productIds) {
            i++;
            values += productId;
            if (i === productsCount) break;
            values += ', ';
        }

        // Prepare query string
        const sqlQuery = `
        SELECT e.entity_id as entityId,
            IF(description.value is NOT NULL,description.value,description_default.value) AS description,
            IF(url_key.value is NOT NULL,url_key.value,url_key_default.value) AS url,
            GROUP_CONCAT(DISTINCT(IF(cpemg.disabled<>0,null,cpemg.value)) SEPARATOR ', ') as images
            FROM catalog_product_entity AS e
            # images
            LEFT JOIN catalog_product_entity_media_gallery_value AS cpemgv ON cpemgv.entity_id = e.entity_id
            LEFT JOIN catalog_product_entity_media_gallery AS cpemg ON cpemg.value_id = cpemgv.value_id AND cpemg.media_type='image'
            # url_key
            LEFT JOIN catalog_product_entity_varchar AS url_key ON (url_key.entity_id = e.entity_id) AND (url_key.attribute_id = '119') AND url_key.store_id = ${store.entityId}
            LEFT JOIN catalog_product_entity_varchar AS url_key_default ON (url_key_default.entity_id = e.entity_id) AND (url_key_default.attribute_id = '119') AND url_key_default.store_id = 0
            # description
            LEFT JOIN catalog_product_entity_text AS description ON (description.entity_id = e.entity_id) AND (description.attribute_id = '75') AND description.store_id = ${store.entityId}
            LEFT JOIN catalog_product_entity_text AS description_default ON (description_default.entity_id = e.entity_id) AND (description_default.attribute_id = '75') AND description_default.store_id = 0
            WHERE e.entity_id IN (${values}) GROUP BY e.entity_id`;

        // Make request
        const response = await this.dbClient.raw(sqlQuery);

        return response[0];
    }

    /**
     * Get region Ids
     */
    public async getRegionStockIds(regions: RegionIdsType[]): Promise<RegionIdsType[]> {
        for (const region of regions) {
            const query = `
            SELECT GROUP_CONCAT(stock_id) as stockIds FROM cataloginventory_stock cs where region_id = ${region.entityId} GROUP BY region_id`;
            // Make request
            const response = await this.dbClient.raw(query);
            region.stockIds = lodash.get(response[0], '[0].stockIds', null);
        }
        return regions;
    }

    /**
     * Get region stock data
     */
    public async getRegionStockData(products: ProductIdsType[], regions: RegionIdsType[], store: StoreType): Promise<PerRegionProductStockDataType[]> {

        const entityIdKeyArray = {};
        // Init values
        let productEntityidsString = '';
        const productsCount = products.length;
        let i = 0;

        // Convert to string
        for (const product of products) {
            i++;
            productEntityidsString += product.entityId;
            entityIdKeyArray[(product.entityId)] = product.id;
            if (i === productsCount) break;
            productEntityidsString += ', ';
        }

        let preparedResult = new Array();
        for (const region of regions) {
            preparedResult = preparedResult.concat(await this.getPerRegionStockData(region, entityIdKeyArray, productEntityidsString));
        }

        return preparedResult;
    }

    /**
     * Get region price data
     */
    public async getRegionPriceData(products: ProductIdsType[], regions: RegionIdsType[], store: StoreType): Promise<PerRegionProductPriceDataType[]> {

        const entityIdKeyArray = {};
        // Init values
        let productEntityIdsString = '';
        const productsCount = products.length;
        let i = 0;

        // Convert to string
        for (const product of products) {
            i++;
            productEntityIdsString += product.entityId;
            entityIdKeyArray[(product.entityId)] = {id: product.id, price: product.price };
            if (i === productsCount) break;
            productEntityIdsString += ', ';
        }


        let preparedResult = new Array();
        for (const region of regions) {
            preparedResult = preparedResult.concat(await this.getPerRegionPriceData(region, entityIdKeyArray, productEntityIdsString));
        }

        return preparedResult;
    }

    /**
     * Get per region stock data
     */
    private async getPerRegionStockData(region: RegionIdsType, entityIdKeyArray: { [key: string]: number }, productEntityidsString: string): Promise<PerRegionProductStockDataType[]> {
        const sqlQuery = `
            SELECT
                arsi.product_id as productId,
                SUM(arsi.qty) as qty
                FROM astrio_region_stock_item arsi
                where arsi.product_id in (${productEntityidsString}) and arsi.stock_id IN (${region.stockIds})
                GROUP BY product_id`;
        // Make request
        const response = await this.dbClient.raw(sqlQuery);

        const preparedResult = new Array();
        for (const perRegionResponse of response[0]) {
            preparedResult.push({
                productEntityId: perRegionResponse.productId,
                qty: perRegionResponse.qty,
                productId: entityIdKeyArray[perRegionResponse.productId],
                regionEntityId: region.entityId,
                regionId: region.id
            });
        }

        return preparedResult;
    }

    /**
     * Get per region price data
     */
    private async getPerRegionPriceData(region: RegionIdsType, entityIdKeyArray: { [key: string]: {id: number, price: number} }, productEntityIdsString: string): Promise<PerRegionProductPriceDataType[]> {

        const sqlQuery = `
            SELECT
                aaprp.product_id as productId,
                aaprp.price
                FROM astrio_axapta_product_region_price aaprp
                WHERE aaprp.product_id in (${productEntityIdsString}) and aaprp.region_id IN (${region.entityId})`;

        // Make request
        const response = await this.dbClient.raw(sqlQuery);

        const foundProductIds = new Array();

        const preparedResult = new Array();
        for (const perRegionResponse of response[0]) {
            preparedResult.push({
                productEntityId: perRegionResponse.productId,
                price: perRegionResponse.price,
                productId: entityIdKeyArray[perRegionResponse.productId].id,
                regionEntityId: region.entityId,
                regionId: region.id
            });
            foundProductIds.push(perRegionResponse.productId)
        }

        // Exclude products push
        const notFoundProducts = lodash.omit(entityIdKeyArray, foundProductIds);
        for(const [productEntityId, localData] of Object.entries(notFoundProducts)) {
            const anyLocalData = localData as any;
            preparedResult.push({
                productEntityId: productEntityId,
                price: anyLocalData.price,
                productId: anyLocalData.id,
                regionEntityId: region.entityId,
                regionId: region.id
            });
        }

        return preparedResult;
    }
}
