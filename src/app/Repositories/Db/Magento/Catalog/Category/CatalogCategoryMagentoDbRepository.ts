/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import dayjs from 'dayjs';
import elasticsearchConfig from '../../../../../../config/elasticsearch';
import StoreType from '../../../../../Types/LocalStorage/StoreType';
import CatalogCategoryFromMagentoType from '../../../../../Types/MagentoApi/CatalogCategoryFromMagentoType';
import AbstractMagentoRepository from '../../AbstractMagentoRepository';
import CatalogCategoryMagentoDbRepositoryInterface from './CatalogCategoryMagentoDbRepositoryInterface';

export default class CatalogCategoryMagentoDbRepository extends AbstractMagentoRepository implements CatalogCategoryMagentoDbRepositoryInterface {
    /**
     * Get all active products
     */
    public async getAllActiveItems(isForce: boolean, store: StoreType): Promise<CatalogCategoryFromMagentoType[]> {
        // Prepare sql query string
        let sqlQuery = `
        SELECT 
            cce.entity_id as entityId,
            name.value as name,
            IF(name_in_menu.value is NOT NULL,name_in_menu.value,name_in_menu_default.value) as nameInMenu,
            IF(url_path.value is NOT NULL,url_path.value,url_path_default.value) as urlPath,
            cce.level,
            IF(thumbnail.value is NOT NULL,thumbnail.value,thumbnail_default.value) as thumbnailImage,
            IF(show_thumbnail.value is NOT NULL,show_thumbnail.value,show_thumbnail_default.value) as showThumbnail,
            cce.parent_id as parentId,
            IF(position.value is NOT NULL,position.value,position_default.value) as position,
            IF(is_active.value is NOT NULL,is_active.value,is_active_default.value) as isActive,
            IF(is_hidden.value is NOT NULL,is_hidden.value,is_hidden_default.value) as isHidden,
            IF(include_in_menu.value is NOT NULL,include_in_menu.value,include_in_menu_default.value) as includeInMenu,
            IF(filterable_attributes .value is NOT NULL,filterable_attributes.value,filterable_attributes_default .value) as filterableAttributes,
            cce.created_at as magentoCreatedAt,
            cce.updated_at as magentoUpdatedAt
        FROM catalog_category_entity cce 
        LEFT JOIN catalog_category_entity_varchar as name ON (name.entity_id = cce.entity_id) AND (name.attribute_id = 45)
        LEFT JOIN catalog_category_entity_varchar as name_in_menu ON (name_in_menu.entity_id = cce.entity_id) AND (name_in_menu.attribute_id = 179) AND name_in_menu.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_varchar as name_in_menu_default ON (name_in_menu_default.entity_id = cce.entity_id) AND (name_in_menu_default.attribute_id = 179) AND name_in_menu_default.store_id =0
        LEFT JOIN catalog_category_entity_varchar as url_path ON (url_path.entity_id = cce.entity_id) AND (url_path.attribute_id = 118) AND url_path.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_varchar as url_path_default ON (url_path_default.entity_id = cce.entity_id) AND (url_path_default.attribute_id = 118) AND url_path_default.store_id =0
        LEFT JOIN catalog_category_entity_varchar as thumbnail ON (thumbnail.entity_id = cce.entity_id) AND (thumbnail.attribute_id = 155) AND url_path.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_varchar as thumbnail_default ON (thumbnail_default.entity_id = cce.entity_id) AND (thumbnail_default.attribute_id = 155) AND url_path_default.store_id =0
        LEFT JOIN catalog_category_entity_text as filterable_attributes ON (filterable_attributes.entity_id = cce.entity_id) AND (filterable_attributes.attribute_id = 968) AND filterable_attributes.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_text as filterable_attributes_default ON (filterable_attributes_default.entity_id = cce.entity_id) AND (filterable_attributes_default.attribute_id = 968) AND filterable_attributes_default.store_id = 0
        LEFT JOIN catalog_category_entity_int as is_active ON (is_active.entity_id = cce.entity_id) AND (is_active.attribute_id = 46) AND is_active.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_int as is_active_default ON (is_active_default.entity_id = cce.entity_id) AND (is_active_default.attribute_id = 46) AND is_active_default.store_id =0
        LEFT JOIN catalog_category_entity_int as is_hidden ON (is_hidden.entity_id = cce.entity_id) AND (is_hidden.attribute_id = 2809) AND is_hidden.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_int as is_hidden_default ON (is_hidden_default.entity_id = cce.entity_id) AND (is_hidden_default.attribute_id = 2809) AND is_hidden_default.store_id =0
        LEFT JOIN catalog_category_entity_int as include_in_menu ON (include_in_menu.entity_id = cce.entity_id) AND (include_in_menu.attribute_id = 69) AND include_in_menu.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_int as include_in_menu_default ON (include_in_menu_default.entity_id = cce.entity_id) AND (include_in_menu_default.attribute_id = 69) AND include_in_menu_default.store_id =0
        LEFT JOIN catalog_category_entity_int as show_thumbnail ON (show_thumbnail.entity_id = cce.entity_id) AND (show_thumbnail.attribute_id = 156) AND show_thumbnail.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_int as show_thumbnail_default ON (show_thumbnail_default.entity_id = cce.entity_id) AND (show_thumbnail_default.attribute_id = 156) AND show_thumbnail_default.store_id =0
        LEFT JOIN catalog_category_entity_int as position ON (position.entity_id = cce.entity_id) AND (position.attribute_id = 178) AND position.store_id = ${store.entityId}
        LEFT JOIN catalog_category_entity_int as position_default ON (position_default.entity_id = cce.entity_id) AND (position_default.attribute_id = 178) AND position_default.store_id =0
        `;

        if (isForce) {
            sqlQuery += `WHERE cce.updated_at > "${dayjs().subtract(elasticsearchConfig.index.substractDays, 'days').format('YYYY-MM-DD HH:mm:ss')}" `;
        }
        sqlQuery += `GROUP BY cce.entity_id`;

        const categories = await this.dbClient.raw(sqlQuery);

        // Typecast
        const preparedCategories = new Array();
        for (const category of categories[0]) {
            const preparedCategory: CatalogCategoryFromMagentoType = {
                entityId: category.entityId,
                name: category.name,
                urlPath: category.urlPath,
                thumbnailImage: category.thumbnailImage,
                showThumbnail: category.showThumbnail === 1,
                parentId: category.parentId,
                position: category.position,
                isActive: category.isActive === 1,
                isHidden: category.isHidden === 1,
                level: category.level,
                nameInMenu: category.nameInMenu,
                includeInMenu: category.includeInMenu === 1,
                filterableAttributes: category.filterableAttributes,
                magentoCreatedAt: dayjs(category.magentoCreatedAt).format('YYYY-MM-DD HH:mm:ss'),
                magentoUpdatedAt: dayjs(category.magentoUpdatedAt).format('YYYY-MM-DD HH:mm:ss')
            }
            preparedCategories.push(preparedCategory);
        }

        return preparedCategories;
    }

}
