/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import Helper from 'sosise-core/build/Helper/Helper';
import CatalogAttributeBackendTypeEnum from '../../../../../Enums/CatalogAttributeBackendTypeEnum';
import GetAttributeValueType from '../../../../../Types/MagentoDb/GetAttributeValueType';
import ProductAttributeOptionType from '../../../../../Types/MagentoDb/ProductAttributeOptionType';
import ProductAttributeType from '../../../../../Types/MagentoDb/ProductAttributeType';
import AbstractMagentoRepository from '../../AbstractMagentoRepository';
import CatalogProductMagentoDbRepositoryInterface from './CatalogAttributesMagentoDbRepositoryInterface';

export default class CatalogAttributesMagentoDbRepository extends AbstractMagentoRepository implements CatalogProductMagentoDbRepositoryInterface {
    /**
     * Get all attributes
     */
    public async getAllActiveAttributes(): Promise<ProductAttributeType[]> {
        // Prepare query string
        const queryString = `
            SELECT
                ea.attribute_id as attributeId,
                ea.attribute_code as attributeCode,
                ea.frontend_label as frontendLabel,
                ea.frontend_input as frontendInput,
                cea.position as position,
                cea.is_visible_on_front as isVisibleOnFront,
                cea.is_visible_in_grid as isVisibleOnGrid,
                cea.is_filterable as isFilterable,
                ea.backend_type as backendType,
                aafs.is_multiselect as isMultiselect
            FROM catalog_eav_attribute cea
            LEFT JOIN eav_attribute ea ON cea.attribute_id = ea.attribute_id
            LEFT JOIN amasty_amshopby_filter_setting aafs ON aafs.filter_code = CONCAT('attr_', ea.attribute_code)
            WHERE cea.is_visible = 1 AND cea.is_visible_on_front =1 AND ea.entity_type_id = 4
        `;

        // Make request
        const response = await this.dbClient.raw(queryString);

        return response[0];
    }
    /**
     * Get attribute options
     */
    public async getAttributeOptions(attributeIds: number[]): Promise<ProductAttributeOptionType[]> {
        // Prepare query string
        const queryString = `
        SELECT
            eao.option_id as optionId,
            eao.attribute_id as attributeId,
            eaov.value_id as valueId,
            eaov.value as value,
            eao.sort_order as sortOrder
        FROM eav_attribute_option eao
        LEFT JOIN eav_attribute_option_value eaov ON eaov.option_id = eao.option_id
        WHERE eao.attribute_id IN (${attributeIds.toString()}) AND eaov.value_id IS NOT NULL
        `;

        // Make request
        const response = await this.dbClient.raw(queryString);

        return response[0];
    }


    /**
     * Get attribute values
     */
    public async getAttributeValues(productIds: number[], attributeIds: number[], backendType: CatalogAttributeBackendTypeEnum): Promise<GetAttributeValueType[]> {
        return await this.dbClient.table(`catalog_product_entity_${backendType}`)
        .select([
            'entity_id as entityId',
            'attribute_id as attributeId',
            'value as attributeValue'
        ])
        .whereIn('attribute_id', attributeIds)
        .whereIn('entity_id', productIds);
    }
}
