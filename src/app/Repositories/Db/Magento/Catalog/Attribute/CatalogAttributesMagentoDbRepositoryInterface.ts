/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import CatalogAttributeBackendTypeEnum from "../../../../../Enums/CatalogAttributeBackendTypeEnum";
import GetAttributeValueType from "../../../../../Types/MagentoDb/GetAttributeValueType";
import ProductAttributeOptionType from "../../../../../Types/MagentoDb/ProductAttributeOptionType";
import ProductAttributeType from "../../../../../Types/MagentoDb/ProductAttributeType";

export default interface CatalogAttributesMagentoDbRepositoryInterface {

    /**
     * Get all attributes
     */
    getAllActiveAttributes(): Promise<ProductAttributeType[]>;

    /**
     * Get attribute options
     */
    getAttributeOptions(attributeIds: number[]): Promise<ProductAttributeOptionType[]>;

    /**
     * Get attribute values
     */
    getAttributeValues(productIds: number[], attributeIds: number[], backendType: CatalogAttributeBackendTypeEnum): Promise<GetAttributeValueType[]>;

}
