/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import MagentoRepositoryInterface from './RegionMagentoDbRepositoryInterface';
import AbstractMagentoRepository from '../AbstractMagentoRepository';
import StoreType from '../../../../Types/LocalStorage/StoreType';
import RegionFromMagentoType from '../../../../Types/MagentoDb/RegionFromMagentoType';

export default class RegionMagentoDbRepository extends AbstractMagentoRepository implements MagentoRepositoryInterface {

    /**
     * Get all customers
     */
    public async getAllActiveItems(isForce: boolean, store: StoreType): Promise<RegionFromMagentoType[]> {
        // Prepare sql query
        const sqlQuery = `
                SELECT ar.region_id as entityId,
                ar.name as name,
                ar.status as isActive,
                ar.code as code,
                ar.declension_name as declensionName,
                ar.region_polygon as polygon,
                arv.visible as visibleInStore,
                IF(ars.position is NOT NULL,ars.position,ar.position) AS position,
                IF(arv.visible is NOT NULL,arv.visible,ar.visible) AS isVisible
        FROM astrio_region AS ar
        LEFT JOIN astrio_region_store AS ars ON ar.region_id=ars.region_id and store_id=${store.entityId}
        LEFT JOIN astrio_region_visible AS arv ON ar.region_id=arv.region_id and arv.store_id=${store.entityId}
        WHERE (status = '1') AND (IF(arv.visible is NOT NULL,arv.visible,ar.visible) = 1) ORDER BY position ASC, name ASC`;

        const regions = await this.dbClient.raw(sqlQuery);

        return regions[0];
    }
}
