/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import MagentoDbRepositoryInterface from "../MagentoDbRepositoryInterface";

export default interface RegionMagentoDbRepositoryInterface extends MagentoDbRepositoryInterface {
// Init
}
