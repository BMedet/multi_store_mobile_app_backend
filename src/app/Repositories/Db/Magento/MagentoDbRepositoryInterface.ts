/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import StoreType from "../../../Types/LocalStorage/StoreType";
import CatalogCategoryFromMagentoType from "../../../Types/MagentoApi/CatalogCategoryFromMagentoType";
import FirstIterationCatalogProductFromMagentoType from "../../../Types/MagentoDb/CatalogProductFromMagentoType";
import ProductAttributeType from "../../../Types/MagentoDb/ProductAttributeType";
import RegionFromMagentoType from "../../../Types/MagentoDb/RegionFromMagentoType";


export default interface MagentoDbRepositoryInterface {

    /**
     * Get all active products
     */
     getAllActiveItems(isForce: boolean, store: StoreType): Promise<FirstIterationCatalogProductFromMagentoType[] | RegionFromMagentoType[] | CatalogCategoryFromMagentoType[] | ProductAttributeType[]>;

}
