/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import Database from 'sosise-core/build/Database/Database';
import { Knex } from 'knex';

export default abstract class AbstractMagentoRepository {

    protected dbClient: Knex;

    /**
     * Constructor
     */
    constructor() {
        this.dbClient = Database.getConnection(process.env.MAGENTO_CONNECTION as string).client;
    }

}
