/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import commander from 'commander';
import BaseCommand, { OptionType } from 'sosise-core/build/Command/BaseCommand';
import Helper from 'sosise-core/build/Helper/Helper';
import IOC from 'sosise-core/build/ServiceProviders/IOC';
import IndexEndpointEnum from '../../Enums/IndexEndpointEnum';
import CatalogProductIndexToDbService from '../../Services/Index/Db/Catalog/CatalogProductIndexToDbService';
import CatalogProductIndexToElasticService from '../../Services/Index/Elastic/Catalog/CatalogProductIndexToElasticService';

export default class IndexCatalogProductCommand extends BaseCommand {
    /**
     * Command name
     */
    protected signature: string = 'index:catalog_product';

    /**
     * Command description
     */
    protected description: string = 'Index catalog product';

    /**
     * When command is executed prevent from double execution
     */
    protected singleExecution: boolean = false;

    /**
     * Command options
     */
    protected options: OptionType[] = [
        // Options can pass some values
        { flag: '-e, --endpoint <endpoint>', description: 'Index endpoint [IndexEndpointEnum]', default: 'all', required: false },
        { flag: '-s, --store <store>', description: 'Index store [StoreCodeEnum]', default: 'all', required: false },
        { flag: '-t, --type <type>', description: 'Index type (force | full)', default: 'force', required: false },
    ];

    /**
     * Execute the console command
     */
    public async handle(cli: commander.Command): Promise<void> {

        // Init index to DB service
        const indexToDbService = IOC.make(CatalogProductIndexToDbService) as CatalogProductIndexToDbService;

        // Init index to ELASTIC service
        const indexToElasticService = IOC.make(CatalogProductIndexToElasticService) as CatalogProductIndexToElasticService;

        // Is force
        const isForceIndex = cli.type === 'force';

        switch (cli.endpoint) {
            // Index to db
            case IndexEndpointEnum.db:
                await indexToDbService.index(cli.store, isForceIndex);
                break;

            // Index to elastic
            case IndexEndpointEnum.elastic:
                await indexToElasticService.index(cli.store, isForceIndex);
                break;

            // Index to db & elastic
            case IndexEndpointEnum.all:
                await indexToDbService.index(cli.store, isForceIndex);
                await indexToElasticService.index(cli.storeisForceIndex);
                break;
        }
    }
}
