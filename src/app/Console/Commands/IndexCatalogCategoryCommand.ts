/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import commander from 'commander';
import BaseCommand, { OptionType } from 'sosise-core/build/Command/BaseCommand';
import Helper from 'sosise-core/build/Helper/Helper';
import IOC from 'sosise-core/build/ServiceProviders/IOC';
import IndexEndpointEnum from '../../Enums/IndexEndpointEnum';
import CatalogCategoryIndexToDbService from '../../Services/Index/Db/Catalog/CatalogCategoryIndexToDbService';
import CatalogCategoryIndexToElasticService from '../../Services/Index/Elastic/Catalog/CatalogCategoryIndexToElasticService';

export default class IndexCatalogCategoryCommand extends BaseCommand {
    /**
     * Command name
     */
    protected signature: string = 'index:catalog_category';

    /**
     * Command description
     */
    protected description: string = 'Index catalog category';

    /**
     * When command is executed prevent from double execution
     */
    protected singleExecution: boolean = false;

    /**
     * Command options
     */
    protected options: OptionType[] = [
        // Options can pass some values
        { flag: '-e, --endpoint <endpoint>', description: 'Index endpoint [IndexEndpointEnum]', default: 'all', required: false },
        { flag: '-s, --store <store>', description: 'Index store [StoreCodeEnum]', default: 'all', required: false },
        { flag: '-t, --type <type>', description: 'Index type (force | full)', default: 'force', required: false },
    ];

    /**
     * Execute the console command
     */
    public async handle(cli: commander.Command): Promise<void> {

        // Init index to DB service
        const catalogCategoryIndexToDbService = IOC.make('CatalogCategoryIndexToDbService') as CatalogCategoryIndexToDbService;

        // Init index to ELASTIC service
        const catalogCategoryIndexToElasticService = IOC.make('CatalogCategoryIndexToElasticService') as CatalogCategoryIndexToElasticService;

        switch (cli.endpoint) {

            // Index to db
            case IndexEndpointEnum.db:
                await catalogCategoryIndexToDbService.index(cli.store, cli.type === 'force');
                break;

            // Index to elastic
            case IndexEndpointEnum.elastic:
                await catalogCategoryIndexToElasticService.index(cli.store, cli.type === 'force');
                break;

            // Index to db & elastic
            case IndexEndpointEnum.all:
                await catalogCategoryIndexToDbService.index(cli.store, cli.type === 'force');
                await catalogCategoryIndexToElasticService.index(cli.store, cli.type === 'force');
                break;
        }
    }
}
