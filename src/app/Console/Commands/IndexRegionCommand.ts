/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import commander from 'commander';
import BaseCommand, { OptionType } from 'sosise-core/build/Command/BaseCommand';
import IOC from 'sosise-core/build/ServiceProviders/IOC';
import IndexEndpointEnum from '../../Enums/IndexEndpointEnum';
import RegionIndexToDbService from '../../Services/Index/Db/Region/RegionIndexToDbService';
import RegionIndexToElasticService from '../../Services/Index/Elastic/Region/RegionIndexToElasticService';

export default class IndexRegionCommand extends BaseCommand {
    /**
     * Command name
     */
    protected signature: string = 'index:region';

    /**
     * Command description
     */
    protected description: string = 'Index region';

    /**
     * When command is executed prevent from double execution
     */
    protected singleExecution: boolean = false;

    /**
     * Command options
     */
    protected options: OptionType[] = [
        // Options can pass some values
        { flag: '-e, --endpoint <endpoint>', description: 'Index endpoint [IndexEndpointEnum]', default: 'all', required: false },
        { flag: '-s, --store <store>', description: 'Index store [StoreCodeEnum]', default: 'all', required: false },
        { flag: '-t, --type <type>', description: 'Index type (force | full)', default: 'force', required: false }
    ];

    /**
     * Execute the console command
     */
    public async handle(cli: commander.Command): Promise<void> {

        // Init index to DB service
        const indexToDbService = IOC.make(RegionIndexToDbService) as RegionIndexToDbService;

        // Init index to ELASTIC service
        const indexToElasticService = IOC.make(RegionIndexToElasticService) as RegionIndexToElasticService;

        switch (cli.endpoint) {

            // Index to db
            case IndexEndpointEnum.db:
                await indexToDbService.index(cli.store);
                break;

            // Index to elastic
            case IndexEndpointEnum.elastic:
                await indexToElasticService.index(cli.store, cli.type === 'force');
                break;

            // Index to db & elastic
            case IndexEndpointEnum.all:
                await indexToDbService.index(cli.store);
                await indexToElasticService.index(cli.store, cli.type === 'force');
                break;
        }
    }
}
