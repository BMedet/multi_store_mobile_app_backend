/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import commander from 'commander';
import BaseCommand, { OptionType } from 'sosise-core/build/Command/BaseCommand';
import IOC from 'sosise-core/build/ServiceProviders/IOC';
import IndexEndpointEnum from '../../Enums/IndexEndpointEnum';
import StoreCodeEnum from '../../Enums/StoreCodeEnum';
import CatalogAttributesIndexToDbService from '../../Services/Index/Db/Catalog/CatalogAttributesIndexToDbService';
import CatalogAttributeIndexToElasticService from '../../Services/Index/Elastic/Catalog/CatalogAttributeIndexToElasticService';

export default class IndexProductAttributesCommand extends BaseCommand {
    /**
     * Command name
     */
    protected signature: string = 'index:catalog_attribute';

    /**
     * Command description
     */
    protected description: string = 'Index attributes';

    /**
     * When command is executed prevent from double execution
     */
    protected singleExecution: boolean = false;

    /**
     * Command options
     */
    protected options: OptionType[] = [
        // Options can pass some values
        { flag: '-e, --endpoint <endpoint>', description: 'Index endpoint [IndexEndpointEnum]', default: 'all', required: false },
        { flag: '-t, --type <type>', description: 'Index type (force | full)', default: 'force', required: false },
    ];

    /**
     * Execute the console command
     */
    public async handle(cli: commander.Command): Promise<void> {

        // Init index to DB service
        const indexToDbService = IOC.make('CatalogAttributesIndexToDbService') as CatalogAttributesIndexToDbService;

        // Init index to ELASTIC service
        const indexToElasticService = IOC.make('CatalogAttributeIndexToElasticService') as CatalogAttributeIndexToElasticService;

        switch (cli.endpoint) {

            // Index to db
            case IndexEndpointEnum.db:
                await indexToDbService.index();
                break;

            // Index to elastic
            case IndexEndpointEnum.elastic:
                await indexToElasticService.index(StoreCodeEnum.all, cli.type === 'force');
                break;

            // Index to db & elastic
            case IndexEndpointEnum.all:
                await indexToDbService.index();
                await indexToElasticService.index(StoreCodeEnum.all, cli.type === 'force');
                break;
        }
    }
}
