import express from 'express';
import { Request, Response, NextFunction } from 'express';
import Helper from 'sosise-core/build/Helper/Helper';
import CustomerController from '../../../../app/Http/Controllers/Customer/CustomerController';
const customerRouter = express.Router({ mergeParams: true });

// Init controller
const customerConstroller = new CustomerController();

/**
 * Init customer
 */
customerRouter.post(`/`, (request: Request, response: Response, next: NextFunction) => {
    customerConstroller.initCustomer(request, response, next);
});

/**
 * Update customer
 */
customerRouter.put(`/`, (request: Request, response: Response, next: NextFunction) => {
    customerConstroller.updateCustomer(request, response, next);
});

export default customerRouter;
