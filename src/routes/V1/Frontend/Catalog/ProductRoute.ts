import express from 'express';
import { Request, Response, NextFunction } from 'express';
import ProductController from '../../../../app/Http/Controllers/Catalog/ProductController';
const catalogProductRouter = express.Router({mergeParams: true});

// Get product
const productController = new ProductController();

/**
 * Get products list by category id
 */
catalogProductRouter.get(`/category/:id`, (request: Request, response: Response, next: NextFunction) => {
    productController.getProductsByCategoryId(request, response, next);
});

/**
 * Get product by id
 */
catalogProductRouter.get(`/:id`, (request: Request, response: Response, next: NextFunction) => {
    productController.getProductById(request, response, next);
});


export default catalogProductRouter;
