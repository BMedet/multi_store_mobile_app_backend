import express from 'express';
import { Request, Response, NextFunction } from 'express';
import Helper from 'sosise-core/build/Helper/Helper';
import CategoryController from '../../../../app/Http/Controllers/Catalog/CategoryController';
const catalogCategoryRouter = express.Router({mergeParams: true});


// IndexController
const categoryController = new CategoryController();
catalogCategoryRouter.get(`/tree`, (request: Request, response: Response, next: NextFunction) => {
    categoryController.getCategoriesTree(request, response, next);
});
catalogCategoryRouter.get(`/:categoryId`, (request: Request, response: Response, next: NextFunction) => {
    // get all data by catgeoryId (carrouselle, banner, rec_widget...)
    // categoryController.getCategoriesTree(request, response, next);
});


export default catalogCategoryRouter;
