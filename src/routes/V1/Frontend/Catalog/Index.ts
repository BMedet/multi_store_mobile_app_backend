import express from 'express';
import catalogCategoryRouter from './CategoryRoute';
import catalogProductRouter from './ProductRoute';
const catalogRouter = express.Router({mergeParams: true});

const categoryPrefix = 'category';
const productPrefix = 'product';

/**
 * Catalog category router
 */
catalogRouter.use(`/${categoryPrefix}`, catalogCategoryRouter);

/**
 * Catalog product router
 */
catalogRouter.use(`/${productPrefix}`, catalogProductRouter);

export default catalogRouter;
