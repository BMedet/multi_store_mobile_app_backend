import express from 'express';
import catalogRouter from './Catalog/Index';
import customerRouter from './Customer/Index';
const frontendRouter = express.Router({mergeParams: true});

const version = 'v1';

const catalogPrefix = 'catalog';
const customerPrefix = 'customer';

/**
 * Catalog router
 */
frontendRouter.use(`/${version}/:storeId/${catalogPrefix}`, catalogRouter);

/**
 * Customer router
 */
frontendRouter.use(`/${version}/:storeId/${customerPrefix}`, customerRouter);

export default frontendRouter;
