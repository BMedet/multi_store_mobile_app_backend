import express from 'express';
import DocumentationBasicAuthMiddleware from '../../app/Http/Middlewares/DocumentationBasicAuthMiddleware';
import frontendRouter from './Frontend/Index';
const firstVersionRouter = express.Router({mergeParams: true});

const version = 'v1';

// Documentation
const documentaionBasicAuthMiddleware = new DocumentationBasicAuthMiddleware();
firstVersionRouter.use(`/${version}/docs`, [
    documentaionBasicAuthMiddleware.handle,
    express.static(process.cwd() + `/docs/${version}`, { index: 'index.html' })
]);

/**
 * Catalog router
 */
firstVersionRouter.use(`/`, frontendRouter);


export default firstVersionRouter;
