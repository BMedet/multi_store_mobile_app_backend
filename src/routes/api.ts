import express from 'express';
import firstVersionRouter from './V1/Index';
const router = express.Router({mergeParams: true});
import cors from 'cors';
const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
};

router.use(cors(corsOptions));

const prefix = 'api';

/**
 * First version api router (v1)
 */
router.use(`/${prefix}`, firstVersionRouter);

export default router;
