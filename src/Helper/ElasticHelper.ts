/**
 * @author Medet Bakhytzhan <b.m.bakytzhan@gmail.com>
 */

import ElasticAliasNameEnum from "../app/Enums/ElasticAliasNameEnum";
import IndexTypeEnum from "../app/Enums/IndexTypeEnum";



export default class ElasticHelper {


    /**
     * Generate index alias name
     */
    public static getAliasName(indexType: IndexTypeEnum, storeCode: string): string {
        let aliasTemplate = '';

        switch (indexType) {
            case IndexTypeEnum.catalogCategory:
                aliasTemplate = ElasticAliasNameEnum.catalogCategory;
                break;
            case IndexTypeEnum.catalogProduct:
                aliasTemplate = ElasticAliasNameEnum.catalogProduct;
                break;
            case IndexTypeEnum.postCategory:
                aliasTemplate = ElasticAliasNameEnum.postCategory;
                break;
            case IndexTypeEnum.postOffer:
                aliasTemplate = ElasticAliasNameEnum.postOffer;
                break;
            case IndexTypeEnum.region:
                aliasTemplate = ElasticAliasNameEnum.region;
                break;
            case IndexTypeEnum.catalogAttribute:
                aliasTemplate = ElasticAliasNameEnum.catalogAttribute;
                break;
            case IndexTypeEnum.catalogAttributeOption:
                aliasTemplate = ElasticAliasNameEnum.catalogAttributeOption;
                break;
        }
        return aliasTemplate.replace('{store}', storeCode.toLowerCase());
    }
}
