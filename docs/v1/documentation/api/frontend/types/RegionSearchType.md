# RegionSearchType

```
entityId: number;
name: string;
code: string;
declensionName: string | null;
polygon: string | null;
```
