# CatalogCategorySearchType

```
    entityId: number,
    name: string,
    urlPath: string,
    thumbnailImage: string | null,
    showThumbnail: boolean,
    parentId: number,
    position: number,
    level: number,
    nameInMenu: string | null,
    includeInMenu: boolean
```
