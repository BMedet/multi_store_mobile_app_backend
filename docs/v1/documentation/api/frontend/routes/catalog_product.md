# Получение списка всех регионов

Данный метод вызывается фронтом приложения

> Возвращает список регионов отсортированный по полю ```position``` magento.

**URL** : `/api/v1/region/:storeId`

**Method** : `GET`

**Content-Type** : `application/json`

**Auth required** : `NO`

## Пример вызова
```bash
curl --location --request POST \
--url http://127.0.0.1:9090/api/v1/region/1 \
--header 'Content-Type: application/json' \
```

## Пример ответа

**Code** : `200 OK`

```json
{
    "message": "\"Product status control\" job was sent to queue",
    "code": 1000,
    "data": null
}
```
