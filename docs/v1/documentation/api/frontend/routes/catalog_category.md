# 1. Получение дерева активных категрий (для бургера)

Данный метод вызывается фронтом приложения

> Возвращает дерево категорий отсортированный по полю ```position``` magento.

**URL** : `/api/v1/:storeId/catalog/category/tree`

**Method** : `GET`

**Auth required** : `NO`

## Пример вызова
```bash
curl --location --request POST \
--url http://127.0.0.1:9090/api/v1/1/catalog/category/tree \
```

## Пример ответа

**Code** : `200 OK`

**Type** : `RegionSearchType[]`

```json
{
    "message": "\"Product status control\" job was sent to queue",
    "code": 1000,
    "data": null
}
```
