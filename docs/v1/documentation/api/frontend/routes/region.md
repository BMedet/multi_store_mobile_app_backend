# 1. Получение списка всех регионов

Данный метод вызывается фронтом приложения

> Возвращает список регионов отсортированный по полю ```position``` magento.

**URL** : `/api/v1/:storeId/region/`

**Method** : `GET`

**Auth required** : `NO`

## Пример вызова
```bash
curl --location --request POST \
--url http://127.0.0.1:9090/api/v1/1/region \
```

## Пример ответа

**Code** : `200 OK`

**Type** : `RegionSearchType[]`

```json
[
    {
        "entityId": 541,
        "name": "Алматы",
        "code": "almaty",
        "declensionName": null,
        "polygon": null
    },
    {
        "entityId": 548,
        "name": "Алтай",
        "code": "zyrjanovsk",
        "declensionName": "в Алтае",
        "polygon": null
    },
    {
        "entityId": 561,
        "name": "Петропавловск",
        "code": "petropavlovsk",
        "declensionName": "в Петропавловске",
        "polygon": null
    }
]
```
