
- API (Frontend)
    - [Region](documentation/api/frontend/routes/region)
    - [Catalog Product](documentation/api/frontend/routes/catalog_product)
    - [Catalog Category](documentation/api/frontend/routes/catalog_category)

- Exceptios
    - [ContentTypeNotSetException](documentation/api/frontend/exceptions/ContentTypeNotSetException)

- Types
    - [RegionSearchType](documentation/api/frontend/types/RegionSearchType)
    - [CatalogCategorySearchType](documentation/api/frontend/types/CatalogCategorySearchType)
